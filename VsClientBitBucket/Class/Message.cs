﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VsClientBitBucket.Class
{
    public class Message
    {
        public string MsgId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string MsgSize { get; set; }
        public string DateRecorded { get; set; }
        public string FileName { get; set; }
        public string ActiveDate { get; set; }
        public decimal RecordTime { get; set; }
        public long Filelength { get; set; }
        public string Person { get; set; }
        public int RecipientCount { get; set; }
        public string Recipients { get; set; }
    }
}
