﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace VsClientBitBucket.Class
{
    [MessageContract]
    public class MessageContract
    {
       
            [MessageHeader]
            public string filePath;

            [MessageBodyMember]
            public Stream bytes;

       
    }
}