﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VsClientBitBucket.Class
{
    public class VsUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string TenantUri { get; set; }
    }
}