﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VsClientBitBucket.Class
{
    public class VsDash
    {
        public string Tenant { get; set; }
        public string TenantType { get; set; }
        public string OneTimeKey { get; set; }
    }
}