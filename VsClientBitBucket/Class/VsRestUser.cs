﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VsClientBitBucket.Class
{
    public class VsRestUser
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AudioSrc { get; set; }
        public string WelcomeFlag { get; set; }
        public int PremiumFlag { get; set; }
        public string InviteNudgeFlag { get; set; }
        public string RecNudgeFlag { get; set; }
        public string TosFlag { get; set; }
        public string MusicDownloadFlag { get; set; }
        public int NetworkCounter { get; set; }
        public DateTime AccountCreated { get; set; }
        public int InviteCount { get; set; }
        public DateTime? LastMessageRecorded { get; set; }
        public DateTime? LastInviteSent { get; set; }
        public int MessageCount { get; set; }
        public string OneTimeKey { get; set; }
        public List<Message> SentList { get; set; }
        public List<Message> RecvList { get; set; }
        public List<VS_Sender> SenderList { get; set; }
    }
}