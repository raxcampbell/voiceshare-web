﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfirmationPage.aspx.cs" Inherits="VsClientBitBucket.ConfirmationPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ConfirmationPage</title>
    <link href="Styles/Custom2.css" rel="stylesheet" />
    <link href="Styles/StyleSheet.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <div id="fg_membersite">
        <form id="frmConfirm" runat="server">
            <div>
                <fieldset>
                    <legend>Confirm</legend>
                    <br />
                    <div>
                        <asp:Label ID="lbl_Confirm" runat="server" Font-Bold="True">
                         Your registration was successful.
                        </asp:Label>
                    </div>
                    <br />
                    <br />
                    <%--<div>
                        <asp:HyperLink ID="hypLogin" runat="server" NavigateUrl="Login.aspx">Login</asp:HyperLink>
                    </div>--%>
                    <div>
                        <asp:Panel ID="pnlForgot" runat="server" Visible="False">
                            <asp:HyperLink ID="hypForgot" runat="server" NavigateUrl="Forgot.aspx" Visible="False">Contact us</asp:HyperLink>
                        </asp:Panel>
                    </div>
                </fieldset>
            </div>
        </form>
    </div>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-15214788-2']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</body>
</html>
