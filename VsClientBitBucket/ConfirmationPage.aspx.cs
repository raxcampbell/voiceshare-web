﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VsClientBitBucket
{
    public partial class ConfirmationPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
                string registrationkey = Request.QueryString["uriparameter"];
                VoiceWebService.RegisterServiceClient UserConfirmation = new VoiceWebService.RegisterServiceClient();
                string UserConfirm = UserConfirmation.Confirmation(registrationkey);
                if (UserConfirm == "Successful")
                {
                    lbl_Confirm.Text = "Your account has been activated. You may now <a href='https://bellybuds.voiceshare.net/login.aspx'>log in.</a>";
                }
                else if (UserConfirm == "AlreadyRegister")
                {
                    lbl_Confirm.Text = "You have already activated your account.";
                }
                  
                else
                {
                    lbl_Confirm.Text = "Sorry, try again.";
                }
            }
         

        }
    }
