﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="Dash.aspx.cs" Inherits="VsClientBitBucket.Dashboard" %>

<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="VsClientBitBucket.SecureVoiceWebService" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Belly Buds: Voiceshare Application</title>
    <link href="Styles/Custom2.css" type="text/css" rel="stylesheet" />
    <link href="Styles/Menu.css" rel="stylesheet" />
    <link href="Styles/Container.css" rel="stylesheet" />
    <link href="Styles/WelcomeModal.css" rel="stylesheet" />
    <%--<script src="scripts/jquery-2.1.0.min.js"></script>--%>
    <script src="scripts/jquery-1.12.0.min.js"></script>
    <script src="scripts/jquery-ui-1.10.3.min.js"></script>
    <script src="scripts/jquery.js"></script>
    <script type="text/javascript" src="Scripts/modernizr-2.6.2.js"></script>
    <script src="scripts/mediaelement-and-player.min.js"></script>
    <link href="scripts/mediaelementplayer.css" rel="stylesheet" />
    <style type="text/css">
        .breakword {
            word-wrap: break-word;
            -moz-word-break: break-all;
            -o-word-break: break-all;
            word-break: break-all;
        }
    </style>
    <style type="text/css">
        .hide {
            display: none;
        }
    </style>
    <style type="text/css">
        .cellSpacing td {
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
    <style>
        #divSentRecvx {
            position: relative;
            height: 400px;
            width: 100%;
            border: thick solid black;
        }

            #divSentRecvx div {
                background: grey;
                width: 200px;
            }

        #divSentMsgx {
            position: absolute;
            left: 0px;
            bottom: 0px;
        }

        #divRecvMsgx {
            position: absolute;
            right: 0px;
            bottom: 0px;
        }

        .auto-style2 {
            width: 100%;
        }
    </style>
</head>
<%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>--%>
<!-- CSS and JS for popups -->
<script type="text/javascript" src="scripts/main.js"></script>
<script type="text/javascript" src="scripts//jquery.js"></script>
<!--<link type="text/css" rel="stylesheet" href="http://www.javascripttoolbox.com/includes/SyntaxHighlighter.css" ></link>-->
<script type="text/javascript" src="scripts/shCore.js"></script>
<script type="text/javascript" src="scripts/shBrushJScript.js"></script>
<script type="text/javascript" src="scripts/popup.js"></script>
<script type="text/javascript" src="Scripts/modernizr-2.6.2.js"></script>
<script type="text/javascript">
    function ShowFwdModal(msgIndex, intGridNum) {
        document.getElementById('hidMsgSelected').value = msgIndex;
        document.getElementById('hidGridUsed').value = intGridNum;
        Popup.showModal('modalFwdMsgPopup', null, null, { 'screenColor': '#99ff99', 'screenOpacity': .6 }); return false;
    }
    function ShowSelRecipModal(msgIndex, intGridNum) {
        document.getElementById('hidMsgSelected').value = msgIndex;
        document.getElementById('hidGridUsed').value = intGridNum;
        Popup.showModal('modalSelRecipPopup', null, null, { 'screenColor': '#99ff99', 'screenOpacity': .6 }); return false;
    }
    function ShowRecipListModal(msgIndex, intGridNum, recipList) {
        document.getElementById('<%=hidMsgSelected.ClientID %>').value = msgIndex;
        document.getElementById('<%=hidGridUsed.ClientID %>').value = intGridNum;
        document.getElementById('<%=hidRecipList.ClientID %>').value = recipList;
        var label = document.getElementById("<%= lblList.ClientID %>");
        var output = (recipList != null) ? label.innerHTML = recipList : label.innerHTML = "No recipients";
        label.innerHTML = output;
        Popup.showModal('modalRecipListPopup', null, null, { 'screenColor': '#99ff99', 'screenOpacity': .6 }); return false;
    }
    function HideRecipListModal() {
        Popup.hide('modalRecipListPopup');
    }
    function HideFwdModal() {
        Popup.hide('modalFwdMsgPopup');
    }
    function HideSelRecipModal() {
        Popup.hide('modalSelRecipPopup');
    }
    function ShowPlayModal(msgIndex, intGridNum) {
        document.getElementById('hidMsgSelected').value = msgIndex;
        document.getElementById('hidGridUsed').value = intGridNum;
        Popup.showModal('modalPlayMsgPopup', null, null, { 'screenColor': '#99ff99', 'screenOpacity': .6 });
        if (intGridNum == 0) {
            selectGridName = 'grdMsgSent';
        } else {
            selectGridName = 'grdMsgReceived';
        }
        selectedFileElement = selectGridName + '_hidMsgId_' + msgIndex;
        playAudio(document.getElementById(selectedFileElement).value);
    }
    function HidePlayModal() {
        Popup.hide('modalFwdMsgPopup');
    }
    function ShowDownModal(msgIndex, intGridNum) {
        document.getElementById('hidMsgSelected').value = msgIndex;
        document.getElementById('hidGridUsed').value = intGridNum;
        if (intGridNum == 0) {
            selectGridName = 'grdMsgSent';
        } else {
            selectGridName = 'grdMsgReceived';
        }
        selectedFileElement = selectGridName + '_hidMsgId_' + msgIndex;
        window.open(document.getElementById("<%= hidDynamicFileSrc.ClientID %>").value + document.getElementById(selectedFileElement).value);
        //Popup.showModal('modalDownMsgPopup', null, null, { 'screenColor': '#99ff99', 'screenOpacity': .6 }); return false;
    }
    function ShowPremiumModal() {
        Popup.showModal('modalNoPremiumPopup', null, null, { 'screenColor': '#99ff99', 'screenOpacity': .6 }); return false;
    }
    function HideDownModal() {
        Popup.hide('modalDownMsgPopup');
    }
    function HidePremiumModal() {
        Popup.hide('modalNoPremiumPopup');
    }
    function ShowTosModal() {
        Popup.showModal('modalTosPopup', null, null, { 'screenColor': '#99ff99', 'screenOpacity': .6 });
    }
    function HideTosModal() {
        Popup.hide('modalTosPopup');
    }
    function ShowDelModal(msgIndex, intGridNum) {
        document.getElementById('hidMsgSelected').value = msgIndex;
        document.getElementById('hidGridUsed').value = intGridNum;
        Popup.showModal('modalDelMsgPopup', null, null, { 'screenColor': '#99ff99', 'screenOpacity': .6 }); return false;
    }
    function ShowDelInvtModal(msgIndex, intGridNum) {
        //alert("Invite Selected: " + msgIndex);
        document.getElementById('hidMsgSelected').value = msgIndex;
        document.getElementById('hidGridUsed').value = intGridNum;
        Popup.showModal('modalDeleteInvtPopup', null, null, { 'screenColor': '#99ff99', 'screenOpacity': .6 }); return false;
    }
    function ShowResendModal(msgIndex, intGridNum) {
        document.getElementById('hidMsgSelected').value = msgIndex;
        document.getElementById('hidGridUsed').value = intGridNum;
        Popup.showModal('modalResendInvtPopup', null, null, { 'screenColor': '#99ff99', 'screenOpacity': .6 }); return false;
    }
    function HideDelModal() {
        Popup.hide('modalDownMsgPopup');
    }
    function HideWelcomeModal() {
        Popup.hide('modalWelcomePopup');
    }
    function modalLoad() {
        document.getElementById('modalPlayer').src = document.getElementById('hidModalMsg').value;
    };
</script>
<script type="text/javascript">
    function playAudio(fileUrl) {
        var aud = document.getElementById('audioTest2');
        var fullFilename = document.getElementById("<%= hidDynamicFileSrc.ClientID %>").value + fileUrl;
        var l = fullFilename.length;
        var sourceType = 'audio/' + fullFilename.substring(fullFilename, l - 3, l);
        /* This will be the live version
        aud.src = 'http://www.voiceshare.net:18080/VoiceShareNewWebService/recordings/' + fileUrl;*/
        aud.src = document.getElementById("<%= hidDynamicFileSrc.ClientID %>").value + fileUrl;
        //aud.type = sourceType;
        /* aud.src = 'http://www.voiceshare.net:18080/VoiceShareBetaService/recordings/' + fileUrl; */
        /*window.alert(aud.src);*/
        aud.play();
    }
</script>
<script type="text/javascript">
    var wordLen = 3; // Maximum word length
    function checkMaxLen(txt, maxLen) {
        try {

            if (txt.value.length > (maxLen - 1)) {
                var cont = txt.value;
                txt.value = cont.substring(0, (maxLen - 1));
                return false;
            };
        } catch (e) {
        }
    }
</script>
<body onload="modalLoad()">
    <form id="form1" runat="server">
        <div id="wrapper">
            <div id="page">
                <!--BEGIN HEADER -->
                <div id="header">
                    <a style="float: left" href="http://voiceshare.wavhello.com/Dash.aspx">
                        <%--<img  src="images/voiceshareicon.jpg" width="410px" />--%><asp:Image ID="imgLogo" runat="server" ImageUrl="images/LoginAndRegister/voiceshare_by_wavhello_logo_transparent_320x114.png" />
                    </a>
                    <!-- iphone icons -->
                    <div class="icons">
                        <div class="nav-icon">
                            <asp:ImageButton ID="btnFacebook" runat="server" PostBackUrl="http://www.facebook.com/wavhello" ImageUrl="images/facebook.png"></asp:ImageButton>
                        </div>
                        <div class="nav-icon">
                            <asp:ImageButton ID="btnTwitter" runat="server" PostBackUrl="http://www.twitter.com/wavhello" ImageUrl="images/twitter.png"></asp:ImageButton>
                        </div>
                    </div>
                    <!-- /end iphone icons -->
                </div>
                <!--END OF HEADER -->
                <!--BEGIN CONTENT -->
                <div id="content">
                    <div class="outside">
                        <div class="inside">
                            <div class="inside_full">
                                <div style="text-align: right; font-size: 10px">
                                    (Not "<asp:Label ID="lbl_UserName" runat="server" />"?, <a href="Login.aspx">Sign In</a>)
                                </div>
                                <ul id="profile_bar">
                                    <li><a href="Record.aspx">Record a Message</a></li>
                                    <li><a href="Invite.aspx">Send an Invitation</a></li>
                                    <li><a href="Dash.aspx">My Dashboard</a></li>
                                    <li><a href="EditForm.aspx">Edit User Profile</a></li>
                                    <li><a href="FAQ.aspx">FAQ</a></li>
                                    <li><strong>You have <span runat="server" style="background-color: red; color: white; -ms-border-radius: 50%; border-radius: 50%; padding: 2px;" id="spanCounter"></span>connections.</strong></li>
                                </ul>
                            </div>
                            <!-- DASH SECTION -->
                            <div id="dash_wrapper">
                                <div class="inside_full" id="divSentRecv">
                                    <!-- MESSAGE SECTION -->
                                    <div class="fboxes itembox all_saved_content" id="divMsgSent" style="margin: 2px 2px 2px 2px; width: 95%; height: 100%;">
                                        <div class="vs-row">
                                            <table class="auto-style2">
                                                <tr>
                                                    <td class="d1">&nbsp;</td>
                                                    <td class="d2">
                                                        <h4><strong>My Messages</strong></h4>
                                                        <%--<h4><strong>Voice Messages I've Created</strong></h4>--%>
                                                    </td>
                                                    <td class="d3">
                                                        <asp:Button ID="cmdMsgSentHide" runat="server" Text="Hide" OnClick="cmdMsgSentHide_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="itemlist">
                                            <asp:GridView
                                                ID="grdMsgSent"
                                                runat="server"
                                                CssClass="cellSpacing"
                                                OnRowDataBound="grdMsgSent_RowDataBound"
                                                AllowSorting="True"
                                                EmptyDataText="You have not sent any messages."
                                                AllowPaging="True"
                                                PageSize="6"
                                                OnPageIndexChanging="grdMsgSent_PageIndexChanging"
                                                OnSorting="grdMsgSent_Sorting"
                                                AutoGenerateColumns="False">
                                                <AlternatingRowStyle BackColor="#D3D3D3 " />
                                                <Columns>
                                                    <asp:TemplateField AccessibleHeaderText="Play" ShowHeader="False">
                                                        <ItemTemplate>
                                                            <asp:Image ID="lnkPlayImage" runat="server" ImageUrl="~/images/audio/vs-play-pnk.fw.png" ToolTip="Play" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField AccessibleHeaderText="Download" ShowHeader="False">
                                                        <ItemTemplate>
                                                            <asp:Image ID="lnkDownImage" runat="server" ImageUrl="~/images/audio/vs-dnl-pnk.fw.png" ToolTip="Download" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField AccessibleHeaderText="Select Recipients" ShowHeader="False">
                                                        <ItemTemplate>
                                                            <asp:Image ID="lnkFwdImage" runat="server" ImageUrl="~/images/audio/vs-fwd-pnk.fw.png" ToolTip="Select Recipients" />
                                                            <asp:HiddenField ID="hidMsgId" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField AccessibleHeaderText="Delete" ShowHeader="False">
                                                        <ItemTemplate>
                                                            <asp:Image ID="lnkDelImage" runat="server" ImageUrl="~/images/audio/vs-del-pnk.fw.png" ToolTip="Delete" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="DateRecorded" HeaderText="Date" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle VerticalAlign="Top" Width="165px" HorizontalAlign="Left" />
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="Title" HeaderText="Title" ItemStyle-CssClass="breakword" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle VerticalAlign="Top" Width="130px" HorizontalAlign="Left" />
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-CssClass="breakword" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle VerticalAlign="Top" Wrap="true" Width="280px" HorizontalAlign="Left" />
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="FileName" HeaderText="FileName" />

                                                    <asp:BoundField DataField="MsgId" HeaderText="MsgId" />

                                                    <asp:TemplateField HeaderText="Recipients">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRecipientsCount" runat="server" Text='<%# Eval("RecipientCount") %>'></asp:Label>
                                                            <asp:Label ID="lblRecip" runat="server" Text="recipients"></asp:Label>
                                                            <asp:Image ID="lnkMagnifyImage" runat="server" ImageUrl="~/images/audio/vs-recip.png" ToolTip="See Recipients" />

                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="RecordTime" HeaderText="Length" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Recipients" HeaderText="RecipientList" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <%--<div class="inside_full" id="div2">
                                    <div class="fboxes itembox all_saved_content" id="divMsgRecv" style="margin: 2px 2px 2px 2px; width: 95%; height: 100%;">
                                        <div class="vs-row" style="text-align: center">
                                            <table class="auto-style2">
                                                <tr>
                                                    <td class="d1">&nbsp;</td>
                                                    <td class="d2">
                                                        <h4><strong>Voice Messages From My Connections</strong></h4>
                                                    </td>
                                                    <td class="d3">
                                                        <asp:Button ID="cmdHideMsgRecv" runat="server" Text="Hide" OnClick="cmdHideMsgRecv_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="itemlist">
                                            <asp:GridView
                                                ID="grdMsgReceived"
                                                runat="server"
                                                CssClass="cellSpacing"
                                                AllowSorting="True"
                                                EmptyDataText="You have not received any messages."
                                                OnRowCommand="grdMsgReceived_RowCommand"
                                                OnRowDataBound="grdMsgReceived_RowDataBound"
                                                AllowPaging="True"
                                                PageSize="6"
                                                OnPageIndexChanging="grdMsgReceived_PageIndexChanging"
                                                OnSorting="grdMsgReceived_Sorting"
                                                AutoGenerateColumns="False">
                                                <AlternatingRowStyle BackColor="#D3D3D3 " />
                                                <Columns>
                                                    <asp:TemplateField AccessibleHeaderText="Play" ShowHeader="False">
                                                        <ItemTemplate>
                                                            <asp:Image ID="lnkPlayImage" runat="server" ImageUrl="~/images/audio/vs-play-pnk.fw.png" ToolTip="Play" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField AccessibleHeaderText="Download" ShowHeader="False">
                                                        <ItemTemplate>
                                                            <asp:Image ID="lnkDownImage" runat="server" ImageUrl="~/images/audio/vs-dnl-pnk.fw.png" ToolTip="Download" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField AccessibleHeaderText="Forward" ShowHeader="False">
                                                        <ItemTemplate>
                                                            <asp:Image ID="lnkFwdImage" runat="server" ImageUrl="~/images/audio/vs-fwd-pnk.fw.png" ToolTip="Forward" />
                                                            <asp:HiddenField ID="hidMsgId" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField AccessibleHeaderText="Delete" ShowHeader="False">
                                                        <ItemTemplate>
                                                            <asp:Image ID="lnkDelImage" runat="server" ImageUrl="~/images/audio/vs-del-pnk.fw.png" ToolTip="Delete" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="DateRecorded" HeaderText="Date" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="155px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Title" HeaderText="Title" ItemStyle-CssClass="breakword" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="135px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-CssClass="breakword" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="255px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="FileName" HeaderText="FileName" />
                                                    <asp:BoundField DataField="MsgId" HeaderText="MsgId" />
                                                    <asp:BoundField DataField="Person" HeaderText="Sender" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="130px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="RecordTime" HeaderText="Length" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="clear_all">
                                    <asp:HiddenField ID="hidDynamicFileSrc" runat="server" />
                                    <asp:HiddenField ID="hidMsgSelected" runat="server" />
                                    <asp:HiddenField ID="hidGridUsed" runat="server" />
                                    <asp:HiddenField ID="hidMsgSelectedName" runat="server" />
                                    <asp:HiddenField ID="hidModalMsg" runat="server" />
                                    <asp:HiddenField ID="hidRecipList" runat="server" />
                                </div>
                                <!--<div class="clear_all"></div>-->
                                <%--<div class="fboxes itembox all_saved_content" id="divInviteList" style="margin: 2px 2px 2px 2px; width: 95%; height: 100%;">
                                    <div class="vs-row" style="text-align: center">
                                        <table class="auto-style2">
                                            <tr>
                                                <td class="d1">&nbsp;</td>
                                                <td class="d2">
                                                    <h4><strong>Invitations I've Created&nbsp;&nbsp;</strong></h4>
                                                </td>
                                                <td class="d3">
                                                    <asp:Button ID="cmdMsgInvite" runat="server" Text="Hide" OnClick="cmdMsgInvite_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="itemlist">
                                        <asp:GridView ID="grdInvtCreated" runat="server" AllowSorting="True" CssClass="cellSpacing" EmptyDataText="You have not sent any messages." AutoGenerateColumns="False" AllowPaging="True" PageSize="6" OnPageIndexChanging="grdInvtCreated_PageIndexChanging" OnSorting="grdInvtCreated_Sorting" OnRowDataBound="grdInvtCreated_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField AccessibleHeaderText="Resend" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:Image ID="lnkResendInvite" runat="server" ImageUrl="~/images/audio/vs-fwd-pnk.fw.png" ToolTip="Resend Invite" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField AccessibleHeaderText="Cancel" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:Image ID="lnkCancelInvite" runat="server" ImageUrl="~/images/audio/vs-del-pnk.fw.png" ToolTip="Cancel Invite" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="RecipientEmail" HeaderText="Email" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="178px"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RecipientLastName" HeaderText="Last Name" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="178px"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RecipientFirstName" HeaderText="First Name" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="178px"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SentDate" HeaderText="Date" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="178px"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="InviteStatus" AccessibleHeaderText="Forward" HeaderText="Invite Status" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="180px"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="InviteId" HeaderText="InviteID" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="vs-row">
                                        <div class="vs-col">
                                            <p>
                                                &nbsp;
                                            </p>
                                        </div>
                                    </div>
                                </div>--%>
                            </div>
                            <!-- END DASH SECTION -->
                        </div>
                    </div>
                    <div class="outside">
                        <div class="inside footer">
                            <!--BEGIN FOOTER -->
                            <div class="container footbox">
                                <div class="footInner" style="margin-left: 18%;">
                                    <div class="page"><strong>Pages:</strong></div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypHome" href="http://www.wavhello.com/" runat="server">Home</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypContact" href="http://wavhello.com/pages/contact-us" runat="server">Contact Us</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypTerms" NavigateUrl="http://www.wavhello.com/terms-of-use" runat="server">Terms of Service</asp:HyperLink>
                                    </div>
                                </div>
                                <div class="footInner">
                                    <div class="stay"><strong>Stay In Touch:</strong></div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypRSS" href="http://www.bellybuds.com/feed/" runat="server">Site RSS Feed</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypTwitter" href="http://www.twitter.com/wavhello" runat="server">Twitter</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypFacebook" href="http://www.facebook.com/wavhello" runat="server">Facebook</asp:HyperLink>
                                    </div>
                                </div>
                                <div style="margin-left: -30px;"><strong>&copy; 2016 BellyBuds</strong></div>
                            </div>
                            <!--END OF FOOTER -->
                            <div class="clear_all"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--END OF CONTENT -->
            <!--BEGIN FOOTER -->
        </div>
        <!--END OF FOOTER -->
        <div class="popUpForm" id="modalRecipListPopup" style="border: 3px solid black; background-color: #ffffff; padding: 25px; display: none;">
            <div class="popUpRow">
                <asp:Label ID="lblList" runat="server"></asp:Label>
            </div>
            <br />
            <asp:Button ID="btnRecipListModal" runat="server" Text="OK" BackColor="White" ForeColor="Black" OnClientClick="Popup.hide('modalRecipListPopup')" />
        </div>

        <div class="popUpForm" id="modalSelRecipPopup" style="border: 3px solid black; background-color: #ffffff; padding: 25px; height: 200px; overflow: scroll; display: none;">
            <div class="popUpRow">
                <asp:GridView ID="grdSelMsgList" runat="server" Width="400px" AllowPaging="True">
                    <AlternatingRowStyle BackColor="#CCFFCC" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#FFCCCC" />
                </asp:GridView>
            </div>
            <div class="popUpRow">
                <asp:Label ID="Label3" runat="server" />
            </div>
            <asp:Button ID="btnSelRecip" runat="server" Text="OK" BackColor="White" ForeColor="Black" OnClick="btnSelRecip_Click" />
        </div>

        <div class="popUpForm" id="modalFwdMsgPopup" style="border: 3px solid black; background-color: #ffffff; padding: 25px; display: none;">
            <div class="popUpRow">
                <asp:GridView ID="grdRecipientList" runat="server" Width="400px" OnRowCommand="grdRecipientList_RowCommand" AllowPaging="True" PageSize="6" EmptyDataText="You have no connections">
                    <AlternatingRowStyle BackColor="#CCFFCC" />
                    <Columns>
                        <asp:ButtonField Text="Select" CommandName="cmdSelClick" />
                    </Columns>
                    <EditRowStyle BackColor="#FFCCCC" />
                </asp:GridView>
            </div>
            <div class="popUpRow">
                <asp:Label ID="lblForward" runat="server" />
            </div>
            <input onclick="Popup.hide('modalFwdMsgPopup')" type="button" value="OK" />
        </div>

        <div class="popUpForm" id="modalPlayMsgPopup" style="border: 3px solid black; background-color: #ffffff; padding: 25px; display: none;">
            <div class="popUpRow">
                <div class="info" style="text-align: center">
                    <asp:Label ID="Label4" runat="server" />
                </div>
                <audio id="audioTest2" type="audio/wav" controls="controls">
                    <p>Oops, looks like your browser doesn't support HTML 5 audio for mp3.</p>
                </audio>
            </div>
            <div class="popUpRow" style="text-align: center">
                <asp:Label ID="Label1" runat="server" />
                <input onclick="document.getElementById('audioTest2').src = ''; Popup.hide('modalPlayMsgPopup')" type="button" value="Done" />
            </div>
            <script>
                /* $('audio,video').mediaelementplayer(); */
            </script>
        </div>

        <div class="popUpForm" id="modalDownMsgPopup" style="border: 3px solid black; background-color: #ffffff; padding: 25px; display: none;">
            <div class="popUpRow" style="text-align: center">
                <p>Download Message</p>
            </div>
            <div class="popUpRow" style="text-align: center">
                <asp:TextBox ID="txtDownloadDir" runat="server"></asp:TextBox>
                <asp:Button ID="btnDownloadFile" runat="server" Text="Download" OnClick="btnDownloadFile_Click" />
                <asp:Label ID="Label2" runat="server" />
                <input onclick="Popup.hide('modalDownMsgPopup')" type="button" value="Done" />
            </div>
        </div>

        <div class="popUpForm" id="modalNoPremiumPopup" style="border: 3px solid black; background-color: #ffffff; padding: 25px; display: none;">
            <div class="popUpRow" style="text-align: center">
                <p>Download access limited to premium members.</p>
            </div>
            <div class="popUpRow" style="text-align: center">
                <input onclick="Popup.hide('modalNoPremiumPopup')" type="button" value="Done" />
            </div>
        </div>

        <div class="popUpForm" id="modalDelMsgPopup" style="border: 3px solid black; background-color: #ffffff; padding: 25px; display: none;">
            <div class="popUpRow" style="text-align: center">
                <p>Delete Message</p>
            </div>
            <div class="popUpRow" style="text-align: center">
                <p>Click </p>
                <asp:Label ID="lblDeleteMessage" runat="server" />
                <asp:Button ID="btnDelMsgConfirm" runat="server" Text="OK" OnClick="btnDelMsgConfirm_Click" />
                <input onclick="Popup.hide('modalDelMsgPopup')" type="button" value="Cancel" />
            </div>
        </div>

        <%--<div class="popUpForm" id="modalResendInvtPopup" style="border: 3px solid black; background-color: #ffffff; padding: 25px; display: none;">
            <div class="popUpRow" style="text-align: center">
                <p>Resend Invitation</p>
            </div>
            <div class="popUpRow" style="text-align: center">
                <p>Click </p>
                <asp:Label ID="lblResendInvt" runat="server" />
                <asp:Button ID="btnResendInvt" runat="server" Text="OK" OnClick="btnResendInvt_Click" />
                <input onclick="Popup.hide('modalResendInvtPopup')" type="button" value="Cancel" />
            </div>
        </div>--%>

        <%--<div class="popUpForm" id="modalDeleteInvtPopup" style="border: 3px solid black; background-color: #ffffff; padding: 25px; display: none;">
            <div class="popUpRow" style="text-align: center">
                <p>Delete Invitation</p>
            </div>
            <div class="popUpRow" style="text-align: center">
                <p>Click </p>
                <asp:Label ID="lblDeleteInvt" runat="server" />
                <asp:Button ID="btnCancelInvt" runat="server" Text="OK" OnClick="btnCancelInvt_Click" PostBackUrl="~/Dash.aspx" />
                <input onclick="Popup.hide('modalDeleteInvtPopup')" type="button" value="Cancel" />
            </div>
        </div>--%>

        <div class="popUpForm" id="modalTosPopup" style="display: none;">
            <div>
                <div class="header">
                    <div class="title">
                        Updated Terms of Service
                    </div>
                </div>
                <div class="content">
                    <p>
                        <asp:CheckBox ID="chkTos" runat="server" />
                        By clicking this checkbox you agree to our <a href="TermsOfService.aspx" target="_blank">Terms of Service</a>
                    </p>
                    <p>
                        <asp:Button ID="btnTosSubmit" runat="server" Text="I agree" CssClass="button" OnClick="btnTosSubmit_Click" CausesValidation="False" />
                    </p>
                    <asp:Label ID="lblTerms" runat="server"></asp:Label>
                </div>
            </div>
        </div>

        <div class="popUpForm" id="modalWelcomePopup" style="display: none;">
            <div id="welcomeWrapper">
                <div class="header">
                    <div class="title">
                        Welcome to BellyBuds VoiceShare!
                    </div>
                </div>
                <div class="content">
                    <div id="msgCenter" class="row_pull_left">
                        <asp:Label ID="lblMusic" runat="server" BackColor="Yellow"></asp:Label>
                        <p>VoiceShare by Bellybuds is a voice recording network that allows you to share your voice from wherever you are. Record a story, a song, a moment - right to the site. Or else invite others to join your network so they can share recordings with you. With a few simple clicks, messages can be downloaded to any MP3 player and share with the baby-to-be.</p>
                        <div class="audio">
                            <audio id="modalPlayer" controls="controls">
                                <p>Your browser does not support the audio element.</p>
                            </audio>
                        </div>
                    </div>
                    <div class="row_pull_right">
                        <p>Invite friends and family to join your network so you can begin sending and receiving messages.</p>
                        <div>
                            <asp:HyperLink ID="hypWelcomeInvite" runat="server" NavigateUrl="~/Invite.aspx">Click to go to the invitation page.</asp:HyperLink>
                        </div>
                    </div>
                    <div class="modal_image_left">
                        <asp:Image ID="imgInvite" runat="server" ImageUrl="images/vs_invite_resize.jpg" />
                    </div>
                    <div class="row_pull_left_bottom_row">
                        <p>Record messages for your baby-to-be or send them to others in your network.</p>
                        <div>
                            <asp:HyperLink ID="hypWelcomeRecord" runat="server" NavigateUrl="~/Record.aspx">Click to go to the record page.</asp:HyperLink>
                        </div>
                    </div>
                    <div class="modal_image_right">
                        <asp:Image ID="imgRecord" runat="server" ImageUrl="images/record_resize198px.jpg" />
                    </div>
                    <div class="welcome_chkbox">
                        <asp:Literal ID="litWelcomeChkbox" runat="server">Do Not Display Again</asp:Literal>
                        <asp:CheckBox ID="welcomeChkbox" runat="server" BorderStyle="None" OnCheckedChanged="welcomeChkbox_CheckedChanged" OnClick="Popup.hide('modalWelcomePopup')" AutoPostBack="True" />
                    </div>
                </div>
            </div>
        </div>

        <div class="popUpForm" id="modalInviteNudge" style="border: 3px solid black; background-color: #ffffff; padding: 25px; display: none;">
            <div class="invite_modal_message">
                It's been 7 days and you haven't invited any friends. You will get the most benefit out of Voice Share if you have a network of friends and family.
                    <div>
                        <asp:HyperLink ID="nudgeInviteHyp" runat="server" NavigateUrl="~/Invite.aspx">Click to go to the invitation page.</asp:HyperLink>
                    </div>
            </div>
        </div>

        <div class="popUpForm" id="modalRecordNudge" style="border: 3px solid black; background-color: #ffffff; padding: 25px; display: none;">
            <div class="record_modal_message">
                It's been 7 days and you haven't recorded a message for your friend or family member's baby-to-be. Your relation would love to be able to play a message for their little one.
                    <div>
                        <asp:HyperLink ID="nudgeRecordHyp" runat="server" NavigateUrl="~/Record.aspx">Click to go to the record page.</asp:HyperLink>
                    </div>
            </div>
        </div>
        <script>
            /*$("#invite_recip_first").keyup(function () {
            var value = $(this).val();
            $("#effectsthis").text(value);
            }).keyup(); */
            //----------Recipient Function Hide/Show Based on Checkbox
            if ($("#chkJustSave").is(":checked")) {
                $("#vsAddRecipWrapper").hide()
            }
            $('#chkJustSave').click(function () {
                $('#vsAddRecipWrapper').toggle("fast");
            });
            //----------Recipient Append
            $('#btnAddRecip').click(function () {
                var itemlist;
                /*Form Vars*/
                var firstinput = $('#invite_recip_first').val();
                var lastinput = $('#invite_recip_last').val();
                var emailinput = $('#invite_recip_email1').val();
                var emailinputmatch = $('#invite_recip_email2').val();
                var nickinput = $('#invite_recip_nick').val();
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                //Validation
                var errormssg = '';
                if (firstinput == '') {
                    errormssg = '- First Name Incorrect\n';
                }
                if (lastinput == '') {
                    errormssg = errormssg + '- Last Name Incorrect\n';
                }
                if (emailinput == '' || !filter.test(emailinput)) {
                    errormssg = errormssg + '- Email Incorrect\n';
                }
                if (emailinput != emailinputmatch) {
                    errormssg = errormssg + '- Emails Fields Do Not Match';
                }
                if (errormssg != '') {
                    alert(errormssg);
                } else {
                    //How many already in list
                    itemlist = $("#vsRecipTable tbody tr").size();
                    alert(itemlist);
                    itemlist = itemlist + 1;

                    //add to list
                    $('#vsRecipTable tbody').append('<tr id="entry[' + itemlist + ']"><th><input type="text" value="' + firstinput + '" name="invite_recip_first[' + itemlist + ']" id="invite_recip_first[' + itemlist + ']"></th><th><input type="text" value="' + lastinput + '" name="invite_recip_last[' + itemlist + ']" id="invite_recip_last[' + itemlist + ']"></th><th><input type="text" value="' + emailinput + '" name="invite_recip_email[' + itemlist + ']" id="invite_recip_email[' + itemlist + ']"></th><th><input type="text" value="' + nickinput + '" name="invite_recip_nick[' + itemlist + ']" id="invite_recip_nick[' + itemlist + ']"></th><th style="cursor:pointer" onclick = $(this).parent().remove();>remove</th></tr>');
                    $('#invite_recip_first').val('');
                    $('#invite_recip_last').val('');
                    $('#invite_recip_email1').val('');
                    $('#invite_recip_email2').val('');
                    $('#invite_recip_nick').val('');
                };
                //Clear values
                var firstinput = '';
                var lastinput = '';
                var emailinput = '';
                var emailinputmatch = '';
                var nickinput = '';
            });
        </script>
    </form>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-15214788-2']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</body>
</html>
