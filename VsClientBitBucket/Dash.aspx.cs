﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using VsClientBitBucket.Class;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using VsClientBitBucket.VoiceWebService;
using CheckBox = System.Web.UI.WebControls.CheckBox;
using Control = System.Web.UI.Control;


namespace VsClientBitBucket
{
    public partial class Dashboard : System.Web.UI.Page
    {
        string sOTKey = "";
        private const int GRIDCOLRECIPLIST = 11;
        private const int GRIDCOLMSGFILENAME = 7;
        private const int GRIDCOLMSGID = 8;
        private const int GRIDINVITEHIDE = 7;
        private const int GRIDINVITEID = 7;
        private readonly string _serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"];

        protected async void Page_Load(object sender, EventArgs e)
        {
            //to disable back button after logout
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            var tenantAudioSrc = new RegisterServiceClient();
            if (Session["UniqueKey"] == null)
            {
                Response.Redirect("~/Login.aspx", false);
            }
            else
            {
                sOTKey = Convert.ToString(Session["UniqueKey"]);
                //string tenant = HttpContext.Current.Request.Url.Host.ToString();
                string tenant = ConfigurationManager.AppSettings["Domain"];
                var user = new VsRestUser();
                var token = Session["AuthToken"] as string;
                const string uriType = "AUDIOSRC";

                var vsDash = new VsDash { Tenant = tenant, TenantType = uriType, OneTimeKey = sOTKey };

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_serviceUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = await client.PostAsJsonAsync("api/Message/DashPageLoadCall", vsDash);
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsAsync<VsRestUser>();
                        user = await result;
                    }
                }

                DataMsg audioSource = tenantAudioSrc.getAudioSrc(tenant, uriType, sOTKey);
                hidDynamicFileSrc.Value = audioSource.Msg;

                ////hidDynamicFileSrc.Value = "http://bellybuds.voiceshare.net:18080/VoiceShareNewWebService/recordings/";
                //string hiddenValue = hidMsgSelected.Value;
                ////Session["UniqueKey"] = null;
                //Session["UniqueKey"] = audioSource.OtKey;
                //sOTKey = audioSource.OtKey;

                //VoiceWebService.VS_Person fullUserName = tenantAudioSrc.GetUserName(sOTKey);
                //sOTKey = fullUserName.OTKey;

                Session["FullUserName"] = user.FirstName + " " + user.LastName;
                Session["UserFirstName"] = user.FirstName;
                Session["UserLastName"] = user.LastName;
                Session["Email"] = user.Email;
                Session["MessageCount"] = user.MessageCount;
                Session["LastMessageRecorded"] = user.LastMessageRecorded;
                //Session["InviteID"] = user.;
                Session["InviteCount"] = user.InviteCount;
                Session["LastInviteSent"] = user.LastInviteSent;
                Session["NetworkCounter"] = user.NetworkCounter;
                Session["WelcomeFlagged"] = user.WelcomeFlag;
                Session["Premium"] = user.PremiumFlag;
                Session["AcctCreation"] = user.AccountCreated;
                Session["InviteNudgeFlag"] = user.InviteNudgeFlag;
                Session["RecordNudgeFlag"] = user.RecNudgeFlag;
                Session["TosFlag"] = user.TosFlag;
                Session["MusicDownload"] = user.MusicDownloadFlag;
                Session["UniqueKey"] = sOTKey;

                spanCounter.InnerHtml = Session["NetworkCounter"].ToString();

                //VoiceWebService.VS_Person userInformation = tenantAudioSrc.PersonInfo(sOTKey);
                //sOTKey = userInformation.OTKey;
                //string firstName = userInformation.FirstName;
                //string lastName = userInformation.LastName;
                lbl_UserName.Text = (string)Session["FullUserName"];

                grdRecipientList.Visible = false;
                lblForward.Visible = false;
                var userList = user.SentList;
                userList.AddRange(user.RecvList);


                ////if (!Page.IsPostBack)
                ////{
                //VoiceWebService.MessageListNOtKey listSentMsg = tenantAudioSrc.GetMessageList(sOTKey, "SENT");
                grdMsgSent.DataSource = userList;
                grdMsgSent.DataBind();
                //sOTKey = listSentMsg.OtKey;
                ////}

                ////btn_Receive_Click(null, null);

                //VoiceWebService.MessageListNOtKey listRecvMsg = tenantAudioSrc.GetMessageList(sOTKey, "RECV");
                //grdMsgReceived.DataSource = user.RecvList;
                //grdMsgReceived.DataBind();
                //sOTKey = listRecvMsg.OtKey;

                //VoiceWebService.RecipientListNOtkey recipientsList = tenantAudioSrc.GetInvitePerInfo(sOTKey);
                //grdInvtCreated.DataSource = user.;
                //grdInvtCreated.DataBind();
                //sOTKey = recipientsList.OtKey;

                //// Populate Recipient Grid

                //VoiceWebService.SenderListNOtkey senders = tenantAudioSrc.SenderList(sOTKey, tenant);

                var senderList = user.SenderList;

                if (senderList != null && !IsPostBack)
                {
                    grdRecipientList.Visible = true;
                    grdRecipientList.DataSource = senderList;
                    grdRecipientList.DataBind();

                    grdSelMsgList.Visible = true;
                    grdSelMsgList.DataSource = senderList;
                    grdSelMsgList.DataBind();
                }
                //else
                //{
                //    /* else code needs to be added */
                //}
            }

            if (!Page.IsPostBack)
            {
                if (Session["WelcomeDisplayed"] == null)
                {
                    if ((string)Session["WelcomeFlagged"] == "False")
                    {
                        hidModalMsg.Value = String.IsNullOrWhiteSpace((string)Session["InviteID"]) ? "Sounds/WelcomeWalkUp.mp3" : "Sounds/WelcomeInvite.mp3";
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "displayModal", "Popup.showModal('modalWelcomePopup','wrapper','center center',{'constrainToScreen':true});", true);
                        Session["WelcomeDisplayed"] = new SessionParameter();
                    }
                    if ((string)Session["MusicDownload"] == "True")
                    {
                        lblMusic.Text =
                            "Congratulations! To receive your Rock-A-Belly music album, visit <a href= 'http://shop.bellybuds.com'>our store</a>, choose your album, and enter your same VoiceShare access code when prompted for a 'Discount Code' during checkout";
                    }
                    //Start nudge nested ifs here (first invite, then record. invite nudge must be dismissed before record nudge can popup)                   

                    //Rex commented this - 1/20/2016
                    //if ((string)Session["WelcomeFlagged"] == "True" && (string)Session["RecordNudgeFlag"] == "False")
                    //{
                    //    var lastInvite = (DateTime?)Session["LastInviteSent"];
                    //    var acctCreation = (DateTime)Session["AcctCreation"];
                    //    var inviteCount = (int)Session["InviteCount"];
                    //    const int numOfDays = 7;
                    //    var now = DateTime.Now;

                    //    var weekSinceLastInvite = lastInvite.HasValue
                    //                                    ? now >= lastInvite.Value.AddDays(numOfDays)
                    //                                    : now >= acctCreation.AddDays(numOfDays);

                    //    if (weekSinceLastInvite == true && Session["InviteNudgeFlag"].ToString() == "False")
                    //    {

                    //        //VoiceWebService.DataMsg inviteNudge = tenantAudioSrc.SetInvNudgeFlag(sOTKey);
                    //        //sOTKey = inviteNudge.OtKey;
                    //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "displayInviteModal", "Popup.showModal('modalInviteNudge','wrapper','center center',{'constrainToScreen':true});", true);
                    //        Session["UniqueKey"] = sOTKey;
                    //        return;
                    //    }

                    //    var lastMessage = (DateTime?)Session["LastMessageRecorded"];
                    //    var messageCount = (int)Session["MessageCount"];

                    //    var weekSinceLastMessage = lastMessage.HasValue
                    //                                    ? now >= lastMessage.Value.AddDays(numOfDays)
                    //                                    : now >= acctCreation.AddDays(numOfDays);

                    //    if (weekSinceLastMessage == true && Session["RecordNudgeFlag"].ToString() == "False")
                    //    {
                    //        //VoiceWebService.DataMsg recordNudge = tenantAudioSrc.RecNudgeFlag(sOTKey);
                    //        //sOTKey = recordNudge.OtKey;
                    //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "displayRecordModal", "Popup.showModal('modalRecordNudge','wrapper','center center',{'constrainToScreen':true});", true);
                    //        Session["UniqueKey"] = sOTKey;
                    //        return;
                    //    }
                    //}
                }
                if (Session["TosFlag"].ToString() == "False")
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "displayTosModal", "Popup.showModal('modalTosPopup','wrapper','center center',{'constrainToScreen':true});", true);
                }
            }
            Session["UniqueKey"] = sOTKey;
        }

        protected async Task lnkSignOut_Click(object sender, EventArgs e)
        {
            //Rex 12/28/2015 - Changed logout functionality to work with .Net authentication
            //FormsAuthentication.SignOut();
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(_serviceUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                var responseMessage = await httpClient.PostAsync("api/Account/Logout", null);
                var result = responseMessage.Content.ReadAsAsync<string>();

                //if (result.Equals("Ok"))
                //{
                    Response.Redirect("~/Signout.aspx", false);
                //}
            }
        }

        protected void grdMsgSent_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var premiumFlag = (int)Session["Premium"];
            // Add JS to Cells
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Click to highlight cell
                Control lnkSelect = e.Row.FindControl("lnkPlayImage");
                if (lnkSelect != null)
                {
                    e.Row.Cells[0].Attributes.Add("onclick", "ShowPlayModal(" + e.Row.RowIndex + ",0)");
                }
                if (premiumFlag == 0)
                {
                    lnkSelect = e.Row.FindControl("lnkDownImage");
                    if (lnkSelect != null)
                    {
                        e.Row.Cells[1].Attributes.Add("onclick", "ShowPremiumModal(" + e.Row.RowIndex + ",0)");
                    }
                }
                else
                {
                    lnkSelect = e.Row.FindControl("lnkDownImage");
                    if (lnkSelect != null)
                    {
                        e.Row.Cells[1].Attributes.Add("onclick", "ShowDownModal(" + e.Row.RowIndex + ",0)");
                    }
                }
                lnkSelect = e.Row.FindControl("lnkFwdImage");
                if (lnkSelect != null)
                {
                    e.Row.Cells[2].Attributes.Add("onclick", "ShowSelRecipModal(" + e.Row.RowIndex + ",0)");
                }

                lnkSelect = e.Row.FindControl("lnkDelImage");
                if (lnkSelect != null)
                {
                    e.Row.Cells[3].Attributes.Add("onclick", "ShowDelModal(" + e.Row.RowIndex + ",0)");
                }
                lnkSelect = e.Row.FindControl("lnkMagnifyImage");
                if (lnkSelect != null)
                {
                    e.Row.Cells[9].Attributes.Add("onclick", "ShowRecipListModal(" + e.Row.RowIndex + ",0,'" + e.Row.Cells[GRIDCOLRECIPLIST].Text + "')");
                }
                // Populate Hidden field with FileName
                HiddenField hidMsgId = (HiddenField)e.Row.FindControl("hidMsgId");
                hidMsgId.Value = e.Row.Cells[GRIDCOLMSGFILENAME].Text;
            }

            if (e.Row.Cells.Count >= GRIDCOLMSGID)
            {
                e.Row.Cells[GRIDCOLMSGFILENAME].Visible = false;
                e.Row.Cells[GRIDCOLMSGID].Visible = false;
                e.Row.Cells[GRIDCOLRECIPLIST].Visible = false;
            }
        }

        protected void grdMsgReceived_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var premiumFlag = (int)Session["Premium"];
            // Add JS to Cells
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Click to highlight cell
                Control lnkSelect = e.Row.FindControl("lnkPlayImage");
                if (lnkSelect != null)
                {
                    e.Row.Cells[0].Attributes.Add("onclick", "ShowPlayModal(" + e.Row.RowIndex + ",1)");

                }
                lnkSelect = e.Row.FindControl("lnkDownImage");
                if (premiumFlag == 0)
                {
                    lnkSelect = e.Row.FindControl("lnkDownImage");
                    if (lnkSelect != null)
                    {
                        e.Row.Cells[1].Attributes.Add("onclick", "ShowPremiumModal(" + e.Row.RowIndex + ",1)");
                    }
                }
                else
                {
                    lnkSelect = e.Row.FindControl("lnkDownImage");
                    if (lnkSelect != null)
                    {
                        e.Row.Cells[1].Attributes.Add("onclick", "ShowDownModal(" + e.Row.RowIndex + ",1)");
                    }
                }
                lnkSelect = e.Row.FindControl("lnkFwdImage");
                if (lnkSelect != null)
                {
                    e.Row.Cells[2].Attributes.Add("onclick", "ShowFwdModal(" + e.Row.RowIndex + ",1)");
                }
                lnkSelect = e.Row.FindControl("lnkDelImage");
                if (lnkSelect != null)
                {
                    e.Row.Cells[3].Attributes.Add("onclick", "ShowDelModal(" + e.Row.RowIndex + ",1)");
                }
                // Populate Hidden field with FileName
                HiddenField hidMsgId = (HiddenField)e.Row.FindControl("hidMsgId");
                hidMsgId.Value = e.Row.Cells[GRIDCOLMSGFILENAME].Text;
            }
            if (e.Row.Cells.Count >= GRIDCOLMSGID)
            {
                e.Row.Cells[GRIDCOLMSGFILENAME].Visible = false;
                e.Row.Cells[GRIDCOLMSGID].Visible = false;
            }
        }

        protected void grdInvtCreated_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Control lnkSelect = e.Row.FindControl("lnkResendInvite");
                if (lnkSelect != null)
                {
                    if (e.Row.Cells[6].Text == "Pending")
                    {
                        e.Row.Cells[0].Attributes.Add("onclick", "ShowResendModal(" + e.Row.RowIndex + ",2)");
                    }
                }
                lnkSelect = e.Row.FindControl("lnkCancelInvite");
                if (lnkSelect != null)
                {
                    if (e.Row.Cells[6].Text == "Pending")
                    {
                        e.Row.Cells[1].Attributes.Add("onclick", "ShowDelInvtModal(" + e.Row.RowIndex + ",2)");
                    }
                }
            }
        }

        protected void grdMsgReceived_RowCommand(object sender, GridViewCommandEventArgs e)
        {
        }

        protected void grdMsgForward_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[4].Visible = false;
            e.Row.Cells[5].Visible = false;
        }

        protected void grdRecipientList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            String sOTKey = Convert.ToString(Session["UniqueKey"]);

            // Get hidden fields
            int selectedMsgIndex = int.Parse(hidMsgSelected.Value);
            String selectedMsgId = "";
            string gridUsed = hidGridUsed.Value;

            if (gridUsed == "0")
            {   // this means sent message
                selectedMsgId = grdMsgSent.Rows[selectedMsgIndex].Cells[GRIDCOLMSGID].Text;
            }
            //else if (gridUsed == "1")
            //{   // this means received message
            //    selectedMsgId = grdMsgReceived.Rows[selectedMsgIndex].Cells[GRIDCOLMSGID].Text;
            //}

            // Get the selected index and the command name
            int selectedIndex = int.Parse(e.CommandArgument.ToString());
            string commandName = e.CommandName;

            if (commandName == "cmdSelClick" && gridUsed == "1")
            {
                string name = grdRecipientList.Rows[selectedIndex].Cells[1].Text;
                string userName = grdRecipientList.Rows[selectedIndex].Cells[2].Text;
                string fullName = name + "(" + userName + ")";
                var getTenantNPersonId = new RegisterServiceClient();

                var forwardMsgResult = getTenantNPersonId.ForwardMessage(userName, selectedMsgId, sOTKey);

                Session["UniqueKey"] = forwardMsgResult.OtKey;
                sOTKey = forwardMsgResult.OtKey;
            }
        }

        protected void grdMsgSent_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMsgSent.PageIndex = e.NewPageIndex;
            grdMsgSent.DataBind();
        }

        protected void grdSelMsgList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSelMsgList.PageIndex = e.NewPageIndex;
            grdSelMsgList.DataBind();
        }

        private string ConvertSortDirectionToSql(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }

            return newSortDirection;
        }

        protected void grdMsgSent_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dataTable = grdMsgSent.DataSource as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

                grdMsgSent.DataSource = dataView;
                grdMsgSent.DataBind();
            }

        }

        protected void btnDownloadFile_Click(object sender, EventArgs e)
        {
            // Get hidden fields
            int selectedMsgIndex = int.Parse(hidMsgSelected.Value);
            String selectedMsgId = "";
            String selectedMsgFileName = "";
            string gridUsed = hidGridUsed.Value;

            if (gridUsed == "0")
            {   // this means sent message
                selectedMsgId = grdMsgSent.Rows[selectedMsgIndex].Cells[GRIDCOLMSGID].Text;
                selectedMsgFileName = grdMsgSent.Rows[selectedMsgIndex].Cells[GRIDCOLMSGFILENAME].Text;
            }
            //else if (gridUsed == "1")
            //{   // this means received message
            //    selectedMsgId = grdMsgReceived.Rows[selectedMsgIndex].Cells[GRIDCOLMSGID].Text;
            //    selectedMsgFileName = grdMsgReceived.Rows[selectedMsgIndex].Cells[GRIDCOLMSGFILENAME].Text;
            //}

            var downloadSoundFile = new VoiceWebService.RegisterServiceClient();
            Stream fileStream = downloadSoundFile.GetFile(selectedMsgId);

            int i = 0;

            //using (FileStream writeStream = new FileStream("C:\\Users\\smalla\\Desktop\\recordings\\test10.mp3", FileMode.Create, FileAccess.Write))
            using (FileStream writeStream = new FileStream(selectedMsgFileName, FileMode.Create, FileAccess.Write))
            {
                const int chunkSize = 6500;
                byte[] buffer = new byte[chunkSize];
                int bytesRead = 1;
                while (bytesRead > 0)
                {
                    bytesRead = fileStream.Read(buffer, 0, chunkSize);
                    writeStream.Write(buffer, 0, bytesRead);
                    //Console.Write(bytesRead);
                    i++;
                }
            }
        }

        protected void btnDelMsgConfirm_Click(object sender, EventArgs e)
        {
            // Get hidden fields
            var selectedMsgIndex = int.Parse(hidMsgSelected.Value);
            String selectedMsgId = "";
            String selectedMsgFileName = "";
            string gridUsed = hidGridUsed.Value;

            if (gridUsed == "0")
            {   // this means sent message
                selectedMsgId = grdMsgSent.Rows[selectedMsgIndex].Cells[GRIDCOLMSGID].Text;
                selectedMsgFileName = grdMsgSent.Rows[selectedMsgIndex].Cells[GRIDCOLMSGFILENAME].Text;
            }
            //else if (gridUsed == "1")
            //{   // this means received message
            //    selectedMsgId = grdMsgReceived.Rows[selectedMsgIndex].Cells[GRIDCOLMSGID].Text;
            //    selectedMsgFileName = grdMsgReceived.Rows[selectedMsgIndex].Cells[GRIDCOLMSGFILENAME].Text;
            //}

            var messageHandler = new VoiceWebService.RegisterServiceClient();
            String retCode = messageHandler.MessageDelete(selectedMsgId);

            if (retCode == "Unsuccess")
            {
                lblDeleteMessage.Text = "Unable to delete the message at this time.";
            }
            else
            {
                Response.Redirect("~/Dash.aspx", false);
            }
        }

        //protected void grdInvtCreated_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    grdInvtCreated.PageIndex = e.NewPageIndex;
        //    grdInvtCreated.DataBind();
        //}

        //protected void grdMsgReceived_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    grdMsgReceived.PageIndex = e.NewPageIndex;
        //    grdMsgReceived.DataBind();
        //}

        //protected void grdMsgReceived_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    DataTable dataTable = grdMsgReceived.DataSource as DataTable;

        //    if (dataTable != null)
        //    {
        //        DataView dataView = new DataView(dataTable);
        //        dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

        //        grdMsgReceived.DataSource = dataView;
        //        grdMsgReceived.DataBind();
        //    }
        //}

        //protected void grdInvtCreated_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    DataTable dataTable = grdInvtCreated.DataSource as DataTable;

        //    if (dataTable != null)
        //    {
        //        DataView dataView = new DataView(dataTable);
        //        dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

        //        grdInvtCreated.DataSource = dataView;
        //        grdInvtCreated.DataBind();
        //    }
        //}

        //protected void cmdHideMsgRecv_Click(object sender, EventArgs e)
        //{
        //    if (grdMsgReceived.Visible == false)
        //    {
        //        grdMsgReceived.Visible = true;
        //        cmdHideMsgRecv.Text = "Hide";
        //    }
        //    else
        //    {
        //        grdMsgReceived.Visible = false;
        //        cmdHideMsgRecv.Text = "Show";
        //    }
        //}

        protected void cmdMsgSentHide_Click(object sender, EventArgs e)
        {
            if (grdMsgSent.Visible == false)
            {
                grdMsgSent.Visible = true;
                cmdMsgSentHide.Text = "Hide";
            }
            else
            {
                grdMsgSent.Visible = false;
                cmdMsgSentHide.Text = "Show";
            }
        }

        //protected void cmdMsgInvite_Click(object sender, EventArgs e)
        //{
        //    if (grdInvtCreated.Visible == false)
        //    {
        //        grdInvtCreated.Visible = true;
        //        cmdMsgInvite.Text = "Hide";
        //    }
        //    else
        //    {
        //        grdInvtCreated.Visible = false;
        //        cmdMsgInvite.Text = "Show";
        //    }
        //}

        protected void welcomeChkbox_CheckedChanged(object sender, EventArgs e)
        {
            var welcomeModalFlag = new RegisterServiceClient();
            var markWelcomeModalFlag = welcomeModalFlag.SetWelcomeModalFlag(sOTKey);
            sOTKey = markWelcomeModalFlag.OtKey;
            Session["UniqueKey"] = sOTKey;
        }

        //protected void btnResendInvt_Click(object sender, EventArgs e)
        //{
        //    string gridUsed = hidGridUsed.Value;
        //    var selectedMsgIndex = int.Parse(hidMsgSelected.Value);
        //    string inviteId = grdInvtCreated.Rows[selectedMsgIndex].Cells[GRIDINVITEID].Text;
        //    string recEmail = grdInvtCreated.Rows[selectedMsgIndex].Cells[2].Text;
        //    string recLastName = grdInvtCreated.Rows[selectedMsgIndex].Cells[3].Text;
        //    string recFirstName = grdInvtCreated.Rows[selectedMsgIndex].Cells[4].Text;
        //    DateTime sentDate = DateTime.Parse(grdInvtCreated.Rows[selectedMsgIndex].Cells[5].Text);

        //    var invite = new RegisterServiceClient();
        //    var resend = invite.ResendInvitation(sOTKey, inviteId, recEmail, recLastName, recFirstName, sentDate);
        //    Session["UniqueKey"] = resend.OtKey;
        //    sOTKey = resend.OtKey;

        //    string firstName = (string)Session["UserFirstName"];
        //    string lastName = (string)Session["UserLastName"];
        //    //Email Portion
        //    //MailSubject
        //    string mailSubject = "Hello " + recFirstName + ", " + firstName + " " + lastName + " has invited you to record a baby message!";
        //    //MailBody
        //    string mailBody = "<p>" + "Dear " + recFirstName + "," + "</p><br/>" + "<p>" + "You've been invited to join the VoiceShare** network at www.bellybuds.com." + "</p><p>" +
        //                        "Click this link to record your message(registration is quick, easy and free for invited users:):) " + resend.Msg + "</p><br/><p>" +
        //                        "Thank you!" + "</p><p>" + "-Team Bellybuds*" + "</p><br/><br/><br/>" + "<p><i>" + "*Bellybuds is specialized baby-bump sound system that safely plays sound to a baby in the womb." + "</i></p><i><p>" +
        //                        "**VoiceShare is an online service that allows user to record, send and download voice messages to and from others in their network." + "</i></p>";

        //    var sendEmail = invite.inviteEmail(recEmail, mailSubject, mailBody, sOTKey);

        //    Session["UniqueKey"] = sendEmail.OtKey;
        //    sOTKey = sendEmail.OtKey;

        //    if (sendEmail.Msg == null)
        //    {
        //        var delInvite = invite.deleteInviteData(sOTKey, recEmail, recLastName, recFirstName);

        //        Session["UniqueKey"] = delInvite.OtKey;
        //        Response.Redirect("Result.aspx?url=InviteFail");
        //    }

        //}

        //protected void btnCancelInvt_Click(object sender, EventArgs e)
        //{
        //    string gridUsed = hidGridUsed.Value;
        //    var selectedMsgIndex = int.Parse(hidMsgSelected.Value);
        //    string inviteId = grdInvtCreated.Rows[selectedMsgIndex].Cells[GRIDINVITEID].Text;

        //    var deleteInviteServCall = new VoiceWebService.RegisterServiceClient();
        //    var deleteInvite = deleteInviteServCall.DeleteDashInviteData(sOTKey, inviteId);
        //    sOTKey = deleteInvite.OtKey;
        //    Session["UniqueKey"] = sOTKey;
        //    Response.Redirect("Dash.aspx");
        //}

        protected void btnSelRecip_Click(object sender, EventArgs e)
        {
            String sOTKey = Convert.ToString(Session["UniqueKey"]);
            var getTenantNPersonId = new RegisterServiceClient();
            // Get hidden fields
            int selectedMsgIndex = int.Parse(hidMsgSelected.Value);
            var selectedMsgId = grdMsgSent.Rows[selectedMsgIndex].Cells[GRIDCOLMSGID].Text;
            string gridUsed = hidGridUsed.Value;

            for (int i = 0; i < grdSelMsgList.Rows.Count; i++)
            {
                var chkSelect = grdSelMsgList.Rows[i].FindControl("chkSelect") as CheckBox;
                if (chkSelect == null || !chkSelect.Checked) continue;
                string name = grdSelMsgList.Rows[i].Cells[1].Text;
                string userName = grdSelMsgList.Rows[i].Cells[2].Text;
                string fullName = name + "(" + userName + ")";

                //Rex edited line 706: first argument in method was userName, but changed it to name. 'name' is in fact the user's email address
                DataMsg forwardMsgResult = getTenantNPersonId.ForwardMessage(name, selectedMsgId, sOTKey);
                sOTKey = forwardMsgResult.OtKey;
                Session["UniqueKey"] = forwardMsgResult.OtKey;
            }
            Response.Redirect("~/Dash.aspx", false);
        }

        protected void btnTosSubmit_Click(object sender, EventArgs e)
        {
            if (chkTos.Checked)
            {
                var tosCall = new RegisterServiceClient();
                tosCall.TosFlag(sOTKey);
            }
            else
            {
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                //lblTerms.Text = "You must agree to the Terms of Service.";
            }
        }
    }
}