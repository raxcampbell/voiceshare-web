﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="EditForm.aspx.cs" Inherits="VsClientBitBucket.EditForm" %>

<%@ Import Namespace="VsClientBitBucket.VoiceWebService" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Styles/Custom2.css" rel="stylesheet" />
    <link href="Styles/Container.css" rel="stylesheet" />
    <%--<script src="scripts/jquery-2.1.0.min.js"></script>--%>
    <script src="scripts/jquery-1.12.0.min.js"></script>
    <script src="scripts/jquery-ui-1.10.3.min.js"></script>
    <style type="text/css">
        table.middle {
            margin-left: auto;
            margin-right: auto;
            padding-top: 100px;
        }

        .auto-style1 {
            width: 100%;
            table-layout: fixed;
        }

        .hide {
            display: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <div id="page">
                <div id="header">
                    <a style="float: left" href="http://voiceshare.wavhello.com/Dash.aspx">
                        <asp:Image ID="imgLogo" runat="server" ImageUrl="images/LoginAndRegister/voiceshare_by_wavhello_logo_transparent_320x114.png" />
                    </a>
                    <!-- iphone icons -->
                    <div class="icons">
                        <div class="nav-icon">
                            <asp:ImageButton ID="btnFacebook" runat="server" PostBackUrl="http://www.facebook.com/wavhello" ImageUrl="images/facebook.png"></asp:ImageButton>
                        </div>
                        <div class="nav-icon">
                            <asp:ImageButton ID="btnTwitter" runat="server" PostBackUrl="http://www.twitter.com/wavhello" ImageUrl="images/twitter.png"></asp:ImageButton>
                        </div>
                    </div>
                    <!-- /end iphone icons -->
                </div>
                <div class="content">
                    <div class="outside" style="margin-top: 50px;">
                        <div class="inside" style="overflow: hidden">
                            <div class="inside_full">
                                <div style="text-align: right; font-size: 10px">
                                    (Not "<asp:Label ID="lbl_UserName" runat="server" />
                                    "?,<a href="Login.aspx">Sign In</a>)
                                </div>
                                <ul id="profile_bar">
                                    <li><a href="Record.aspx">Record a Message</a></li>
                                    <li><a href="Invite.aspx">Send an Invitation</a></li>
                                    <li><a href="Dash.aspx">My Dashboard</a></li>
                                    <li><a href="EditForm.aspx">Edit User Profile</a></li>
                                    <li><a href="FAQ.aspx">FAQ</a></li>
                                    <li><strong>You have <span runat="server" style="background-color: red; color: white; -ms-border-radius: 50%; border-radius: 50%; padding: 2px;" id="spanCounter"></span>connections.</strong></li>
                                </ul>
                                <div class="clear_all;"></div>
                            </div>
                            <div class="inside_full">
                                <div class="fboxes" style="text-align: center">
                                    Feel free to update your user profile.
                                </div>
                                <div class="clear_all"></div>
                            </div>
                            <div class="inside_full">
                                <div class="fboxes">
                                    <table style="width: 95%" aria-hidden="true">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFirstName" runat="server" Text="First Name"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblLastName" runat="server" Text="Last Name"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMiddleName" runat="server" Text="Middle Name"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAge" runat="server" Text="Age"></asp:Label>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtFirst" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtLast" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMiddle" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlGender" runat="server" Width="108px" Style="font-size: 12px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', Arial, Verdana, sans-serif; text-align: center;">
                                                    <asp:ListItem>--Select One--</asp:ListItem>
                                                    <asp:ListItem Value="M">Male</asp:ListItem>
                                                    <asp:ListItem Value="F">Female</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAge" runat="server"></asp:TextBox>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblAddrType" runat="server" Text="Address Type"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAddr1" runat="server" Text="Address"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblZIP" runat="server" Text="Postal Code"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCountryCd" runat="server" Text="Country Code"></asp:Label>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:DropDownList
                                                    ID="ddlAddrType"
                                                    runat="server"
                                                    Width="108px"
                                                    Style="font-size: 12px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', Arial, Verdana, sans-serif; text-align: center;">
                                                    <asp:ListItem>--Select One--</asp:ListItem>
                                                    <asp:ListItem Value="H">Home</asp:ListItem>
                                                    <asp:ListItem Value="W">Work</asp:ListItem>
                                                    <asp:ListItem Value="O">Other</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddr" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPostal" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:DropDownList
                                                    ID="ddlCountryCd"
                                                    runat="server"
                                                    DataTextField="CountryName"
                                                    DataValueField="CountryCode"
                                                    AppendDataBoundItems="true"
                                                    Style="font-size: 12px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', Arial, Verdana, sans-serif; text-align: center;"
                                                    Width="150px">
                                                    <asp:ListItem
                                                        Value="UNS"
                                                        Text="--Select One--"
                                                        Selected="True">
                                                    </asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:LinkButton
                                                    ID="lnkUpdateInfo"
                                                    runat="server"
                                                    ForeColor="#75CE7E"
                                                    Style="text-decoration: none"
                                                    Font-Bold="True"
                                                    CausesValidation="false"
                                                    OnClick="lnkUpdateInfo_Click">Update Info</asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="clear_all"></div>
                            </div>
                            <div class="inside_full">
                                <div class="editmsg">
                                    <asp:Label ID="lblSubmitStatus" runat="server"></asp:Label>
                                </div>
                                <div class="clear_all"></div>
                            </div>
                            <!--Email-->
                            <div class="inside_full">
                                <div class="fboxes">
                                    <div>
                                        <table class="auto-style1">
                                            <tr>
                                                <td>
                                                    <asp:GridView
                                                        ID="grdConnectionList"
                                                        runat="server"
                                                        AutoGenerateColumns="False"
                                                        EmptyDataText="You currently have no connections."
                                                        AllowPaging="True"
                                                        AllowSorting="True"
                                                        ShowFooter="True"
                                                        PageIndex="4">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkConnect" runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Connections" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblConnection" runat="server" Text="<%# ((VS_Person) Container.DataItem).FullName %>"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Id" DataField="PersonId" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                                        </Columns>
                                                    </asp:GridView>
                                                    <asp:LinkButton ID="lnkBtnConnect" runat="server" ForeColor="#75CE7E" Style="text-decoration: none" Font-Bold="True" CausesValidation="false" OnClick="lnkBtnConnect_Click">Delete Connections</asp:LinkButton>
                                                    <asp:Label ID="lblConnect" runat="server" ForeColor="#75CE7E"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblOldPass" runat="server" Text="Old password: "></asp:Label>
                                                    <br />
                                                    <asp:TextBox ID="txtOldPass" runat="server" TextMode="Password"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valReqOldPass"
                                                        runat="server"
                                                        ErrorMessage="Old password a required field."
                                                        Text="*"
                                                        ForeColor="#75CE7E"
                                                        ControlToValidate="txtOldPass"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator
                                                        ID="compValOldNewPass"
                                                        runat="server"
                                                        Text="*"
                                                        ForeColor="#75CE7E"
                                                        ErrorMessage="The old and new passwords cannot be the same."
                                                        ControlToCompare="txtOldPass"
                                                        ControlToValidate="txtNewPass"
                                                        Display="Dynamic"
                                                        Operator="NotEqual"></asp:CompareValidator>
                                                    <br />
                                                    <div>
                                                        <asp:Label ID="lblNewPass" runat="server" Text="New password: "></asp:Label>
                                                    </div>
                                                    <asp:TextBox ID="txtNewPass" runat="server" TextMode="Password"></asp:TextBox>
                                                    <asp:CompareValidator
                                                        ID="compValidPassword"
                                                        runat="server"
                                                        ErrorMessage="The new passwords entered do not match."
                                                        Text="*"
                                                        ForeColor="#75CE7E"
                                                        ControlToCompare="txtNewPass"
                                                        ControlToValidate="txtConfirmPass"> </asp:CompareValidator>
                                                    <asp:RequiredFieldValidator ID="valReqNewPass"
                                                        runat="server"
                                                        ErrorMessage="New password is a required field."
                                                        Text="*"
                                                        ForeColor="#75CE7E"
                                                        ControlToValidate="txtNewPass"> </asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="valRegex"
                                                        runat="server"
                                                        ErrorMessage="New password cannot contain white space, forward, or backward slashes."
                                                        Text="*"
                                                        ForeColor="#75CE7E"
                                                        ControlToValidate="txtNewPass"
                                                        ValidationExpression='^[^\s\\\/]+$'> </asp:RegularExpressionValidator>
                                                    <asp:RegularExpressionValidator
                                                        ID="valRegExUpperCase"
                                                        runat="server"
                                                        ErrorMessage="Password requires at least one uppercase letter, at least one number, and at least 6 characters"
                                                        ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$"
                                                        Display="Static"
                                                        ControlToValidate="txtNewPass"
                                                        Text="*"
                                                        ForeColor="#75CE7E">
                                                    </asp:RegularExpressionValidator>
                                                    <br />
                                                    <div>
                                                        <asp:Label ID="lblConfirmPass" runat="server" Text="Confirm new password: "></asp:Label>
                                                    </div>
                                                    <asp:TextBox ID="txtConfirmPass" runat="server" TextMode="Password"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="reqValPassConfirm"
                                                        runat="server"
                                                        ErrorMessage="Confirm new password a required field."
                                                        Text="*"
                                                        ForeColor="#75CE7E"
                                                        ControlToValidate="txtConfirmPass"> </asp:RequiredFieldValidator>
                                                    <br />
                                                    <asp:LinkButton
                                                        ID="lnkReset"
                                                        runat="server"
                                                        ForeColor="#75CE7E"
                                                        Style="text-decoration: none"
                                                        Font-Bold="True"
                                                        OnClick="lnkReset_Click">Reset</asp:LinkButton>
                                                    <br />
                                                    <br />
                                                    <asp:Label ID="lblPasswordMsg" runat="server"></asp:Label>
                                                    <asp:ValidationSummary ID="valSummary"
                                                        runat="server"
                                                        ForeColor="#75CE7E"
                                                        HeaderText="The following errors occured:"
                                                        EnableClientScript="true"
                                                        DisplayMode="List" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="outside">
                        <div class="inside footer">
                            <!--BEGIN FOOTER -->
                            <div class="container footbox">
                                <div class="footInner" style="margin-left: 18%;">
                                    <div class="page"><strong>Pages:</strong></div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypHome" href="http://www.wavhello.com/" runat="server">Home</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypContact" href="http://wavhello.com/pages/contact-us" runat="server">Contact Us</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypTerms" NavigateUrl="http://www.wavhello.com/terms-of-use" runat="server">Terms of Service</asp:HyperLink>
                                    </div>
                                </div>
                                <div class="footInner">
                                    <div class="stay"><strong>Stay In Touch:</strong></div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypRSS" href="http://www.bellybuds.com/feed/" runat="server">Site RSS Feed</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypTwitter" href="http://www.twitter.com/wavhello" runat="server">Twitter</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypFacebook" href="http://www.facebook.com/wavhello" runat="server">Facebook</asp:HyperLink>
                                    </div>
                                </div>
                                <div style="margin-left: -30px;"><strong>&copy; 2016 BellyBuds</strong></div>
                            </div>
                            <!--END OF FOOTER -->
                            <div class="clear_all"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-15214788-2']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</body>
</html>
