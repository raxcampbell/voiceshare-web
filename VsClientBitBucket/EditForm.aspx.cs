﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI.WebControls;
using LinqToTwitter;
using VsClientBitBucket.Class;
using VsClientBitBucket.VoiceWebService;
using DataMsg = VsClientBitBucket.SecureVoiceWebService.DataMsg;

namespace VsClientBitBucket
{
    public partial class EditForm : System.Web.UI.Page
    {
        private AspNetAuthorizer auth;
        private TwitterContext twitterCtx;
        long fbAppId = 453176514805087;
        string fbAppSecret = "80d4fcbc476545f1dada235f2b253730";
        private readonly string _serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"];

        protected void Page_Load(object sender, EventArgs e)
        {
            //to disable back button after logout
            
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            lbl_UserName.Text = Convert.ToString(Session["FullUserName"]);

            if (Session["UniqueKey"] == null)
            {
                Response.Redirect("~/Login.aspx", false);
            }
            else
            {// test getuser to make sure != null
                if (!IsPostBack)
                {
                    string sOTKey = "";
                    sOTKey = Convert.ToString(Session["UniqueKey"]);
                    string tenant = HttpContext.Current.Request.Url.Host.ToString();

                    var userGet = new RegisterServiceClient();
                    #region User
                    VS_Person personalInfo = userGet.GetUserInfo(sOTKey);
                    txtFirst.Text = personalInfo.FirstName;
                    txtLast.Text = personalInfo.LastName;
                    txtMiddle.Text = personalInfo.MiddleName;
                    ddlGender.SelectedValue = personalInfo.Gender;
                    txtAge.Text = personalInfo.Age.ToString();
                    spanCounter.InnerHtml = Session["NetworkCounter"].ToString();

                    sOTKey = personalInfo.OTKey;
                    #endregion

                    # region Address
                    VS_PersonAddress getAddress = userGet.GetUserAddress(sOTKey);
                    ddlAddrType.SelectedValue = getAddress.Addresstype;
                    txtAddr.Text = getAddress.Address1;
                    txtCity.Text = getAddress.City;
                    txtPostal.Text = getAddress.Postal;
                    ddlCountryCd.SelectedValue = getAddress.Country;

                    sOTKey = getAddress.OTKey;
                    #endregion

                    ConnectionList userConnections = userGet.GetConnectionList(sOTKey);
                    grdConnectionList.DataSource = userConnections.Connections;
                    grdConnectionList.DataBind();

                    sOTKey = userConnections.OTKey;
                    Session["UniqueKey"] = sOTKey;

                    var utilsCall = new UtilsWebService.UtilsClient();
                    UtilsWebService.CountryList countryDropDown = utilsCall.EditFormCountryDropDown();
                    ddlCountryCd.DataSource = countryDropDown.List;
                    ddlCountryCd.DataBind();
                }
            }
        }

        protected void lnkUpdateInfo_Click(object sender, EventArgs e)
        {
            string sOTKey = "";
            sOTKey = Convert.ToString(Session["UniqueKey"]);
            string Tenant = HttpContext.Current.Request.Url.Host.ToString();

            string lastName = txtLast.Text;
            string firstName = txtFirst.Text;
            string middleName = txtMiddle.Text;
            string gender = ddlGender.SelectedValue;
            int age = int.Parse(txtAge.Text);
            DateTime birthDate = DateTime.Now;

            var userUpdate = new RegisterServiceClient();
            VoiceWebService.DataMsg updateInfo = userUpdate.UpdatePersonInfo(sOTKey, lastName, firstName, middleName, gender, age, birthDate);

            sOTKey = updateInfo.OtKey;

            if (updateInfo.Msg == "Success")
            {

            }
            else
            {
                lblSubmitStatus.Text = "One or more fields of your personal information are incorrect.";
            }

            string addressType = ddlAddrType.SelectedValue;
            string address1 = txtAddr.Text;
            string city = txtCity.Text;
            string postal = txtPostal.Text;
            string countryCd = ddlCountryCd.SelectedValue;

            VoiceWebService.DataMsg addUp = userUpdate.updatePersonAddress(sOTKey, addressType, address1, city, postal, countryCd);

            sOTKey = addUp.OtKey;
            Session["UniqueKey"] = sOTKey;

            lblSubmitStatus.Text = addUp.Msg == "Success" ? "Your changes have been successfully submitted" : "One or more fields of your address are incorrect.";
        }

        protected async void lnkReset_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtOldPass.Text) && !String.IsNullOrEmpty(txtNewPass.Text) && !String.IsNullOrEmpty(txtConfirmPass.Text))
            {
                var token = Session["AuthToken"] as string;
                var changePassword = new ChangePasswordViewModel();
                changePassword.OldPassword = txtOldPass.Text;
                changePassword.NewPassword = txtNewPass.Text;
                changePassword.ConfirmPassword = txtConfirmPass.Text;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_serviceUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = await client.PostAsJsonAsync("api/Account/ChangePassword", changePassword);
                    lblPasswordMsg.Text = response.IsSuccessStatusCode ? "Your password has been successfully updated." : "There was an error changing your password. ";
                } 
            }
            //string sOTKey = "";
            //sOTKey = Convert.ToString(Session["UniqueKey"]);
            //string tenant = HttpContext.Current.Request.Url.Host.ToString();

            //var passwordReset = new SecureVoiceWebService.RegisterServiceClient();

            //string password = txtOldPass.Text;
            //string newPassword = txtNewPass.Text;
            //string confirmPass = txtConfirmPass.Text;

            //SecureVoiceWebService.DataMsg changePassword = passwordReset.ResetPassword(sOTKey, password, newPassword, confirmPass);

            //sOTKey = changePassword.OtKey;
            //Session["UniqueKey"] = sOTKey;

            //if (changePassword.Msg == "Success" & txtOldPass.Text != txtNewPass.Text)
            //{
            //    lblPasswordMsg.Text = "Your password has been successfully updated.";
            //}
            //else
            //{
            //    lblPasswordMsg.Text = "Your password did not save successfully.";
            //}
        }

        protected void lnkBtnConnect_Click(object sender, EventArgs e)
        {
            string sOTKey = "";
            sOTKey = Convert.ToString(Session["UniqueKey"]);
            string tenant = HttpContext.Current.Request.Url.Host.ToString();

            var manageConnections = new SecureVoiceWebService.RegisterServiceClient();

            for (int i = 0; i < grdConnectionList.Rows.Count; i++)
            {
                int personId = int.Parse(grdConnectionList.Rows[i].Cells[2].Text);
                var chkConnect = grdConnectionList.Rows[i].FindControl("chkConnect") as CheckBox;
                if (chkConnect != null && chkConnect.Checked)
                {
                    DataMsg delConnResponse = manageConnections.DeleteConnections(sOTKey, personId);

                    Session["UniqueKey"] = delConnResponse.OtKey;
                    sOTKey = delConnResponse.OtKey;
                }
            }
            Response.Redirect("~/EditForm.aspx", false);
        }
    }
}