﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomError.aspx.cs" Inherits="VsClientBitBucket.ErrorPage.CustomError" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/Styles/Custom2.css"  rel="stylesheet" />
     <link href="~/Styles/StyleSheet.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <div id="fg_membersite">
        <form id="frmException" runat="server">
        <div>
            <fieldset>
                <legend>OOPS!</legend>
                <br />
                  <div>
                     <asp:Label ID ="Lbl_Exception" runat="server" Font-Bold="True">
                         An unexpeted error occured on our website. The website administrator has been notified.
                    </asp:Label>
                </div>
                <br />
                <br />
                <div>
                    <asp:HyperLink ID="hypLogin" runat="server" NavigateUrl="~/Login.aspx">Try to Login Again?</asp:HyperLink>
                </div>
                 <div>
                    <asp:HyperLink ID="hypLogout" runat="server" NavigateUrl="~/Signout.aspx">Logout?</asp:HyperLink>
                </div>
                <div>
                    <asp:HyperLink ID="hypForgot" runat="server" NavigateUrl="~/Forgot.aspx">Contact US</asp:HyperLink>
                </div>              
            </fieldset>

        </div>
        </form>
    </div>
</body>
</html>
