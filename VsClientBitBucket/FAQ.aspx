﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FAQ.aspx.cs" Inherits="VsClientBitBucket.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Belly Buds: Voiceshare Application</title>
    <link href="Styles/Custom2.css" rel="stylesheet" />
    <link href="Styles/Container.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <div id="page">
                <div id="header">
                    <a style="float: left" href="http://voiceshare.wavhello.com/Dash.aspx">
                        <asp:Image ID="imgLogo" runat="server" ImageUrl="images/LoginAndRegister/voiceshare_by_wavhello_logo_transparent_320x114.png" />
                    </a>
                    <div class="icons">
                        <div class="nav-icon">
                            <asp:ImageButton ID="btnFacebook" runat="server" PostBackUrl="http://www.facebook.com/wavhello" ImageUrl="images/facebook.png"></asp:ImageButton>
                        </div>
                        <div class="nav-icon">
                            <asp:ImageButton ID="btnTwitter" runat="server" PostBackUrl="http://www.twitter.com/wavhello" ImageUrl="images/twitter.png"></asp:ImageButton>
                        </div>
                    </div>
                </div>
                <div id="content">
                    <div class="outside">
                        <div class="inside">
                            <div class="inside_full">
                                <div style="text-align: right; font-size: 10px">
                                    (Not "<asp:Label ID="lbl_UserName" runat="server" />"?, <a href="Login.aspx">Sign In</a>)
                                </div>
                                <ul id="profile_bar">
                                    <li><a href="Record.aspx">Record a Message</a></li>
                                    <li><a href="Invite.aspx">Send an Invitation</a></li>
                                    <li><a href="Dash.aspx">My Dashboard</a></li>
                                    <li><a href="EditForm.aspx">Edit User Profile</a></li>
                                    <li><a href="FAQ.aspx">FAQ</a></li>
                                    <li><strong>You have <span runat="server" style="background-color: red; color: white; -ms-border-radius: 50%; border-radius: 50%; padding: 2px;" id="spanCounter"></span>connections.</strong></li>
                                </ul>
                            </div>
                            <div id="FaqWrapper">
                                <div class="inside_full">
                                    <div data-reamaze-embed="kb"></div>
                                    <%--<div class="inside_left" style="width: 100%">
                                        <h3>VoiceShare Frequently Asked Questions</h3>
                                        <p>
                                            <span style="font-family: 'sans-serif'; font-size: 12pt; font-style: italic; color: #000000"><strong>What are the requirements for Voiceshare to work correctly?</strong></span>
                                        </p>
                                        <p>
                                            <span style="font-size: 9.8pt; color: #000000">Voiceshare was developed with cross-browser capability and should function correctly in IE, Firefox, Chrome, Safari and Opera. In order to record, you need to be using a device that accepts Adobe Flash (http://get2.adobe.com/flashplayer/). At this time, Apple IOS products (such as the iPhone and iPad) do not support Adobe Flash technology. You may, however, use these devices to play and listen to your recordings.</span>
                                        </p>
                                        <p>
                                            <span style="font-family: 'sans-serif'; font-size: 12pt; font-style: italic; color: #000000"><strong>How do I get my recordings into my itunes?</strong></span>
                                        </p>
                                        <p>
                                            <span style="font-size: 9.8pt; color: #000000">First, locate the Voiceshare message on your Dashboard and click the pink downward arrow which will either download the message or else open it in a browser window (depending on which browser you are using); right-click the message and choose "Save As" to save it to a folder on your computer.  You may also right-click the pink downward arrow, choose "Save Target As" or "Save Link As" or "Download Linked File" (again, depending on which browser you are using) and save to a folder on your computer. Open ITunes, Click Edit>Preferences. Go to the Advanced tab. Make sure that "Copy files to ITunes Media folder when adding to Library" is checked. If it isn't do it now. Click OK. Now click File>Add file to library. A dialog should pop up. Open up the folder to which you saved your Voiceshare message. Click import, it should bring it into the Library.</span>
                                        </p>
                                        <p>
                                            <span style="font-family: 'sans-serif'; font-size: 12pt; font-style: italic; color: #000000"><strong>What kind of message should I record?</strong></span>
                                        </p>
                                        <p>
                                            <span style="font-size: 9.8pt; color: #000000">You can say whatever you like!  A baby in the womb can hear at about 20 weeks and they start to have short term memories at about 30 weeks.  If you’d like some suggestions on what to record we’ve jotted down some ideas <a href="/WhatToSay.aspx">here.</a> And for more information about what a baby is able to learn in the womb, click <a href="http://wavhello.com/pages/help#research">here.</a></span>
                                        </p>
                                        <p>
                                            <span style="font-family: 'sans-serif'; font-size: 12pt; font-style: italic; color: #000000"><strong>I'm sending out invitations, why is nobody accepting them?</strong></span>
                                        </p>
                                        <p>
                                            <span style="font-size: 9.8pt; color: #000000">Privacy and security settings vary for each browser or email hosting service.  It may be that your invite was delivered to your invitee’s spam folder.  If possible, reach out to your friend or family member and let them know to keep an eye out for your invite from info@voiceshare.net.</span>
                                        </p>
                                        <p>
                                            <span style="font-family: 'sans-serif'; font-size: 12pt; font-style: italic; color: #000000"><strong>What is Premium Membership?</strong></span>
                                        </p>
                                        <p>
                                            <span style="font-size: 9.8pt; color: #000000">At this point, premium membership is reserved for those users who register with an access code that comes included with a purchase of the Bellybuds Baby-Bump Sound System. The main difference between regular and premium membership is the ability to download voice messages.</span>
                                        </p>
                                        <p>
                                            <span style="font-family: 'sans-serif'; font-size: 12pt; font-style: italic; color: #000000"><strong>I never received my email with instructions to download the free music album - What gives?</strong></span>
                                        </p>
                                        <p>
                                            <span style="font-size: 9.8pt; color: #000000">The free download comes with a purchase of the Bellybuds Baby-Bump Sound System Deluxe.  If you entered the correct access code and never received your email instructions (check your spam, too) then send an email to support@wavhello.com and include a description of your issue along with the email address you used to register with VoiceShare and we will get back with promptly with download instructions.</span>
                                        </p>
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                        <div class="clear_all"></div>
                    </div>
                    <div class="outside">
                        <div class="inside footer">
                            <!--BEGIN FOOTER -->
                            <div class="container footbox">
                                <div class="footInner" style="margin-left: 18%;">
                                    <div class="page"><strong>Pages:</strong></div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypHome" href="http://www.wavhello.com/" runat="server">Home</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypContact" href="http://wavhello.com/pages/contact-us" runat="server">Contact Us</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypTerms" NavigateUrl="http://www.wavhello.com/terms-of-use" runat="server">Terms of Service</asp:HyperLink>
                                    </div>
                                </div>
                                <div class="footInner">
                                    <div class="stay"><strong>Stay In Touch:</strong></div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypRSS" href="http://www.bellybuds.com/feed/" runat="server">Site RSS Feed</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypTwitter" href="http://www.twitter.com/wavhello" runat="server">Twitter</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypFacebook" href="http://www.facebook.com/wavhello" runat="server">Facebook</asp:HyperLink>
                                    </div>
                                </div>
                                <div style="margin-left: -30px;"><strong>&copy; 2016 BellyBuds</strong></div>
                            </div>
                            <!--END OF FOOTER -->
                            <div class="clear_all"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="https://d3itxuyrq7vzpz.cloudfront.net/assets/reamaze.js"></script>
    <script type="text/javascript">
        var _support = _support || { 'ui': {}, 'user': {} };
        _support['account'] = 'wavhello';
    </script>

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-15214788-2']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</body>
</html>
