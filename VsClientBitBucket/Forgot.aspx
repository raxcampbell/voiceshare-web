﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Forgot.aspx.cs" Inherits="VsClientBitBucket.Forgot" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Styles/Custom2.css" rel="stylesheet" />
    <link href="Styles/StyleSheet.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/modernizr-2.6.2.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id='fg_membersite'>
                <fieldset>
                    <legend>Forgot Password?</legend>
                    <div class='short_explanation'><strong>Enter registered email address below</strong></div>
                    <div><span class='error'></span></div>
                    <div class='container'>
                        <asp:Label ID="lblEmail" runat="server" Text="Email Address:"></asp:Label><br />
                        <asp:TextBox ID="txt_Email" runat="server" MaxLength="50"></asp:TextBox><br />
                        <span id='login_username_errorloc' class='error'></span>
                    </div>
                    <div class='container'>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                    </div>
                </fieldset>
            </div>
        </div>
    </form>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-15214788-2']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</body>
</html>
