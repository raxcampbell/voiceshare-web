﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using VsClientBitBucket.Class;

namespace VsClientBitBucket
{
    public partial class Forgot : System.Web.UI.Page
    {
        private readonly string _serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"];
        protected void Page_Load(object sender, EventArgs e)
        {
            string forgotPasswordId = Request.QueryString["id"];

            //string url = Request.Url.AbsoluteUri;
            //if (url.Contains("https://"))
            //{
            //    Response.Redirect(url.Replace("https://", "http://"));
            //}

        }

        public async Task ProcessChangeRequest(string forgotPasswordId)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(_serviceUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                var responseMessage = await httpClient.PostAsync("api/Forgot/Change", new FormUrlEncodedContent(
                        new[] {
                        new KeyValuePair < string, string > ("forgotPasswordId", forgotPasswordId)
                    }
                        ));

                var result = await responseMessage.Content.ReadAsAsync<UpdatePassViewModel>();
                SubmitChange(result.E, result.P);
            }
        }

        public async Task SubmitChange(string email, string password)
        {
            try
            {
                var token = "";
                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(_serviceUrl);
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                    var responseMessage = await httpClient.PostAsync("Token", new FormUrlEncodedContent(
                        new[] {
                        new KeyValuePair < string, string > ("grant_type", "password"),
                        new KeyValuePair < string, string > ("username", email),
                        new KeyValuePair < string, string > ("password", password)
                    }

                        ));
                    var tokenModel = await responseMessage.Content.ReadAsAsync<TokenModel>();
                    token = tokenModel.AccessToken;
                }

                Session["AuthToken"] = token;
            }
            catch (Exception)
            {
                return;
                //throw;
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(_serviceUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));


            }
            //string Tenant = HttpContext.Current.Request.Url.Host.ToString();
            //VoiceWebService.RegisterServiceClient passWordRecovery = new VoiceWebService.RegisterServiceClient();
            //string Password = passWordRecovery.ForgetPassword(txt_Email.Text, Tenant);
            //if (Password == "Success")
            //{
            //    Response.Redirect("Result.aspx?url=passwordRecovery");
            //}
        }
    }
}