﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;

namespace VsClientBitBucket
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery",
                new ScriptResourceDefinition
            {
                Path = "~/Scripts/jquery-1.7.2.min.js"
            }
            );
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // //Get the error details
            //HttpException lastErrorWrapper = Server.GetLastError() as HttpException;

            //Exception lastError = lastErrorWrapper;
            //if (lastErrorWrapper.InnerException != null)
            //    lastError = lastErrorWrapper.InnerException;

            //string lastErrorTypeName = lastError.GetType().ToString();
            //string lastErrorMessage = lastError.Message;
            //string lastErrorStackTrace = lastError.StackTrace;

            //string URL = Request.RawUrl.ToString();


            //string UPLOAD_DIRECTORY = "Exception/";
            //string uploadDirectory = HttpContext.Current.Server.MapPath(UPLOAD_DIRECTORY);
            ////if (!Directory.Exists(uploadDirectory))
            ////{
            ////    Directory.CreateDirectory(uploadDirectory);
            ////}                  
            //string exceptionFile = "exceptionlog.txt";
            //string uploadFile = Path.Combine(uploadDirectory, exceptionFile);
            //string New = "New";

            //Exception exception1 = Server.GetLastError();
            //string exception = exception1.ToString();
            //string[] ExceptionOccur = { " ", New, "Date:", DateTime.Now.ToString(), "Url", URL, "Exception Type:", lastErrorTypeName, "Message:", lastErrorMessage, "Details:", lastErrorStackTrace };
            //File.AppendAllLines(uploadFile, ExceptionOccur);
       

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }        

        protected void Application_End(object sender, EventArgs e)
        {
            

        }
    }
}