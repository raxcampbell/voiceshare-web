﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Invite.aspx.cs" Inherits="VsClientBitBucket.Invite" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Belly Buds: Voiceshare Application</title>
    <link href="Styles/Custom2.css" rel="stylesheet" />
    <link href="Styles/Container.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style5 {
            width: 142px;
        }

        .auto-style6 {
            width: 142px;
            font-family: "Lucida Sans Unicode";
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <div id="page">
                <!--BEGIN HEADER -->
                <div id="header">
                    <a style="float: left" href="http://voiceshare.wavhello.com/Dash.aspx">
                        <asp:Image ID="imgLogo" runat="server" ImageUrl="images/LoginAndRegister/voiceshare_by_wavhello_logo_transparent_320x114.png" />
                    </a>
                    <div style="display: none; position: absolute" id="loginsystem">
                        <div style="position: relative">
                            <a id="openreg" href="javascript:void(0);">Login/register</a>
                        </div>
                    </div>
                    <!-- iphone icons -->
                    <div class="icons">
                        <div class="nav-icon">
                            <asp:ImageButton ID="btnFacebook" PostBackUrl="http://www.facebook.com/wavhello" ImageUrl="images/facebook.png" runat="server"></asp:ImageButton>
                        </div>
                        <div class="nav-icon">
                            <asp:ImageButton ID="btnTwitter" PostBackUrl="http://www.twitter.com/wavhello" ImageUrl="images/twitter.png" runat="server"></asp:ImageButton>
                        </div>
                    </div>
                    <!-- /end iphone icons -->
                </div>
                <!--END OF HEADER -->
                <!--BEGIN CONTENT -->
                <div id="content">
                    <div class="outside">
                        <div class="inside">
                            <div class="inside_full">
                                <div style="text-align: right; font-size: 10px">
                                    (Not "<asp:Label ID="lbl_UserName" runat="server" />
                                    "?,<a href="Login.aspx">Sign In</a>)
                                </div>
                                <ul id="profile_bar">
                                    <li><a href="Record.aspx">Record a Message</a></li>
                                    <li><a href="Invite.aspx">Send an Invitation</a></li>
                                    <li><a href="Dash.aspx">My Dashboard</a></li>
                                    <li><a href="EditForm.aspx">Edit User Profile</a></li>
                                    <li><a href="FAQ.aspx">FAQ</a></li>
                                    <li><strong>You have <span runat="server" style="background-color: red; color: white; -ms-border-radius: 50%; border-radius: 50%; padding: 2px;" id="spanCounter"></span>connections.</strong></li>
                                </ul>
                                <div class="inside_left">
                                    <h3>Invite friends or family</h3>
                                    <p>The information you fill in below will be used to create an email invitation to send to your friends and family, inviting them to join your VoiceShare network.  Once they have accepted your invite, you will be able to exchange voice messages with them.</p>
                                    <p>There is an option to add a "personalized message" as well. Feel free to use that space just to say 'hi' or perhaps to suggest ideas for what the recipient should record for you. Or you can just leave it blank.</p>
                                </div>
                                <div class="inside_right" style="padding-top: 5px;">
                                </div>
                            </div>
                            <div class="clear_all"></div>
                            <div class="inside_full">
                                <div class="fboxes2" id="vsFormWrapper">
                                    <div class="vs-row vs-row-sect fboxes">
                                        <div class="vs-row">
                                            <table class="auto-style1">
                                                <tr>
                                                    <td class="auto-style6"><strong>My Invitation Details</strong></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                            <p>
                                                Invitation Title (For your Belly Bud&#39;s archives - for example, &quot;Invitation to Aunt Sue&quot;)*
                                            </p>
                                            <table class="auto-style1">
                                                <tr>
                                                    <td class="auto-style2">
                                                        <asp:TextBox ID="txtInviteTitle" runat="server" Width="310px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <p>
                                                Your Personalized Message (You may edit to say whatever you&#39;d like!)
                                            </p>
                                            <table class="auto-style1">
                                                <tr>
                                                    <td class="auto-style3">
                                                        <asp:TextBox ID="txtPersonalMsg" runat="server" Columns="100" Rows="8" TextMode="MultiLine"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <br>
                                        <div class="vs-row">
                                            <h4>Select recipients</h4>
                                        </div>
                                        <div id="vsAddRecipWrapper">
                                            <div class="vs-row">
                                                <table class="auto-style1">
                                                    <tr>
                                                        <td class="auto-style5">
                                                            <asp:Label ID="lblRecipFirst" runat="server" Text="First Name:"></asp:Label>
                                                        </td>
                                                        <td class="auto-style5">
                                                            <asp:Label ID="lblRecipLast" runat="server" Text="Last Name:"></asp:Label>
                                                        </td>
                                                        <td class="auto-style5">
                                                            <asp:Label ID="lblEmail" runat="server" Text="Email:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblEmailConfirm" runat="server" Text="Confirm Email:"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style5">
                                                            <asp:TextBox ID="txtRecipFirst" runat="server" OnTextChanged="txtRecipFirst_TextChanged"></asp:TextBox>
                                                            <asp:RequiredFieldValidator
                                                                ID="valReqFirstName"
                                                                runat="server"
                                                                Text="*"
                                                                ErrorMessage="Enter a first name"
                                                                ControlToValidate="txtRecipFirst"
                                                                ForeColor="#009933">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="auto-style5">
                                                            <asp:TextBox ID="txtRecipLast" runat="server" OnTextChanged="txtRecipLast_TextChanged"></asp:TextBox>
                                                            <asp:RequiredFieldValidator
                                                                ID="valReqLastName"
                                                                runat="server"
                                                                Text="*"
                                                                ErrorMessage="Enter a last name"
                                                                ControlToValidate="txtRecipLast"
                                                                ForeColor="#009933">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="auto-style5">
                                                            <asp:TextBox ID="txtRecipEmail" runat="server" OnTextChanged="txtRecipEmail_TextChanged"></asp:TextBox>
                                                            <asp:RequiredFieldValidator
                                                                ID="valReqEmail"
                                                                runat="server"
                                                                Text="*"
                                                                ErrorMessage="Enter an email address"
                                                                ControlToValidate="txtRecipEmail"
                                                                ForeColor="#009933">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtEmailConfirm" runat="server" OnTextChanged="txtEmailConfirm_TextChanged"></asp:TextBox>
                                                            <asp:CompareValidator
                                                                ID="valEmailCompare"
                                                                runat="server"
                                                                ErrorMessage="The emails supplied do not match."
                                                                Text="*"
                                                                ControlToCompare="txtRecipEmail"
                                                                ControlToValidate="txtEmailConfirm"
                                                                ForeColor="#009933">
                                                            </asp:CompareValidator>
                                                            <asp:RequiredFieldValidator ID="valReqEmailCompare"
                                                                runat="server"
                                                                Text="*"
                                                                ErrorMessage="Comfirm Email is a required field."
                                                                ControlToValidate="txtEmailConfirm"
                                                                ForeColor="#009933">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="width: 100%; text-align: center">
                                                <asp:ValidationSummary ID="ValSummary"
                                                    runat="server"
                                                    HeaderText="The following error(s) occured:"
                                                    EnableClientScript="true"
                                                    DisplayMode="List"
                                                    ForeColor="#009933" />
                                            </div>
                                            <br />
                                            <div style="text-align: center" class="vs-row">
                                                <div>
                                                    <asp:Button ID="btnAddRecip"
                                                        runat="server"
                                                        Text="Add this person to recipient list"
                                                        CausesValidation="true"
                                                        OnClick="btnAddRecip_Click"></asp:Button>
                                                </div>
                                            </div>
                                            <div class="vs-row">
                                                <h4>Recipient List</h4>
                                                (Add as many people as you'd like!)
                                            </div>
                                            <br />
                                            <asp:GridView ID="Gridview1" runat="server" ShowFooter="true" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:BoundField DataField="RowNumber" HeaderStyle-Width="20px" />
                                                    <asp:TemplateField HeaderText="First Name">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TextBox1" runat="server" Enabled="false"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Last Name">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TextBox2" runat="server" Enabled="false"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Email Address">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TextBox3" runat="server" Enabled="false"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <FooterStyle HorizontalAlign="Right" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <br />
                                        </div>
                                        <div class="vs-row vs-row-sect" id="vsSubmitWrapper" style="padding-bottom: 10px">
                                            <div style="text-align: center" class="vs-row">
                                                <asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label>
                                            </div>
                                            <div style="text-align: center" class="vs-row">
                                                <asp:Button ID="btnContinue" runat="server" Text="Submit" OnClick="btnContinue_Click"></asp:Button>
                                            </div>
                                        </div>
                                        <div class="clear_all"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="clear_all"></div>
                    </div>
                    <div class="outside">
                        <div class="inside footer">
                            <!--BEGIN FOOTER -->
                            <div class="container footbox">
                                <div class="footInner" style="margin-left: 18%;">
                                    <div class="page"><strong>Pages:</strong></div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypHome" href="http://www.wavhello.com/" runat="server">Home</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypContact" href="http://wavhello.com/pages/contact-us" runat="server">Contact Us</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypTerms" NavigateUrl="http://www.wavhello.com/terms-of-use" runat="server">Terms of Service</asp:HyperLink>
                                    </div>
                                </div>
                                <div class="footInner">
                                    <div class="stay"><strong>Stay In Touch:</strong></div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypRSS" href="http://www.bellybuds.com/feed/" runat="server">Site RSS Feed</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypTwitter" href="http://www.twitter.com/wavhello" runat="server">Twitter</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypFacebook" href="http://www.facebook.com/wavhello" runat="server">Facebook</asp:HyperLink>
                                    </div>
                                </div>
                                <div style="margin-left: -30px;"><strong>&copy; 2016 BellyBuds</strong></div>
                            </div>
                            <!--END OF FOOTER -->
                            <div class="clear_all"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--END OF CONTENT -->
    </form>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-15214788-2']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</body>
</html>
