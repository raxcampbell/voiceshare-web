﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using VsClientBitBucket.Class;

namespace VsClientBitBucket
{
    public partial class Invite : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            spanCounter.InnerHtml = Session["NetworkCounter"].ToString();
            lbl_UserName.Text = (string)Session["FullUserName"];
            lblErrorMsg.Visible = false;

            if (Session["UniqueKey"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!Page.IsPostBack)
            {
                SetInitialRow();
            }
        }
        protected void lnkSignout_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("Signout.aspx");
        }
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            var otkey = (string)Session["UniqueKey"];
            var sentDate = DateTime.Now;
            const string messageType = "Pr.";
            var dtCurrentTable = (DataTable)ViewState["CurrentTable"];

            if (!string.IsNullOrEmpty(dtCurrentTable.Rows[0][1].ToString()) && !string.IsNullOrEmpty(dtCurrentTable.Rows[0][2].ToString()) && !string.IsNullOrEmpty(dtCurrentTable.Rows[0][3].ToString()))
            {
                for (var i = 0; i < (dtCurrentTable.Rows.Count - 1); i++)
                {
                    var txtFirstName = (TextBox)Gridview1.Rows[i].Cells[1].FindControl("TextBox1");
                    var txtLastName = (TextBox)Gridview1.Rows[i].Cells[2].FindControl("TextBox2");
                    var txtEmailAdd = (TextBox)Gridview1.Rows[i].Cells[3].FindControl("TextBox3");

                    dtCurrentTable.Rows[i]["FirstName"] = txtFirstName.Text;
                    dtCurrentTable.Rows[i]["LastName"] = txtLastName.Text;
                    dtCurrentTable.Rows[i]["EmailAddress"] = txtEmailAdd.Text;

                    var recipFirstName = dtCurrentTable.Rows[i]["FirstName"].ToString();
                    var recipLastName = dtCurrentTable.Rows[i]["LastName"].ToString();
                    var recipEmail = dtCurrentTable.Rows[i]["EmailAddress"].ToString();

                    var invite = new VoiceWebService.RegisterServiceClient();
                    var invitation = invite.Invitation(otkey, recipEmail, recipLastName, recipFirstName, sentDate, messageType, txtPersonalMsg.Text);
                    Session["UniqueKey"] = invitation.OtKey;
                    otkey = invitation.OtKey;

                    var firstName = (string)Session["UserFirstName"];
                    var lastName = (string)Session["UserLastName"];
                    //********************************Old email code - Rex commented 1/21/2016*********************************************************************
                    ////Email Portion
                    ////MailSubject
                    //string MailSubject = "Hello " + RecipFirstName + ", " + firstName +" " + lastName + " has invited you to record a baby message!";
                    ////MailBody
                    //string MailBody = "<p>"+"Dear "+ RecipFirstName+","+"</p><br/>"+"<p>" + "You've been invited to join the VoiceShare** network at www.bellybuds.com."+"</p><p>"+
                    //                    "Here's the message from " + firstName + ":"+"</p><p>" + txtPersonalMsg.Text +"</p><p>"+
                    //                    "Click this link to record your message. NOTE: You must record messages on a Mac or PC, the service is not currently supported by mobile/tablet devices. (registration is quick, free and easy for invited users) " + invitation.Msg + "</p><br/><p>"+
                    //                    "Thank you!"+"</p><p>"+"-Team Bellybuds*"+"</p><br/><br/><br/>"+"<p><i>"+"*Bellybuds is specialized baby-bump sound system that safely plays sound to a baby in the womb."+"</i></p><i><p>"+                               
                    //                    "**VoiceShare is an online service that allows user to record, send and download voice messages to and from others in their network."+"</i></p>";
                    //*********************************************************************************************************************************************
                    const string mailSubject = "Will you please join me on VoiceShare?";
                    var mailBody = "<p>Dear " + recipFirstName + ",</p><p>" + "You've been invited to connect with " + firstName + " " + lastName + " on VoiceShare®.</p>" +
                        "<p>Here's the message from " + firstName + ":</p>" +
                        "<p><b><i>" + txtPersonalMsg.Text + "</i></b></p>" +
                        "<p>Click this link to accept the invitation: " + invitation.Msg + "</p>" +
                        "<p>(If you haven’t yet registered, don’t worry – it’s quick, free and easy for invited users.)</p>" +
                        "<p><small>VoiceShare is a complimentary voice messaging service provided by WavHello® that allows you and anyone you love to record messages, sing songs and even create playlists to share with baby - and with each other.</small></p>" +
                        "<p><small>VoiceShare is compatible with WavHello products; BellyBuds® baby-bump sound system and SoundBub® portable Bluetooth music player and infant soother.</small></p>" +
                        "<p><small>Learn more about VoiceShare as well as our other products and services at wavhello.com</small></p>" +
                        "<p>Thank you!</p>" +
                        "<p>-Team WavHello</p>";

                    var sendEmail = invite.inviteEmail(recipEmail, mailSubject, mailBody, otkey);

                    Session["UniqueKey"] = sendEmail.OtKey;
                    otkey = sendEmail.OtKey;

                    if (sendEmail.Msg != "Success")
                    {
                        //Delete Datas
                        var delInvite = invite.deleteInviteData(otkey, recipEmail, recipLastName, recipFirstName);

                        Session["UniqueKey"] = delInvite.OtKey;
                        Response.Redirect("Result.aspx?url=InviteFail");
                    }
                }
                Response.Redirect("Result.aspx?url=Invite");
            }
            else
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text =
                    "You must first click 'Add this person to recipient list' before you can send your invitations.";
                lblErrorMsg.ForeColor = Color.Red;
            }
            //var cv = new CustomValidator
            //{
            //    IsValid = false,
            //    ErrorMessage = "Add recipients to invite friends and family."
            //};

            //Page.Validators.Add(cv);
        }
        public BabyInfo BabyInfo(int bbUSerId)
        {
            var sqlConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["bbvs_d01ConnectionString"].ConnectionString);

            sqlConn.Open();
            try
            {
                var cmd = new SqlCommand
                {
                    CommandText = "Select baby_first_name, due_date from bbuser_baby where id_user='" + bbUSerId + "'",
                    Connection = sqlConn
                };
                var reader = cmd.ExecuteReader();
                var baby = new BabyInfo();
                while (reader.Read())
                {
                    if (reader["baby_first_name"] != null)
                    {
                        baby.babyName = (string)reader["baby_first_name"];
                    }
                    baby.dueDate = (DateTime)reader["due_date"];
                }
                return baby;
            }
            catch (Exception ex)
            {
                string UPLOAD_DIRECTORY = "Exception/";
                string uploadDirectory = HttpContext.Current.Server.MapPath(UPLOAD_DIRECTORY);
                string exceptionFile = "exceptionlog.txt";
                string uploadFile = Path.Combine(uploadDirectory, exceptionFile);
                string New = "New";
                DateTime DateTime = System.DateTime.Now;
                string MethodName = "WebService's database connect exception occurs over BabyInfo babyInfo(int bbUSerId)";
                string exception = ex.Message.ToString();
                string[] ExceptionOccur = { " ", New, "Date:", DateTime.ToString(), "MethodName:", MethodName, "Exception:", exception };
                File.AppendAllLines(uploadFile, ExceptionOccur);
                return null;
            }
            finally
            {
                sqlConn.Close();
                sqlConn.Dispose();
            }
        }
        private void SetInitialRow()
        {
            var dt = new DataTable();
            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("FirstName", typeof(string)));
            dt.Columns.Add(new DataColumn("LastName", typeof(string)));
            dt.Columns.Add(new DataColumn("EmailAddress", typeof(string)));
            var dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["FirstName"] = string.Empty;
            dr["LastName"] = string.Empty;
            dr["EmailAddress"] = string.Empty;
            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            Gridview1.DataSource = dt;
            Gridview1.DataBind();
        }
        private void AddNewRowToGrid()
        {
            var rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                var dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if ((dtCurrentTable.Rows.Count > 0) && (txtRecipFirst.Text != "") && (txtRecipLast.Text != "") && (txtRecipEmail.Text != "") && (txtEmailConfirm.Text != ""))
                {
                    var i = dtCurrentTable.Rows.Count;
                    //extract the TextBox values
                    var box1 = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("TextBox1");
                    var box2 = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("TextBox2");
                    var box3 = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("TextBox3");

                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["RowNumber"] = i + 1;

                    dtCurrentTable.Rows[i - 1]["FirstName"] = txtRecipFirst.Text;
                    dtCurrentTable.Rows[i - 1]["LastName"] = txtRecipLast.Text;
                    dtCurrentTable.Rows[i - 1]["EmailAddress"] = txtRecipEmail.Text;

                    rowIndex++;

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;

                    Gridview1.DataSource = dtCurrentTable;
                    Gridview1.DataBind();

                    txtRecipFirst.Text = "";
                    txtRecipLast.Text = "";
                    txtRecipEmail.Text = "";
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData();
        }
        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox box1 = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("TextBox1");
                        TextBox box2 = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("TextBox2");
                        TextBox box3 = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("TextBox3");

                        box1.Text = dt.Rows[i]["FirstName"].ToString();
                        box2.Text = dt.Rows[i]["LastName"].ToString();
                        box3.Text = dt.Rows[i]["EmailAddress"].ToString();

                        rowIndex++;
                    }
                }
            }
        }
        protected void btnAddRecip_Click(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
            if (txtRecipEmail.Text == txtEmailConfirm.Text)
            {
                AddNewRowToGrid();

                txtEmailConfirm.Text = "";
            }
        }
        protected void txtRecipFirst_TextChanged(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
        }
        protected void txtRecipLast_TextChanged(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
        }
        protected void txtRecipEmail_TextChanged(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
        }
        protected void txtEmailConfirm_TextChanged(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
        }
    }
}
