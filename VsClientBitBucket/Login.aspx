﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VSLogin.Master" Async="true" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="VsClientBitBucket.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" style="margin-top: 35%;">
                <div class="redirect_link">
                    <a id="A1" style="text-decoration: none; color: #ffffff;" type="text/html" href="http://wavhello.com/pages/about-us/">WHAT ARE BELLYBUDS?</a>
                </div>
                <div class="redirect_link">
                    <a id="A2" style="text-decoration: none; color: #ffffff;" type="text/html" href="http://wavhello.com/products/bellybuds">PURCHASE BELLYBUDS</a>
                </div>
            </div>
            <div class="col-lg-5 col-md-5" style="margin-top: 20%;">
                <div class="custom-well">
                    <img src='images/LoginAndRegister/voiceshare_by_wavhello_logo_transparent_320x114.png' class="img-responsive center-block" alt="VoiceShare logo" />
                    <p class="text-center">
                        VoiceShare by WavHello is a voice recording network that allows you to share your voice from wherever you are. Record a story, a song, a moment - right to the site. Or else invite others to join your network so they can share recordings with you. With a few simple clicks, messages can be downloaded to any MP3 player and shared with the baby-to-be.
                    </p>
                    <div class="col-lg-12 col-xs-12">
                        <div class="form-group">
                            <asp:Label ID="lblTitle" CssClass="text-center" runat="server" Text="Register or Sign-In:" ForeColor="#54646e" Font-Size="Small" Font-Bold="True"></asp:Label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtUserId" CssClass="form-control" runat="server" Width="100%" TabIndex="1" PlaceHolder="Email"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="valUserId"
                                runat="server"
                                ControlToValidate="txtUserId"
                                Text="*"
                                ErrorMessage="Enter an email"
                                ForeColor="#009933"
                                Display="Static">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtPassword" CssClass="form-control" runat="server" Width="100%" TabIndex="2" PlaceHolder="Password" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="valPassword"
                                runat="server"
                                Text="*"
                                ControlToValidate="txtPassword"
                                ErrorMessage="Enter a password"
                                ForeColor="#009933"
                                Display="Static">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <a runat="server" onserverclick="btnLogin_Click" class="ghost-button">Log-In</a>
                            <%--<asp:Button ID="btnLogin" runat="server" TabIndex="3" Text="Log-In" CssClass="ghost-button" OnClick="btnLogin_Click" />--%>
                        </div>
                        <p>
                            <asp:HyperLink ID="hypRegister" runat="server" TabIndex="4" NavigateUrl="~/Register.aspx" ForeColor="#54646e" Style="text-decoration: none;" Font-Bold="True">Register</asp:HyperLink>
                        </p>
                        <p>
                            <asp:HyperLink ID="hypForgot" runat="server" TabIndex="5" NavigateUrl="~/UserManagement/Account/ForgotPassword" ForeColor="#54646e" Style="text-decoration: none" Font-Bold="True">Forgot Password?</asp:HyperLink>
                        </p>
                        <div class="summary">
                            <asp:ValidationSummary ID="valLoginSummary"
                                runat="server"
                                HeaderText="The following errors have occured:"
                                ForeColor="#009933"
                                DisplayMode="List"
                                ShowMessageBox="True"
                                ShowSummary="False" />
                            <asp:Label ID="lblUserError" runat="server" ForeColor="#009933"></asp:Label>
                        </div>
                    </div>
                    <%--<div class="login">--%>
                    <div class="title">
                        <%--<asp:Label ID="lblTitle" runat="server" Text="Register or Sign-In:" ForeColor="#FF9999" Font-Size="Small"></asp:Label>--%>
                    </div>
                    <div class="textboxes">
                        <%--<asp:TextBox ID="txtUserId" runat="server" TabIndex="1" PlaceHolder="User Id"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="valUserId"
                                runat="server"
                                ControlToValidate="txtUserId"
                                Text="*"
                                ErrorMessage="Enter a user id"
                                ForeColor="#009933"
                                Display="Static">
                            </asp:RequiredFieldValidator>--%>
                    </div>
                    <%--<br />--%>
                    <div class="textboxes">
                        <%--<asp:TextBox ID="txtPassword" runat="server" TabIndex="2" PlaceHolder="Password" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="valPassword"
                                runat="server"
                                Text="*"
                                ControlToValidate="txtPassword"
                                ErrorMessage="Enter a password"
                                ForeColor="#009933"
                                Display="Static">
                            </asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="clear_all">
                        <%--<br />--%>
                        <%--<div class="summary">
                            <asp:ValidationSummary ID="valLoginSummary"
                                runat="server"
                                HeaderText="The following errors have occured:"
                                ForeColor="#009933"
                                DisplayMode="List"
                                ShowMessageBox="True"
                                ShowSummary="False" />
                            <asp:Label ID="lblUserError" runat="server" ForeColor="#009933"></asp:Label>
                        </div>--%>
                        <%--<div class="button_collection_container">
                        <div class="login_btn_container">
                            <asp:Button ID="btnLogin" runat="server" TabIndex="3" Text="Log-In" CssClass="login_button" OnClick="btnLogin_Click" />
                            <asp:LinkButton ID="lnkLogin" runat="server" ForeColor="#75CE7E" Style="text-decoration: none" OnClick="lnkLogin_Click" Font-Size="Small">Log-In</asp:LinkButton>
                        </div>
                        <div class="clear_all"></div>
                        <div class="register_btn_container">
                            <asp:Button ID="btnRegisterLoginPg" runat="server" Text="Register" CausesValidation="false" CssClass="register_button_login_pg" OnClick="btnRegisterLoginPg_Click" />
                            <asp:HyperLink ID="hypRegister" runat="server" TabIndex="4" NavigateUrl="~/Register.aspx" ForeColor="#75CE7E" Style="text-decoration: none;">Register</asp:HyperLink>
                        </div>
                        <div class="clear_all"></div>
                        <div class="forgot_btn_container">
                            <asp:Button ID="btnForgot" runat="server" Text="Forgot Password?" CssClass="forgot_button" NavigateUrl="~/Forgot.aspx" />
                            <asp:HyperLink ID="hypForgot" runat="server" TabIndex="5" NavigateUrl="~/Forgot.aspx" ForeColor="#75CE7E" Style="text-decoration: none">Forgot Password?</asp:HyperLink>
                        </div>
                    </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-15214788-2']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</asp:Content>
