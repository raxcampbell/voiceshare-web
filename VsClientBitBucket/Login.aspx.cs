﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Data.SqlClient;
using System.IO;
using VsClientBitBucket.Class;

namespace VsClientBitBucket
{
    public partial class Login : System.Web.UI.Page
    {
        //Service addresses//
        private readonly string _serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"];

        protected void Page_Load(object sender, EventArgs e)
        {
            //string url = Request.Url.AbsoluteUri;

            //if (url == "http://bellybuds.voiceshare.net")
            //{
            //    Response.Redirect("https://bellybuds.voiceshare.net/");
            //}
            //LoginVoiceShare("waltersilin@bauengroup.us", "Test1234");
        }

        protected async void btnLogin_Click(object sender, EventArgs e)
        {
            if ((txtUserId.Text != "") && (txtPassword.Text != ""))
            {
                //await LoginVoiceShare("waltersilin@bauengroup.us", "Test1234");
                await LoginVoiceShare(txtUserId.Text, txtPassword.Text);
            }

            if (Session["AuthToken"] != null)
            {
                Response.Redirect("~/Dash.aspx", false);
            }
            else
            {
                txtUserId.Text = "";
                txtPassword.Text = "";
                lblUserError.Text = "Username and/or password is invalid";
            }

            //    // Task 194 - wsilin - 7/25/13 - logging of activity
                //    string UPLOAD_DIRECTORY2 = "Exception/";
                //    string uploadDirectory2 = HttpContext.Current.Server.MapPath(UPLOAD_DIRECTORY2);
                //    string exceptionFile2 = "exceptionlog.txt";
                //    string uploadFile2 = Path.Combine(uploadDirectory2, exceptionFile2);
                //    string New2 = "New";
                //    DateTime DateTime2 = System.DateTime.Now;
                //    string MethodName2 = "Login-btnSubmit_Click";
                //    string exception2 = "Login Attempted from " + HttpContext.Current.Request.Url.Host.ToString() + " by user " + txtUserId.Text;
                //    string[] ExceptionOccur2 = { " ", New2, "Date:", DateTime2.ToString(), "MethodName:", MethodName2, "Exception:", exception2 };

                //    File.AppendAllLines(uploadFile2, ExceptionOccur2);
                //    # region getbellybudsID
                //    // Get Belly Buds ID
                //    SqlConnection sqlConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["bbvs_d01ConnectionString"].ConnectionString.ToString());
                //    sqlConn.Open();
                //    try
                //    {
                //        SqlCommand cmd = new SqlCommand();
                //        cmd.CommandText = "SELECT id_user FROM bbuser WHERE username='" + txtUserId.Text + "'";
                //        cmd.Connection = sqlConn;
                //        SqlDataReader readerBBID;
                //        readerBBID = cmd.ExecuteReader();
                //        while (readerBBID.Read())
                //        {
                //            Session.Add("bellybuds_id", (string)readerBBID["id_user"].ToString());
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        string UPLOAD_DIRECTORY = "Exception/";
                //        string uploadDirectory = HttpContext.Current.Server.MapPath(UPLOAD_DIRECTORY);                 
                //        string exceptionFile = "exceptionlog.txt";
                //        string uploadFile = Path.Combine(uploadDirectory, exceptionFile);
                //        string New = "New";
                //        DateTime DateTime = System.DateTime.Now;
                //        string MethodName = "Login-btnSubmit_Click";
                //        string exception = ex.Message.ToString();
                //        string[] ExceptionOccur = { " ", New, "Date:", DateTime.ToString(), "MethodName:", MethodName, "Exception:", exception };

                //        File.AppendAllLines(uploadFile, ExceptionOccur);

                //    }
                //    finally
                //    {
                //        sqlConn.Close();
                //        sqlConn.Dispose();
                //    }
                //    #endregion

                //    // Call web service
                //    string Tenant = HttpContext.Current.Request.Url.Host.ToString();
                //    //string Tenant = "localhost";
                //    SecureVoiceWebService.RegisterServiceClient login = new SecureVoiceWebService.RegisterServiceClient();
                //    SecureVoiceWebService.AuthenticationResponse authenticate = null;
                //    try
                //    {
                //        authenticate = login.Authentication(txtUserId.Text, txtPassword.Text, Tenant);
                //    }
                //    catch (Exception webException)
                //    {
                //        if (webException.InnerException.HResult == -2146232000)
                //        {
                //            authenticate = login.Authentication(txtUserId.Text, txtPassword.Text, Tenant);
                //        }
                //    }

                //    if (authenticate.Boolean == "yes")
                //    {
                //        Session["UniqueKey"] = authenticate.UniqId;

                //        Response.Redirect("Dash.aspx");
                //    }
                //    else
                //    {
                //        txtUserId.Text = "";
                //        txtPassword.Text = "";
                //        lblUserError.Text = "Username and/or password is invalid";
                //    }
        }

        protected void btnRegisterLoginPg_Click(object sender, EventArgs e)
        {
            Response.Redirect("Register.aspx");
        }

        public async Task LoginVoiceShare(string username, string password)
        {
            try
            {
                var email = "";
                string token;
                string oneTimeKey;
                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(_serviceUrl);
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                    var responseMessage = await httpClient.PostAsync("Token", new FormUrlEncodedContent(
                        new[] {
                        new KeyValuePair < string, string > ("grant_type", "password"),
                        new KeyValuePair < string, string > ("username", username),
                        new KeyValuePair < string, string > ("password", password)
                    }

                        ));
                    var tokenModel = await responseMessage.Content.ReadAsAsync<TokenModel>();
                    token = tokenModel.AccessToken;
                    oneTimeKey = await GetOneTimeKey(tokenModel.Username);
                }

                Session["AuthToken"] = token;
                Session["UniqueKey"] = oneTimeKey;
            }
            catch (Exception)
            {
                return;
                //throw;
            }
        }

        //private async Task<bool> IsValidEmail(string username)
        //{
        //    bool isValidEmail;
        //    using (var httpClient = new HttpClient())
        //    {
        //        httpClient.BaseAddress = new Uri(TestServiceUrl);
        //        httpClient.DefaultRequestHeaders.Accept.Clear();
        //        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

        //        var response = await httpClient.GetAsync("api/Person/IsValidEmail?email=" + username);
        //        isValidEmail = await response.Content.ReadAsAsync<Boolean>();
        //    }
        //    return isValidEmail;
        //}

        public async Task<string> GetOneTimeKey(string email)
        {
            string oneTimeKey;
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(_serviceUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                var response = await httpClient.GetAsync("api/Auth/GetOneTimeKey?email=" + email);
                oneTimeKey = await response.Content.ReadAsAsync<String>();
            }
            return oneTimeKey;
        }
    }
}