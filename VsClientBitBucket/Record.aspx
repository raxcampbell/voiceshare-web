﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Record.aspx.cs" Inherits="VsClientBitBucket.RecordDemo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Belly Buds: Voiceshare Application</title>
    <link type="text/css" rel="stylesheet" href="Styles/Custom2.css" />
    <link href="Styles/Container.css" rel="stylesheet" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/modernizr-2.6.2.js"></script>
    <script type="text/javascript">
        var wordLen = 3; // Maximum word length
        function checkMaxLen(txt, maxLen) {
            try {

                if (txt.value.length > (maxLen - 1)) {
                    var cont = txt.value;
                    txt.value = cont.substring(0, (maxLen - 1));
                    return false;
                };
            } catch (e) {
            }
        }
    </script>
    <script type="text/javascript" src="swfobject.js"></script>
    <!-- Include CSS to eliminate any default margins/padding and set the height of the html element and 
             the body element to 100%, because Firefox, or any Gecko based browser, interprets percentage as 
             the percentage of the height of its parent container, which has to be set explicitly.  Fix for
             Firefox 3.6 focus border issues.  Initially, don't display flashContent div so it won't show 
             if JavaScript disabled.
        -->
    <style type="text/css" media="screen">
        object:focus {
            outline: none;
        }

        #flashContent {
            display: none;
        }
    </style>

    <script type="text/javascript" src="swfobject.js"></script>
    <script type="text/javascript">
        var swfVersionStr = "10.2.0";
        var xiSwfUrlStr = "";

        var flashvars = {};

        flashvars.lstext = "Loading..."; //you can provide a translation here for the "Laoding..." text taht shows up while this file and the external language file is loaded
        flashvars.recorderId = "REC1"; //set this var if you have multiple instances of the mp3recorder on a page and you want to identify them
        flashvars.userId = "walter123"; //this variable is sent back to upload.php when the user presses the [SAVE] button
        flashvars.licenseKey = "77617668656c6c6f2e636faurc8be6d3f6c6f63616c686f7374aurc0c2c4bacebac0"; //licensekey variable, you get it when you purchase the software
        //Old code - 766f69636573686172652e6eaurc8be65743f6c6f63616c686f7374aurc0c2c4bacebae3
        
        var params = {};
        params.quality = "high";
        params.bgcolor = "#ffffff";
        params.allowscriptaccess = "sameDomain";
        params.allowfullscreen = "true";

        var attributes = {};
        attributes.id = "Audior";
        attributes.name = "Audior";
        attributes.align = "middle";

        swfobject.embedSWF(
            "Audior.swf", "flashContent",
            "500", "240",
            swfVersionStr, xiSwfUrlStr,
            flashvars, params, attributes);
        swfobject.createCSS("#flashContent", "display:block;text-align:left;");
    </script>

    <!-- Below is the MP3Recorder JS API (Callbacks) -->

</head>
<body>
    <form id="Form1" runat="server">
        <div id="wrapper">
            <div id="page">
                <asp:HiddenField ID="HiddenField1" runat="server" Value="sonik" />
                <!--BEGIN HEADER -->
                <div id="header">
                    <a style="float: left" href="http://voiceshare.wavhello.com/Dash.aspx">
                        <asp:Image ID="imgLogo" runat="server" ImageUrl="images/LoginAndRegister/voiceshare_by_wavhello_logo_transparent_320x114.png" />
                    </a>
                    <div style="display: none; position: absolute" id="loginsystem">
                        <div style="position: relative">
                            <a id="openreg" href="javascript:void(0);">Login/register</a>
                        </div>
                    </div>
                    <!-- iphone icons -->
                    <div class="icons">
                        <div class="nav-icon">
                            <asp:ImageButton ID="btnFacebook" PostBackUrl="http://www.facebook.com/wavhello" ImageUrl="images/facebook.png" runat="server"></asp:ImageButton>
                        </div>
                        <div class="nav-icon">
                            <asp:ImageButton ID="btnTwitter" PostBackUrl="http://www.twitter.com/wavhello" ImageUrl="images/twitter.png" runat="server"></asp:ImageButton>
                        </div>
                    </div>
                    <!-- /end iphone icons -->
                </div>
                <!--END OF HEADER -->
                <!--BEGIN CONTENT -->
                <div id="content">
                    <div class="outside">
                        <div class="inside">
                            <div class="inside_full">
                                <div style="text-align: right; font-size: 10px">
                                    (Not "<asp:Label ID="lbl_UserName" runat="server" />
                                    "?,<a href="Login.aspx">Sign In</a>)
                                </div>
                                <ul id="profile_bar">
                                    <li><a href="Record.aspx">Record a Message</a></li>
                                    <li><a href="Invite.aspx">Send an Invitation</a></li>
                                    <li><a href="Dash.aspx">My Dashboard</a></li>
                                    <li><a href="EditForm.aspx">Edit User Profile</a></li>
                                    <li><a href="FAQ.aspx">FAQ</a></li>
                                    <li><strong>You have <span runat="server" style="background-color: red; color: white; -ms-border-radius: 50%; border-radius: 50%; padding: 2px;" id="spanCounter"></span> connections.</strong></li>
                                </ul>
                                <div class="inside_left" style="width: 66%">
                                    <h3>Record a message</h3>
                                    <p>Here's how to record a message (we recommend you use a headset or external microphone for best clarity):</p>
                                    <ul>
                                        <li class="directions">Add a Voice Message Title and Voice Message Description</li>
                                        <li class="directions">Press the Record button to begin</li>
                                        <li class="directions">In the pop-up, click “Allow” and “Close”</li>
                                        <li class="directions">Record your message</li>
                                        <li class="directions">Press the Play button to listen or the “Record Again” button (located beneath the green “Play” button) to erase and re-record</li>
                                        <li class="directions">Click “Save” to save your message</li>
                                        <li class="directions">Navigate to the Dash page, under "Voice Messages I've Created", click the pink, right directional arrow to select recipients from your list of connections</li>
                                        <li class="directions">Once you have checked as many recipients as you want, click o.k. and your message will be sent out</li>
                                        <li class="directions">You can also give permission through Facebook or Twitter to post a message to your Facebook or Twitter feeds using the appropriate icon below.<br/>Example: "Mary recorded a message titled, 'Happy Birthday Baby' at 10:06 AM"</li>
                                    </ul>
                                    <div>
                                        <div style="display: inline; margin: 5px;">
                                            <asp:ImageButton ID="imgBtnFbLogin" runat="server" ImageUrl="images/FB-f-Logo__blue_29.png" OnClick="imgBtnFbLogin_Click"/>
                                        </div>
                                        <asp:Label ID="lblFbLogin" runat="server" Text="Connect with Facebook"></asp:Label>
                                        <div style="display: inline; margin: 5px;">
                                            <asp:ImageButton ID="imgBtnTwLogin" runat="server" ImageUrl="images/rsz_twitter-icon-png-29x29.png" OnClick="imgBtnTwLogin_Click"/>
                                        </div>
                                        <asp:Label ID="lblTwLogin" runat="server" Text="Connect with Twitter"></asp:Label>
                                    </div>
                                    <p>
                                        You can talk up to 10 minutes.
                                        <asp:HyperLink ID="hypWhat" runat="server" NavigateUrl="~/WhatToSay.aspx">Don't know what to say?</asp:HyperLink>
                                    </p>
                                </div>
                                <div class="inside_right">
                                    <img style="float: right; padding: 5px" src="images/record.jpg">
                                </div>
                            </div>
                            <div class="clear_all"></div>
                            <div class="inside_full">
                                <div class="fboxes2" id="vsFormWrapper">
                                    <input type="hidden" name="strmname" id="strmname">
                                    <input type="hidden" name="message_recorded">
                                    <input type="hidden" name="message_file">
                                    <input type="hidden" name="message_dur">
                                    <input type="hidden" name="message_user">
                                    <input type="hidden" value="0" name="invite_recip_num">
                                    <div class="vs-row vs-row-sect fboxes" style="padding-bottom: 10px;">
                                        <div class="vs-row">
                                            <div class="vs-col">
                                                <div id="voiceshare_box">
                                                    <script type="text/javascript">
                                                        function btSaveClick(recordName, duration, recorderId) {
                                                            //this function is called when the SAVE button is released and it is called with 3 parameters: 
                                                            //recordName: the file name of the new audio recording  including the .mp3 extension
                                                            //recordDuration: duration of the recorded audio file in seconds but accurate to the millisecond (like this: 4.322)
                                                            //recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page

                                                            //alert("btSaveClick("+recordName+","+recordDuration+","+recorderId+")");
                                                            document.getElementById("firstDisplay").innerHTML = "Last JS callback: btSaveClick()";
                                                        }

                                                        function btDownloadClick(recorderId) {
                                                            //this function is called when the DOWNLOAD button is released 
                                                            //alert("btDownloadClick("+recorderId+")");
                                                            document.getElementById("firstDisplay").innerHTML = "Last JS callback: btDownloadClick()";
                                                        }
                                                        function btPlayClick(recorderId) {
                                                            //this function is called when the PLAY button is released
                                                            //alert("btPlayClick("+recorderId+")");
                                                            document.getElementById("firstDisplay").innerHTML = "Last JS callback: btPlayClick()";
                                                        }

                                                        function btPauseClick(recorderId) {
                                                            //this function is called when the PAUSE button is released
                                                            //alert("btPauseClick("+recorderId+")");
                                                            document.getElementById("firstDisplay").innerHTML = "Last JS callback: btPauseClick()";
                                                        }

                                                        function btStopClick(recorderId) {
                                                            //this function is called when the STOP button is released
                                                            //alert("btStopClick("+recorderId+")");
                                                            document.getElementById("firstDisplay").innerHTML = "Last JS callback: btStopClick()";
                                                        }

                                                        function btRecordClick(recorderId) {
                                                            //this function is called when the RECORD button is released
                                                            //alert("btRecordClick("+recorderId+")");
                                                            document.getElementById("sav").disabled = true
                                                            document.getElementById("dwl").disabled = true
                                                            document.getElementById("firstDisplay").innerHTML = "Last JS callback: btRecordClick()";
                                                        }

                                                        function onRecordingPlaybackStopped(recorderId) {
                                                            //the recording has stopped playing
                                                            //alert('onRecordingPlaybackStopped('+recorderId+')')
                                                            document.getElementById("firstDisplay").innerHTML = "Last JS callback: onRecordingPlaybackStopped()";
                                                        }

                                                        function onEncodingDone(duration, recorderId) {
                                                            //the MP3Recorder has finished the encoding of your audio data to mp3
                                                            //alert('onEncodingDone('+duration+', '+recorderId+')')
                                                            document.getElementById("sav").disabled = false
                                                            document.getElementById("firstDisplay").innerHTML = "Last JS callback: onEncodingDone()";
                                                        }

                                                        function onUploadDone(success, recordName, duration, recorderId) {
                                                            //the MP3Recorder has finished uploading the files to your server AND it has received a save=ok OR save=failed response from the upload script on the web server
                                                            //success  will be true if the upload succeeded and false otherwise
                                                            //alert('onUploadDone('+success+', '+recordName+', '+duration+', '+recorderId+')')
                                                            document.getElementById("fbPostMessage").click();
                                                            //alert("FBclicked");
                                                            //document.getElementById("btnUpdateTwitter").click();
                                                            //document.getElementById("dwl").disabled = false
                                                            //document.getElementById("firstDisplay").innerHTML = "Last JS callback: onUploadDone()";
                                                        }

                                                        function onMicAccess(allowed, recorderId) {
                                                            //alert("onMicAccess("+allowed+")");
                                                            //the user clicked Allow or Deny in the Privacy panel dialog box in Flash Player
                                                            //when the user clicks Deny this function is called with allowed=false
                                                            //when the user clicks Allow this function is called with allowed=true
                                                            //this function can be called anytime during the life of the Audior instance as the user has permanent access to the Privacy panel dialog box in Flash Player
                                                            document.getElementById("secondDisplay").innerHTML = "Access to mic allowed: " + allowed;
                                                            document.getElementById("firstDisplay").innerHTML = "Last JS callback: onMicAccess()";
                                                        }

                                                        function onFlashReady(recorderId) {
                                                            //alert("onFlashReady()");
                                                            //as soon as this function is called you can communicate with Audior using the JS Control API
                                                            //Example : document.Audior.record(); will make a call to flash in order to start recording
                                                            //recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
                                                            document.getElementById("firstDisplay").innerHTML = "Last JS callback: onFlashReady()";
                                                        }
                                                    </script>

                                                    <div>
                                                        <div>
                                                            <div runat="server" id="recordMsg" style="display: table; height: auto; margin: 0 auto;">
                                                                <table>
                                                                    <tr>
                                                                        <td colspan="2">Voice Message Title:</td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="txt_msg_Title" runat="server" MaxLength="50" Width="336px" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="vertical-align: top">Voice Message Description:</td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="txt_msg_description" runat="server" Style="-moz-resize: none; -ms-resize: none; -o-resize: none; resize: none; font-size: 12px; font-family: Lucida Grande,Lucida Sans Unicode,Arial,Verdana,sans-serif;" Width="336px" Rows="3" TextMode="MultiLine" onkeyup="return checkMaxLen(this,201)" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"></td>
                                                                        <td colspan="2">
                                                                            <asp:Button ID="btnRecord" runat="server" Style="padding: 10px; font-size: 14px; font-weight: normal; -ms-border-radius: 5px; border-radius: 5px;" BackColor="#75CE7E" ForeColor="White" Text="Ready to Record" OnClick="btnRecord_Click" /></td>
                                                                    </tr>
                                                                </table>                                           
                                                                <div class="error_msg" style="margin: 10px auto;">
                                                                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <!-- SWFObject's dynamic embed method replaces this alternative HTML content with Flash content when enough 
             JavaScript and Flash plug-in support is available. The div is initially hidden so that it doesn't show
             when JavaScript is disabled.-->
                                                            <div id="a" style="display: table; margin: 10px auto;">
                                                                <div id="flashContent" runat="server">
                                                                    <p>
                                                                        To view this page ensure that Adobe Flash Player version 10.2.0 or greater is installed. 
                                                                    </p>
                                                                    <script type="text/javascript">
                                                                        var pageHost = ((document.location.protocol == "https:") ? "https://" : "http://");
                                                                        document.write("<a href='http://www.adobe.com/go/getflashplayer'>Get Adobe Flash Player</a>");
                                                                    </script>
                                                                </div>
                                                                <noscript>
                                                                    You need to have JS enabled for Audior to show.
                                                                </noscript>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END OF CONTENT -->
                            <div class="clear_all">
                                <asp:Button ID="fbPostMessage" runat="server" CausesValidation="False" Style="display: none;" OnClick="fbPostMessage_Click" Text="fbPostMessage" />
                            </div>
                        </div>
                    </div>
                    <div class="outside">
                        <div class="inside footer">
                            <!--BEGIN FOOTER -->
                            <div class="container footbox">
                                <div class="footInner" style="margin-left: 18%;">
                                    <div class="page"><strong>Pages:</strong></div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypHome" href="http://www.wavhello.com/" runat="server">Home</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypContact" href="http://wavhello.com/pages/contact-us" runat="server">Contact Us</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypTerms" NavigateUrl="http://www.wavhello.com/terms-of-use" runat="server">Terms of Service</asp:HyperLink>
                                    </div>
                                </div>
                                <div class="footInner">
                                    <div class="stay"><strong>Stay In Touch:</strong></div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypRSS" href="http://www.bellybuds.com/feed/" runat="server">Site RSS Feed</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypTwitter" href="http://www.twitter.com/wavhello" runat="server">Twitter</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypFacebook" href="http://www.facebook.com/wavhello" runat="server">Facebook</asp:HyperLink>
                                    </div>
                                </div>
                                <div style="margin-left: -30px;"><strong>&copy; 2016 BellyBuds</strong></div>
                            </div>
                            <!--END OF FOOTER -->
                            <div class="clear_all"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script>
            /*$("#invite_recip_first").keyup(function () {
            var value = $(this).val();
            $("#effectsthis").text(value);
            }).keyup(); */




            //----------Recipient Function Hide/Show Based on Checkbox

            if ($("#chkJustSave").is(":checked")) {
                $("#vsAddRecipWrapper").hide()
            }
            $('#chkJustSave').click(function () {
                $('#vsAddRecipWrapper').toggle("fast");
            });

            //----------Recipient Append
            $('#btnAddRecip').click(function () {
                var itemlist;

                /*Form Vars*/

                var firstinput = $('#invite_recip_first').val();
                var lastinput = $('#invite_recip_last').val();
                var emailinput = $('#invite_recip_email1').val();
                var emailinputmatch = $('#invite_recip_email2').val();
                var nickinput = $('#invite_recip_nick').val();

                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                //Validation

                var errormssg = '';

                if (firstinput == '') {
                    errormssg = '- First Name Incorrect\n';
                }
                if (lastinput == '') {
                    errormssg = errormssg + '- Last Name Incorrect\n';
                }
                if (emailinput == '' || !filter.test(emailinput)) {
                    errormssg = errormssg + '- Email Incorrect\n';
                }
                if (emailinput != emailinputmatch) {
                    errormssg = errormssg + '- Emails Fields Do Not Match';
                }

                if (errormssg != '') {
                    alert(errormssg);
                } else {
                    //How many already in list
                    itemlist = $("#vsRecipTable tbody tr").size();
                    alert(itemlist);
                    itemlist = itemlist + 1;

                    //add to list
                    $('#vsRecipTable tbody').append('<tr id="entry[' + itemlist + ']"><th><input type="text" value="' + firstinput + '" name="invite_recip_first[' + itemlist + ']" id="invite_recip_first[' + itemlist + ']"></th><th><input type="text" value="' + lastinput + '" name="invite_recip_last[' + itemlist + ']" id="invite_recip_last[' + itemlist + ']"></th><th><input type="text" value="' + emailinput + '" name="invite_recip_email[' + itemlist + ']" id="invite_recip_email[' + itemlist + ']"></th><th><input type="text" value="' + nickinput + '" name="invite_recip_nick[' + itemlist + ']" id="invite_recip_nick[' + itemlist + ']"></th><th style="cursor:pointer" onclick = $(this).parent().remove();>remove</th></tr>');

                    $('#invite_recip_first').val('');
                    $('#invite_recip_last').val('');
                    $('#invite_recip_email1').val('');
                    $('#invite_recip_email2').val('');
                    $('#invite_recip_nick').val('');
                };

                //Clear values 

                var firstinput = '';
                var lastinput = '';
                var emailinput = '';
                var emailinputmatch = '';
                var nickinput = '';
            });
        </script>
    </form>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-15214788-2']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

</script>
</body>
</html>
