﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using Facebook;
using LinqToTwitter;
using CheckBox = System.Web.UI.WebControls.CheckBox;

namespace VsClientBitBucket
{
    public partial class RecordDemo : System.Web.UI.Page
    {
        private const string OAuthCredentialsKey = "OAuthCredentialsKey";
        private AspNetAuthorizer auth;
        private TwitterContext twitterCtx;
        long fbAppId = 453176514805087;
        string fbAppSecret = "80d4fcbc476545f1dada235f2b253730";

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            //var pathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
            //var fbCallbackUri = new Uri(Request.Url.AbsoluteUri.Replace(pathAndQuery, "Record.aspx"));

            if (Session["UniqueKey"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                string OtKey = "";
                OtKey = (string)Session["UniqueKey"];
                spanCounter.InnerHtml = Session["NetworkCounter"].ToString();

                VoiceWebService.RegisterServiceClient ListSenders = new VoiceWebService.RegisterServiceClient();
                string Tenant = HttpContext.Current.Request.Url.Host.ToString();

                if (!IsPostBack)
                {
                    //btnContinue.Visible = false;
                    flashContent.Visible = false;
                    lbl_UserName.Text = (String)Session["FullUserName"];
                }
            }

            //#region Facebook
            //if (Session["FbAccessToken"] != null)
            //{
            //    var fb = new FacebookClient(Session["FbAccessToken"].ToString());
            //    dynamic me = fb.Get("me?fields=email");
            //    string email = me.email;

            //    lblFbLogin.Text = "Logout of Facebook";
            //}

            //else if (Request.QueryString["code"] != null)
            //{
            //    string accessCode = Request.QueryString["code"].ToString();

            //    var fb = new FacebookClient();
            //    // throws OAuthException
            //    dynamic result = fb.Post("oauth/access_token", new
            //    {
            //        client_id = fbAppId,
            //        client_secret = fbAppSecret,
            //        redirect_uri = "https://bellybuds.voiceshare.net/record.aspx",
            //        code = accessCode
            //    });

            //    var accessToken = result.access_token;
            //    var expires = result.expires;

            //    // Store the access token in the session
            //    Session["FbAccessToken"] = accessToken;

            //    // update the facebook client with the access token
            //    fb.AccessToken = accessToken;

            //    // Calling Graph API for user info
            //    dynamic me = fb.Get("me?fields=email");

            //    string email = me.email;

            //    lblFbLogin.Text = "Logout of Facebook";

            //    FormsAuthentication.SetAuthCookie(email, false);
            //}
            //else if (Request.QueryString["error"] != null)
            //{
            //    string error = Request.QueryString["error"];
            //    string errorReason = Request.QueryString["error_reason"];
            //    string errorDescription = Request.QueryString["error_description"];
            //}
            //else
            //{

            //}
            //#endregion

            //#region Twitter
            //IOAuthCredentials credentials = new InMemoryCredentials();
            //var authString = Session[OAuthCredentialsKey] as string;

            //if (authString == null)
            //{
            //    credentials.ConsumerKey = ConfigurationManager.AppSettings["twitterConsumerKey"];
            //    credentials.ConsumerSecret = ConfigurationManager.AppSettings["twitterConsumerSecret"];

            //    Session[OAuthCredentialsKey] = credentials.ToString();
            //}
            //else
            //{
            //    credentials.Load(authString);
            //}

            //auth = new WebAuthorizer
            //{
            //    Credentials = new InMemoryCredentials
            //    {
            //        ConsumerKey = ConfigurationManager.AppSettings["twitterConsumerKey"],
            //        ConsumerSecret = ConfigurationManager.AppSettings["twitterConsumerSecret"]
            //    },
            //    PerformRedirect = authUrl => Response.Redirect(authUrl)
            //};
            ////RCampbell - 1/31 - Added to enable auth on record page
            //if (!Page.IsPostBack && Request.QueryString["oauth_token"] != null)
            //{
            //    auth.CompleteAuthorization(Request.Url);
            //}

            //if (auth.IsAuthorized)
            //{
            //    twitterCtx = new TwitterContext(auth);
            //    Session["TwitterContext"] = twitterCtx;
            //    lblTwLogin.Text = "Connected to Twitter";
            //}
            //#endregion
        }

        protected void btnRecord_Click(object sender, EventArgs e)
        {
            var fullName = (string)Session["FullUserName"];
            var email = (string)Session["Email"];
            Session["SenderEmail"] = email;
            Session["SenderName"] = fullName;
            Session["Sender"] = fullName + "(" + email + ")";

            if ((txt_msg_description.Text == "") && (txt_msg_Title.Text == ""))
            {
                lblMsg.Text = "Please enter a message title and/or description.";
            }

            if ((txt_msg_description.Text != "") & (txt_msg_Title.Text != "") & (fullName != "") & (email != ""))
            {
                Session["Title"] = txt_msg_Title.Text;
                Session["Description"] = txt_msg_description.Text;
                flashContent.Visible = true;

                txt_msg_description.Enabled = false;
                txt_msg_Title.Enabled = false;
                lblMsg.Visible = false;
            }          
        }

        protected void txt_msg_Title_TextChanged(object sender, EventArgs e)
        {
            Session["Title"] = txt_msg_Title.Text;
        }

        protected void txt_msg_Title_TextChanged1(object sender, EventArgs e)
        {
            Session["Title"] = txt_msg_Title.Text;

        }

        //Twitter and Facebook functionality we never really used

        //protected void fbPostMessage_Click(object sender, EventArgs e)
        //{
        //    var userFirst = (string)Session["UserFirstName"];
        //    var title = (string)Session["Title"];
        //    var currentTime = DateTime.Now.ToShortTimeString();

        //    if (Session["FbAccessToken"] != null)
        //    {
        //        var fb = new FacebookClient(Session["FbAccessToken"].ToString());
        //        fb.Post("/me/feed", new { message = String.Format(userFirst + " recorded a message titled '" + title + "' at {0}", currentTime) });
        //    }
        //    if (Session["TwitterContext"] != null)
        //    {
        //        twitterCtx = (TwitterContext)Session["TwitterContext"];
        //        twitterCtx.UpdateStatus(String.Format(userFirst + " recorded a message titled '" + title + "' at {0}",
        //            currentTime));
        //    }
        //    Response.Redirect("Record.aspx");
        //}

        //protected void imgBtnFbLogin_Click(object sender, ImageClickEventArgs e)
        //{
        //    //var pathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
        //    //var fbCallbackUri = new Uri(Request.Url.AbsoluteUri.Replace(pathAndQuery, "Record.aspx"));

        //    if (lblFbLogin.Text == "Logout of Facebook")
        //    {
        //        Logout();
        //    }
        //    else
        //    {
        //        var fb = new FacebookClient();

        //        var loginUrl = fb.GetLoginUrl(new
        //        {
        //            client_id = fbAppId,
        //            redirect_uri = "https://bellybuds.voiceshare.net/record.aspx",
        //            response_type = "code",
        //            scope = "publish_stream,email" // Add other permissions as needed
        //        });
        //        Response.Redirect(loginUrl.AbsoluteUri);
        //    }
        //}

        //private void Logout()
        //{
        //    //var pathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
        //    //var fbCallbackUri = new Uri(Request.Url.AbsoluteUri.Replace(pathAndQuery, "Record.aspx"));
        //    var fb = new FacebookClient();

        //    var logoutUrl = fb.GetLogoutUrl(new
        //    {
        //        access_token = Session["FbAccessToken"],
        //        next = "https://bellybuds.voiceshare.net/record.aspx"
        //    });

        //    // User Logged out, remove access token from session
        //    Session.Remove("FbAccessToken");

        //    Response.Redirect(logoutUrl.AbsoluteUri);
        //}

        //protected void imgBtnTwLogin_Click(object sender, ImageClickEventArgs e)
        //{
        //    auth.BeginAuthorization(Request.Url, true);
        //}
    }
}
