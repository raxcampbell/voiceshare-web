﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VSLogin.Master" Async="true" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="VsClientBitBucket.Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            jQuery(function ($) {
                $('#' + '<%=txtRegKey.ClientID %>').mask('***-****-***', {
                    completed: function () { $('#' + '<%=txtRegKey.ClientID %>').val($(this).val().toUpperCase()); }
                })
            })
        });

        jQuery(function () {
            $('#' + '<%=btnRegSubmit.ClientID %>').click(function () {
                $('#' + '<%=lbl_Exception.ClientID %>').empty();
                $('#' + '<%=lblUserErr.ClientID %>').empty();
            })
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" style="margin-top: 35%;">
                <div class="redirect_link">
                    <a id="A1" style="text-decoration: none; color: #ffffff;" type="text/html" href="http://wavhello.com/pages/about-us/">WHAT ARE BELLYBUDS?</a>
                </div>
                <div class="redirect_link">
                    <a id="A2" style="text-decoration: none; color: #ffffff;" type="text/html" href="http://wavhello.com/products/bellybuds">PURCHASE BELLYBUDS</a>
                </div>
            </div>
            <div class="col-lg-5 col-md-5" style="margin-top: 10%;">
                <div class="custom-well">
                    <img src='images/LoginAndRegister/voiceshare_by_wavhello_logo_transparent_320x114.png' class="img-responsive center-block" alt="VoiceShare logo" />
                    <div id="registerContentDiv" runat="server">
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group">
                            <asp:TextBox
                                ID="txtFirstName"
                                CssClass="form-control"
                                alt="First Name Textbox"
                                TabIndex="1"
                                runat="server"
                                MaxLength="50"
                                PlaceHolder="First Name" Width="100%">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="valFirstName"
                                runat="server"
                                ControlToValidate="txtFirstName"
                                ErrorMessage="Enter first name"
                                Display="None"
                                ValidationGroup="RegErr"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group">
                            <asp:TextBox
                                ID="txtLastName"
                                CssClass="form-control"
                                alt="Last Name Textbox"
                                TabIndex="2"
                                PlaceHolder="Last Name"
                                runat="server"
                                MaxLength="50" Width="100%">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="valLastName"
                                runat="server"
                                ControlToValidate="txtLastName"
                                ErrorMessage="Enter last name"
                                Display="None"
                                ValidationGroup="RegErr">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xs-12">
                        <p style="font-size: small;">**Password must be at least 6 characters long, contain at least 1 uppercase, 1 lowercase character along with at least 1 number.</p>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group">
                            <asp:TextBox
                                ID="txtPassword"
                                CssClass="form-control"
                                alt="Password Textbox"
                                TabIndex="3"
                                runat="server"
                                PlaceHolder="Password"
                                TextMode="Password"
                                Width="100%">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="valPassword"
                                runat="server"
                                ControlToValidate="txtPassword"
                                ErrorMessage="Enter a password"
                                Display="None"
                                EnableClientScript="True"
                                ValidationGroup="RegErr">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator
                                ID="valRegExUpperCase"
                                runat="server"
                                ErrorMessage="Password requires at least one uppercase letter, at least one number, and at least 6 characters"
                                ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$"
                                ValidationGroup="RegErr"
                                Display="None"
                                EnableViewState="True"
                                ControlToValidate="txtPassword">
                            </asp:RegularExpressionValidator>
                            <%--<asp:RegularExpressionValidator
                                ID="valRegExNumber"
                                runat="server"
                                ErrorMessage="Password requires at least one number"
                                ControlToValidate="txtPassword"
                                Display="None"
                                ValidationGroup="RegErr"
                                ValidationExpression="^(?=.*\d)$">
                            </asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator
                                ID="valRegExMinimum"
                                runat="server"
                                ErrorMessage="Password requires at least six characters"
                                ControlToValidate="txtPassword"
                                Display="None"
                                ValidationGroup="RegErr"
                                ValidationExpression="^[a-zA-Z\d]{6,}$">
                            </asp:RegularExpressionValidator>--%>
                            <%--<asp:RangeValidator
                                ID="valPassLength"
                                runat="server"
                                ErrorMessage="Password must be at least 6 characters long"
                                ControlToValidate="txtPassword"
                                Display="None"
                                MinimumValue="6"
                                ValidationGroup="RegErr"
                                MaximumValue="25"
                                Type="Integer">
                            </asp:RangeValidator>--%>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group">
                            <asp:TextBox
                                ID="txtConfirmPassword"
                                CssClass="form-control"
                                alt="Confirm Password Textbox"
                                TabIndex="4"
                                runat="server"
                                PlaceHolder="Confirm Password"
                                TextMode="Password" Width="100%">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="valConfirm"
                                runat="server"
                                ControlToValidate="txtConfirmPassword"
                                ErrorMessage="Confirm password"
                                Display="None"
                                EnableClientScript="True"
                                ValidationGroup="RegErr">
                            </asp:RequiredFieldValidator>
                            <asp:CompareValidator
                                ID="valPasswords"
                                runat="server"
                                ErrorMessage="Passwords supplied do not match"
                                ControlToCompare="txtConfirmPassword"
                                ControlToValidate="txtPassword"
                                Display="None"
                                ValidationGroup="RegErr">
                            </asp:CompareValidator>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xs-12">
                        <div class="form-group text-center">
                            <asp:TextBox
                                ID="txtZIP"
                                CssClass="form-control"
                                alt="Postal Code Textbox"
                                TabIndex="5"
                                runat="server"
                                PlaceHolder="Postal Code" Width="100%">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="valZIP"
                                runat="server"
                                ControlToValidate="txtZIP"
                                ErrorMessage="Enter a postal code"
                                Display="None"
                                ValidationGroup="RegErr">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <asp:TextBox
                                ID="txtEmail"
                                CssClass="form-control"
                                alt="Email Textbox"
                                TabIndex="6"
                                runat="server"
                                MaxLength="50"
                                PlaceHolder="Email Address"
                                TextMode="Email" Width="100%">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="valEmail"
                                runat="server"
                                ControlToValidate="txtEmail"
                                ErrorMessage="Enter an email address"
                                Display="None"
                                EnableClientScript="True" ValidationGroup="RegErr">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator
                                ID="valCorrectEmail"
                                runat="server"
                                ControlToValidate="txtEmail"
                                ErrorMessage="Enter a valid e-mail address"
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                Display="None" ValidationGroup="RegErr">
                            </asp:RegularExpressionValidator>
                        </div>
                        <div class="form-group">
                            <asp:TextBox
                                ID="txtEmailConfirm"
                                CssClass="form-control"
                                alt="Email Confirm Textbox"
                                TabIndex="7"
                                runat="server"
                                PlaceHolder="Confirm Email"
                                TextMode="Email" Width="100%"></asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="valReqEmailConfirm"
                                runat="server"
                                ControlToValidate="txtEmailConfirm"
                                ErrorMessage="Confirm email"
                                Display="None"
                                EnableClientScript="True" ValidationGroup="RegErr">
                            </asp:RequiredFieldValidator>
                            <asp:CompareValidator
                                ID="valCompareEmail"
                                runat="server"
                                ControlToCompare="txtEmail"
                                ControlToValidate="txtEmailConfirm"
                                ErrorMessage="Email fields do not match"
                                Display="None"
                                ValidationGroup="RegErr">
                            </asp:CompareValidator>
                        </div>
                        <div class="form-group text-center">
                            <asp:TextBox
                                ID="txtRegKey"
                                CssClass="form-control"
                                alt="Access Code Textbox"
                                TabIndex="8"
                                runat="server"
                                PlaceHolder="Access Code" Width="100%">
                            </asp:TextBox>
                        </div>
                        <div class="form-group text-center">
                            <asp:CheckBox ID="chkTerms" alt="Terms and Conditions Checkbox" TabIndex="12" runat="server" />
                            <span style="font-size: 14px;">Yes, I agree to the</span>
                            <asp:HyperLink ID="lnkTerms" TabIndex="13" runat="server" NavigateUrl="http://www.wavhello.com/terms-of-use"><span style="font-size: 14px;">Terms and Conditions</span></asp:HyperLink>
                        </div>
                    </div>
                    <a id="btnRegSubmit" runat="server" onserverclick="btnRegSubmit_Click" causesvalidation="True" validationgroup="RegErr" class="ghost-button">Click to register</a>
                    <%--                    <asp:Button ID="btnRegSubmit"
                        alt="Register Button"
                        TabIndex="14"
                        runat="server"
                        Text="Click to register"
                        OnClick="btnRegSubmit_Click"
                        Style="text-decoration: none"
                        Font-Bold="True"
                        CausesValidation="true"
                        CssClass="button center-block"
                        ValidationGroup="RegErr" />--%>
                    <div class="hidden">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary
                            ID="valSummary"
                            runat="server"
                            HeaderText="Please correct the following error(s):"
                            DisplayMode="List"
                            ForeColor="#FF9999"
                            ShowMessageBox="True"
                            ShowSummary="False"
                            ValidationGroup="RegErr" />
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblTerms" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                        <asp:Label ID="lbl_Exception" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                        <asp:Label ID="lblUserErr" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--Rex commented this 1/20/2016
            <div class="hidden">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <asp:ValidationSummary
                ID="valSummary"
                runat="server"
                HeaderText="Please correct the following error(s):"
                DisplayMode="List"
                ForeColor="#FF9999"
                ShowMessageBox="True"
                ShowSummary="False"
                ValidationGroup="RegErr" />
        </div>
        <div class="error_container">
            <asp:Label ID="lblTerms" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
            <br />
            <asp:Label ID="lbl_Exception" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
            <br />
            <asp:Label ID="lblUserErr" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        </div>--%>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-15214788-2']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</asp:Content>
