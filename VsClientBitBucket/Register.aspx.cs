﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using VsClientBitBucket.Class;

namespace VsClientBitBucket
{
    public partial class Register : System.Web.UI.Page
    {
        private const int TenantId = 1005;
        private readonly string _serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"];

        protected void Page_Load(object sender, EventArgs e)
        {
            string url = Request.Url.AbsoluteUri;

            registerContentDiv.InnerHtml = "<h3>Welcome to VoiceShare by WavHello!</h3>" +
                                           "<p>VoiceShare is our free recording service that allows you to share your voice from wherever you are. Record a story, a song, a moment - right to the site. Or else invite others to join your network so they can share recordings with you. With a few simple clicks, messages can be downloaded and shared with the little loved ones in your life.</p>";

            if (url.Contains("http://bellybuds.voiceshare.net"))
            {
                Response.Redirect(url.Replace("http://", "https://"));
            }

            if (!IsPostBack)
            {
                string parameter = Request.QueryString["parameter"];
                if (parameter == "Invite" || parameter == "InviteWithField")
                {
                    var senderFullName = Session["SenderFullName"];
                    registerContentDiv.InnerHtml = "<h3>Welcome to VoiceShare by WavHello!</h3>" +
                                                   "<p>Complete your registration below to connect with " +
                                                   senderFullName + " and start sharing!</p>" +
                                                   "<p>VoiceShare is our free recording service that allows you to share your voice from wherever you are. Record a story, a song, a moment - right to the site. Or else invite others to join your network so they can share recordings with you. With a few simple clicks, messages can be downloaded and shared with the little loved ones in your life.</p>";
                    txtEmail.Text = (string)Session["AccepterEmail"];
                    txtEmailConfirm.Text = (string)Session["AccepterEmail"];
                    txtEmail.Enabled = false;
                    txtEmailConfirm.Enabled = false;
                    txtRegKey.Enabled = false;

                    txtFirstName.Text = (string)Session["AccepterFirstName"];
                    txtLastName.Text = (string)Session["AccepterLastName"];
                }
            }
            else
            {
                if ((string)Session["btnRegSubmitClicked"] == "True")
                {

                    txtPassword.Text = Session["txtPassword"].ToString();
                    txtConfirmPassword.Text = Session["txtPassword"].ToString();
                }
            }

            UtilsWebService.UtilsClient UtilsCall = new UtilsWebService.UtilsClient();
            UtilsWebService.CountryList CountryDropDown = UtilsCall.EditFormCountryDropDown();
            //ddlCountry.DataSource = CountryDropDown.List;
            //ddlCountry.DataBind();
        }

        #region DataBase
        public string SaveToBBvsDataBase(string userfullname, string useremail, string userphone, string username, string userpassword, string confirmcode)
        {

            SqlConnection SqlConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["bbvs_d01ConnectionString"].ConnectionString.ToString());
            System.Data.SqlClient.SqlCommand sqlcomm = new System.Data.SqlClient.SqlCommand("CreateBBUser", SqlConn);

            string returnValue = string.Empty;

            try
            {
                SqlConn.Open();
                sqlcomm.CommandType = CommandType.StoredProcedure;

                sqlcomm.Parameters.Add(new SqlParameter("@userfullname", SqlDbType.VarChar));
                sqlcomm.Parameters.Add(new SqlParameter("@useremail", SqlDbType.VarChar));
                sqlcomm.Parameters.Add(new SqlParameter("@userphone", SqlDbType.VarChar));
                sqlcomm.Parameters.Add(new SqlParameter("@username", SqlDbType.VarChar));
                sqlcomm.Parameters.Add(new SqlParameter("@userpassword", SqlDbType.VarChar));
                sqlcomm.Parameters.Add(new SqlParameter("@confirmcode", SqlDbType.VarChar));

                sqlcomm.Parameters[0].Value = userfullname;
                sqlcomm.Parameters[1].Value = useremail;
                sqlcomm.Parameters[2].Value = userphone;
                sqlcomm.Parameters[3].Value = username;
                sqlcomm.Parameters[4].Value = userpassword;
                sqlcomm.Parameters[5].Value = confirmcode;


                sqlcomm.ExecuteNonQuery();
                return "Success";
            }
            catch (SqlException ex)
            {
                //catch exception
                return "Unscuccess";
                //lbl_Exception.Text = ex.Message;
            }
            finally
            {
                SqlConn.Close();
                SqlConn.Dispose();
            }

        }

        public string readSaveTenantInternalID(string userfullname, string useremail, string userphone, string username, string userpassword, string confirmcode)
        {
            SqlConnection SqlConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["bbvs_d01ConnectionString"].ConnectionString.ToString());
            System.Data.SqlClient.SqlCommand sqlcomm = new System.Data.SqlClient.SqlCommand("SelectBBUserId", SqlConn);

            try
            {
                SqlConn.Open();
                sqlcomm.CommandType = CommandType.StoredProcedure;

                sqlcomm.Parameters.Add(new SqlParameter("@userfullname", SqlDbType.VarChar));
                sqlcomm.Parameters.Add(new SqlParameter("@useremail", SqlDbType.VarChar));
                sqlcomm.Parameters.Add(new SqlParameter("@userphone", SqlDbType.VarChar));
                sqlcomm.Parameters.Add(new SqlParameter("@username", SqlDbType.VarChar));
                sqlcomm.Parameters.Add(new SqlParameter("@userpassword", SqlDbType.VarChar));
                sqlcomm.Parameters.Add(new SqlParameter("@confirmcode", SqlDbType.VarChar));


                sqlcomm.Parameters[0].Value = userfullname;
                sqlcomm.Parameters[1].Value = useremail;
                sqlcomm.Parameters[2].Value = userphone;
                sqlcomm.Parameters[3].Value = username;
                sqlcomm.Parameters[4].Value = userpassword;
                sqlcomm.Parameters[5].Value = confirmcode;



                SqlDataReader reader = sqlcomm.ExecuteReader();
                string strUser = "";
                while (reader.Read())
                {
                    strUser = reader["id_user"].ToString();

                }
                return strUser;


            }
            catch (SqlException ex)
            {
                return "0";
                // do something
                //lbl_Exception.Text = ex.Message;
            }
            finally
            {
                SqlConn.Close();
                SqlConn.Dispose();
            }
        }
        #endregion

        protected async void btnRegSubmit_Click(object sender, EventArgs e)
        {
            var uriparameter = Request.QueryString["parameter"];

            var registerModel = new RegisterBindingModel();
            registerModel.Email = txtEmail.Text;
            registerModel.Password = txtPassword.Text;
            registerModel.ConfirmPassword = txtConfirmPassword.Text;
            registerModel.FirstName = txtFirstName.Text;
            registerModel.LastName = txtLastName.Text;
            registerModel.PostalCode = txtZIP.Text;
            registerModel.TenantId = TenantId;

            var urlPath = "";

            if (chkTerms.Checked == false)
            {
                lblTerms.Text = "Acceptance of Terms and Conditions required.";
                return;
            }

            if (uriparameter == "Invite" || uriparameter == "InviteWithField")
            {
                registerModel.InviteId = (string)Session["inviteId"];
                urlPath = "api/Account/SiteRegisterInvite";
            }
            else
            {
                urlPath = "api/Account/SiteRegister";
            }

            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(_serviceUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                var responseMessage = await httpClient.PostAsJsonAsync(urlPath, registerModel);
                if (responseMessage.Content != null)
                {
                    var registerResult = await responseMessage.Content.ReadAsAsync<string>();
                    if (registerResult.Equals("Site registration successful"))
                    {
                        Response.Redirect("Result.aspx?url=ConfirmEmail");
                    }
                    if (registerResult.Equals("The email provided is already in our system. Try another email address."))
                    {
                        lbl_Exception.Text = registerResult;
                    }
                }
            }
        }

        //protected void btnRegSubmit_Click(object sender, EventArgs e)
        //{
        //    //Rex - 12/23/15 - Commented out because unnecessary
        //    // Task 194 - wsilin - 7/25/13 - logging of activity
        //    //string UPLOAD_DIRECTORY2 = "Exception/";
        //    //string uploadDirectory2 = HttpContext.Current.Server.MapPath(UPLOAD_DIRECTORY2);
        //    //string exceptionFile2 = "exceptionlog.txt";
        //    //string uploadFile2 = Path.Combine(uploadDirectory2, exceptionFile2);
        //    //string New2 = "New";
        //    //DateTime DateTime2 = System.DateTime.Now;
        //    //string MethodName2 = "Register-lnkRegSubmit_Click";
        //    //string exception2 = "Register Attempted from " + HttpContext.Current.Request.Url.Host.ToString() + " by user " + txtUserId.Text + " " + Request.QueryString["parameter"] +
        //    //  "    Values: " + txtFirstName.Text + " " + txtLastName.Text + " " + txtEmail.Text + " " + txtEmailConfirm.Text + " " + ddlGender.SelectedValue + " " + txtPassword.Text + " " + txtConfirmPassword.Text + " " + txtRegKey.Text + " " + txtZIP.Text + " " + ddlCountry.SelectedValue + " " + chkTerms.Checked;
        //    //string[] ExceptionOccur2 = { " ", New2, "Date:", DateTime2.ToString(), "MethodName:", MethodName2, "Exception:", exception2 };
        //    //File.AppendAllLines(uploadFile2, ExceptionOccur2);

        //    string BBUserId = "";
        //    Session["RegSubmitClicked"] = "True";
        //    Session["txtFirstName"] = txtFirstName.Text;
        //    Session["txtLastName"] = txtLastName.Text;
        //    Session["txtEmail"] = txtEmail.Text;
        //    //RCampbell - 8/8/13 - Possible registration issue
        //    //string userId = txtUserId.Text;
        //    //userId = userId.Replace(" ", String.Empty);
        //    //Session["txtUserId"] = txtUserId.Text;
        //    //Session["ddlGender"] = ddlGender.SelectedValue;
        //    Session["txtPassword"] = txtPassword.Text;
        //    Session["txtConfirmPassword"] = txtConfirmPassword.Text;
        //    Session["txtRegKey"] = txtRegKey.Text;
        //    Session["txtZIP"] = txtZIP.Text;
        //    //Session["ddlCountry"] = ddlCountry.SelectedValue;

        //    lblTerms.Text = "";
        //    lbl_Exception.Text = "";
        //    lblUserErr.Text = "";

        //    // wsilin 7/30/13 - set here to have access throughout proc
        //    string uriparameter = Request.QueryString["parameter"];

        //    if (chkTerms.Checked == false)
        //    {
        //        lblTerms.Text = "Acceptance of Terms and Conditions required.";
        //        return;
        //    }
        //    Session["chkTerms"] = chkTerms.Checked;

        //    string TenantDomain = HttpContext.Current.Request.Url.Host.ToString();
        //    //string TenantDomain = ConfigurationManager.AppSettings["domain"];
        //    var register = new SecureVoiceWebService.RegisterServiceClient();
        //    bool TenantRegkey = false;
        //    // Rcampbell - 8/7/13 - Is the following if (!String.IsNullOrWhiteSpace(txtRegKey.Text)) statement necessary since its optional?
        //    if (!String.IsNullOrWhiteSpace(txtRegKey.Text))
        //    {
        //        TenantRegkey = register.selectTenantRegKey(txtRegKey.Text, TenantDomain);
        //        if (TenantRegkey == false)
        //        {
        //            lbl_Exception.Text = "The access code is invalid. Please verify your code.";
        //            return;
        //        }
        //    }
        //    else
        //    {
        //        txtPassword.Text = Session["txtPassword"].ToString();
        //        txtConfirmPassword.Text = Session["txtConfirmPassword"].ToString();
        //    }

        //    //if ((txtFirstName.Text != "") && (txtLastName.Text != "") && (txtUserId.Text != "") && (txtEmail.Text != "") && (txtPassword.Text != "") && (ddlGender.SelectedValue != "--Select One--") && (txtConfirmPassword.Text != "") && (chkTerms.Checked) && (IsValidEmail(txtEmail.Text)) && (txtPassword.Text == txtConfirmPassword.Text) && (txtZIP.Text != "") && (ddlCountry.SelectedValue != "--Select One--"))
        //    if ((txtFirstName.Text != "") && (txtLastName.Text != "") && (txtEmail.Text != "") && (txtPassword.Text != "") && (txtConfirmPassword.Text != "") && (chkTerms.Checked) && (IsValidEmail(txtEmail.Text)) && (txtPassword.Text == txtConfirmPassword.Text) && (txtZIP.Text != ""))
        //    {
        //        //checks to see whether the primary email already exist or not
        //        string resultUniquenessEmail = register.CheckUniquenessEmail(txtEmail.Text);
        //        //checks to see whether the UserId already exist or not
        //        //bool ResultUserExistence = register.CheckUniquenessUserID(txtUserId.Text);

        //        //Rex - 12/23/15 - Took this out of if statement because unnecessary for app integration updates: && ResultUserExistence == false
        //        if (String.IsNullOrWhiteSpace(resultUniquenessEmail))
        //        {
        //            string nothing = "";
        //            string resgisterresult = "";

        //            if (uriparameter == "Invite" || uriparameter == "InviteWithField")
        //            {
        //                string inviteID = (string)Session["inviteId"];

        //                //Rex - 12/23/15 - Took this out of method argument statement because unnecessary for app integration updates:
        //                resgisterresult = register.RegisterDataThroughInvitation(txtEmail.Text, "", nothing, txtPassword.Text, txtLastName.Text, txtFirstName.Text, nothing, nothing, nothing, "", 0, DateTime.Now, "A", DateTime.Now, TenantDomain, BBUserId, inviteID, txtZIP.Text, "");
        //                //create connection

        //                if (resgisterresult == "Success")
        //                {
        //                    Response.Redirect("Result.aspx?url=RegisterInvite");
        //                }
        //                else
        //                {
        //                    lbl_Exception.Text = "An internal error has occurred.";
        //                }
        //            }
        //            else
        //            {
        //                resgisterresult = register.RegisterNewUser(txtEmail.Text, txtEmail.Text, nothing, txtPassword.Text, txtLastName.Text, txtFirstName.Text, nothing, nothing, nothing, "", 0, DateTime.Now, TenantDomain, BBUserId, txtRegKey.Text, txtZIP.Text, "", chkTerms.Checked);
        //                if (resgisterresult != "Unsuccess")
        //                {
        //                    //MailSubject
        //                    string MailSubject = "Bellybuds VoiceShare | Verify your email";
        //                    //MailBody
        //                    string MailBody = "<p>" + "Hello!"
        //                                      + "</p><p>" + "Thank you for registering with VoiceShare. Please click here to validate your account (or copy/paste link to browser) " + resgisterresult + "</p><br/><p>" + "If you have any questions, please don't hesitate to contact us at info@bellybuds.com" + "</p><br/><p>" + "Congratulations on the baby-to-be in your life!" + "</p><br/><p>" + "-Team Bellybuds" + "</p>";
        //                    string sendemailRegister = register.registerEmail(txtEmail.Text, MailSubject, MailBody);
        //                    if (sendemailRegister == "Success")
        //                    {
        //                        Response.Redirect("Result.aspx?url=Register");
        //                    }
        //                    else
        //                    {
        //                        string deletedatas = register.deletedatas(txtEmail.Text);//Delete all datas
        //                        //if (TenantRegkey)
        //                        //{
        //                        //    register.ResetAccessCode(txtRegKey.Text);
        //                        //}
        //                        lbl_Exception.Text = "There was an error during registration. Please try to register again.";
        //                    }
        //                }
        //                else if (resgisterresult == "Unsuccess")
        //                {
        //                    lbl_Exception.Text = "An internal error has occurred.";
        //                }
        //            }
        //        }
        //        //RCampbell - 8/7/13 - Possibly make this a switch case with a third option of both exceptions combined?
        //        else
        //        {
        //            if (!String.IsNullOrWhiteSpace(resultUniquenessEmail))
        //            {
        //                lbl_Exception.Text = "The provided email address has already been registered.";
        //            }
        //            //Rex - 12/23/15 - interfered with app integration
        //            //if (ResultUserExistence == true)
        //            //{
        //            //    lblUserErr.Text = "The provided user name has already been registered.";
        //            //}
        //        }
        //    }

        //    //else
        //    //{
        //    //    lblTerms.Text = "Acceptance of Terms and Conditions required.";
        //    //}
        //}

        public bool IsValidEmail(string emailAddress)
        {
            // Return true if emailAddress is in valid e-mail format.
            return Regex.IsMatch(emailAddress, @"^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
        }

        protected void CustomValidatorChkBox_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (chkTerms.Checked)
            {
                args.IsValid = true;
            }
        }
    }
}