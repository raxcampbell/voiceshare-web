﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="Result.aspx.cs" Inherits="VsClientBitBucket.InvitationResult" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RegistrationResponse</title>
    <link href="Styles/Custom2.css" rel="stylesheet" />
    <link href="Styles/StyleSheet.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/modernizr-2.6.2.js"></script>
</head>
<body style="background: url('/images/invite_landing_ext2806.jpg') no-repeat fixed bottom; -ms-background-size: 100%; background-size: 100%;">
    <div id="fg_membersite">
        <form id="frmRegistrationResponse" runat="server">
            <div>
                <fieldset>
                    <legend>
                        <%--<asp:Label ID="lblResultLegend" runat="server" Text="Confirmation"></asp:Label>--%>
                    </legend>
                    <asp:Label ID="lblResultLegend" runat="server" Text="Confirmation"></asp:Label>
                    <div id="resultContentDiv" runat="server"></div>
                    <%--<div style="text-align: center">
                        <asp:Label ID="lbl_InvitationResult" runat="server" Font-Bold="True"></asp:Label>
                    </div>--%>
                    <div style="text-align: center">
                        <asp:HyperLink ID="hypLogin" runat="server" NavigateUrl="Login.aspx">Click here to login</asp:HyperLink>
                    </div>
                    <div style="text-align: center">
                        <asp:HyperLink ID="hypInvite" runat="server" NavigateUrl="Invite.aspx">Return to the invite page</asp:HyperLink>
                    </div>
                </fieldset>
            </div>
        </form>
    </div>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-15214788-2']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

</script>

</body>
</html>
