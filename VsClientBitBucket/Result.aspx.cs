﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;

namespace VsClientBitBucket
{
    public partial class InvitationResult : Page
    {
        //private readonly string _serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"];
        private readonly string _serviceUrl = "http://localhost:51845/";

        protected async void Page_Load(object sender, EventArgs e)
        {
            string response = Request.QueryString["url"];
            string regKey = Request.QueryString["registerKey"];
            string emailConfirmKey = Request.QueryString["emailConfirmKey"];

            if (!String.IsNullOrEmpty(response))
            {
                if (response == "Register" || response == "RegisterInvite")
                {
                    HttpCookie myCookie = new HttpCookie("bbCookie");
                    Response.Cookies.Add(myCookie);
                    lblResultLegend.Text = "Thank you for registering!";
                    resultContentDiv.InnerHtml = "<p><strong>And by the way, VoiceShare is now available as a free mobile app!</strong></p>" +
                                                 "<p>That's right - we've updated, streamlined and added more features to the VoiceShare experience so now it's easier to share and stay in touch with the little loved ones in your family.</p>" +
                                                 "<p>Just download the iOS or Android version of the app to your smartphone and start sharing!</p>" +
                                                 "<p><strong>**To ensure consistency, please be certain to use the same email when registering on the app as you just used when registering here.**</strong></p>" +
                                                 "<p style='text-align: center;'><a href='https://play.google.com/store/apps/details?id=com.wavhello.VoiceShare'><img src='/images/AppStoreBadges/en_app_rgb_wo_45.png' alt='Android store button'></a></p>" +
                                                 "<p style='text-align: center;'><a href='https://itunes.apple.com/us/app/voiceshare-by-wavhello/id1020772900?mt=8'><img src='/images/AppStoreBadges/Download_on_the_App_Store_Badge_US-UK_135x40.svg' alt='Apple store button'></a></p>" +
                                                 "<p><a href='/Login.aspx'>Continue to the web version of VoiceShare</a></p>";
                    //lbl_InvitationResult.Text = "Your registration was successful. You may now begin using the VoiceShare™ service.";
                    ////lbl_InvitationResult.Text = "Your registration was successful. We have sent a verification email to you, please click the link in that email to verify and activate your account.";
                    hypLogin.Visible = false;
                    hypInvite.Visible = false;
                }
                //else if (response == "RegisterInvite")
                //{
                //    HttpCookie myCookie = new HttpCookie("bbCookie");
                //    Response.Cookies.Add(myCookie);
                //    lblResultLegend.Text = "Thank you for registering!";
                //    //lbl_InvitationResult.Text = "Your registration is now complete.";
                //    //hypLogin.Visible = true;
                //    //hypInvite.Visible = false;
                //}
                else if (response == "Invite")
                {
                    lblResultLegend.Text = "Invite Success";
                    resultContentDiv.InnerHtml = "<p>Your invitation has been delivered</p>";
                    hypLogin.Visible = false;
                    hypInvite.Visible = true;

                }
                else if (response == "ConfirmEmail")
                {
                    lblResultLegend.Text = "Confirm Email";
                    resultContentDiv.InnerHtml = "<p>Please check your email to confirm your registration</p>";
                    hypLogin.Visible = true;
                    hypInvite.Visible = false;
                }
                else if (response == "passwordRecovery")
                {
                    lblResultLegend.Text = "Password Recovery";
                    resultContentDiv.InnerHtml = "<p>Your username and password has been sent to your e-mail address</p>";
                    hypLogin.Visible = true;
                    hypInvite.Visible = false;
                }
                else if (response == "InviteFail")
                {
                    lblResultLegend.Text = "Invite Failure";
                    resultContentDiv.InnerHtml = "<p>Sorry! Your invitation has not been sent successfully. Please try again</p>";
                    hypLogin.Visible = false;
                    hypInvite.Visible = true;
                } 
            }
            if (!String.IsNullOrEmpty(emailConfirmKey))
            {
                await ConfirmSecondaryEmail(emailConfirmKey);
                lblResultLegend.Text = "Secondary Email Confirmed";
                resultContentDiv.InnerHtml = "<p>You have now registered a secondary email with VoiceShare. The change will now be reflected in the app.</p>" +
                                             "<p><a href='/Login.aspx'>Continue to the web version of VoiceShare</a></p>";
            }
            else
            {
                await ConfirmUserAccount(regKey);
                lblResultLegend.Text = "Thank you for registering!";
                resultContentDiv.InnerHtml = "<p><strong>And by the way, VoiceShare is now available as a free mobile app!</strong></p>" +
                                             "<p>That's right - we've updated, streamlined and added more features to the VoiceShare experience so now it's easier to share and stay in touch with the little loved ones in your family.</p>" +
                                             "<p>Just download the iOS or Android version of the app to your smartphone and start sharing!</p>" +
                                             "<p><strong>**To ensure consistency, please be certain to use the same email when registering on the app as you just used when registering here.**</strong></p>" +
                                             "<p style='text-align: center;'><a href='https://play.google.com/store/apps/details?id=com.wavhello.VoiceShare'><img src='/images/AppStoreBadges/en_app_rgb_wo_45.png' alt='Android store button'></a></p>" +
                                             "<p style='text-align: center;'><a href='https://itunes.apple.com/us/app/voiceshare-by-wavhello/id1020772900?mt=8'><img src='/images/AppStoreBadges/Download_on_the_App_Store_Badge_US-UK_135x40.svg' alt='Apple store button'></a></p>" +
                                             "<p><a href='/Login.aspx'>Continue to the web version of VoiceShare</a></p>";
                hypLogin.Visible = false;
                hypInvite.Visible = false;
            }
        }

        private async Task ConfirmUserAccount(string regKey)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(_serviceUrl);
                var content = new FormUrlEncodedContent(new[] 
                {
                    new KeyValuePair<string, string>("", regKey)
                });

                var response = await httpClient.PostAsync("api/Person/ConfirmUserAccount", content);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    //Future code
                }
            }
        }

        private async Task ConfirmSecondaryEmail(string emailConfirmKey)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(_serviceUrl);
                var content = new FormUrlEncodedContent(new[] 
                {
                    new KeyValuePair<string, string>("", emailConfirmKey)
                });

                var response = await httpClient.PostAsync("api/Person/ConfirmSecondaryEmail", content);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync();
                }
            }
        }
    }
}
