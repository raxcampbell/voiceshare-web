﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RevisedInvite.aspx.cs" Inherits="VsClientBitBucket.RevisedInvite" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Styles/Custom2.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        
        .auto-style2 {
        }
        .auto-style3 {
        }
        .auto-style5 {
            width: 142px;
        }
        .auto-style6 {
            width: 142px;
            font-family: "Lucida Sans Unicode";
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
     <div>
            <div id="wrapper">
                <div id="page">
                    <!--BEGIN HEADER -->
                    <div id="header">
                        <a style="float: left" href="http://www.bellybuds.com">
                            <img alt="Bellybuds" src="Images/BB_Linear_Logo_Registered.jpg" class="headerimage">
                        </a>
                        <div style="display: none; position: absolute" id="loginsystem">
                            <div style="position: relative">
                                <a id="openreg" href="javascript:void(0);">Login/register</a>

                            </div>
                        </div>
                        <!-- iphone icons -->
                        <div class="icons">
                            <div class="nav-icon">
                                <asp:ImageButton ID="btnFacebook" PostBackUrl="http://www.facebook.com/bellybuds" ImageUrl="images/facebook.png" runat="server"></asp:ImageButton>
                            </div>
                            <div class="nav-icon">
                                <asp:ImageButton ID="btnTwitter" PostBackUrl="http://www.twitter.com/bellybuds" ImageUrl="images/twitter.png" runat="server"></asp:ImageButton>
                            </div>
                        </div>
                        <!-- /end iphone icons -->
                    </div>
                    <!--END OF HEADER -->
                    <!--BEGIN CONTENT -->
                    <div id="content">
                        <div class="outside">
                            <div class="inside">
                                <div class="inside_full">
                                    <div style="text-align: right; font-size: 10px">
                                        (Not "<asp:Label ID="lbl_UserName" runat="server" />
                                        "?,<a href="Login.aspx">Sign In</a>)
                                    </div>
                                    <ul id="profile_bar">
                                        <li><a href="Record.aspx">Record a Message</a></li>
                                        <li><a href="Invite.aspx">Send an Invitation</a></li>
                                        <li><a href="Dash.aspx">My Dashboard</a></li>
                                        <li><a href="Signout.aspx">Sign Out</a></li>
                                        <li><a href="Terms.aspx">Terms and Services</a></li>
                                    </ul>
                                    <div class="inside_left">
                                        <img src="images/voiceshare_logo.jpg">
                                        <h3>Invite friends or family</h3>


                                        <p>
                                            The information you fill in below will create an email invitation to send to your friends and family. It will link back to VoiceShare so that they may record a message for baby-to-be!
			

                                        </p>

                                        <ul style="list-style-type: disc; margin: 5px 0 15px; padding-left: 20px;">
                                            <li>You can create and send multiple invitations.</li>



                                            <li>You can send each invitation to as many people as
            you'd like.</li>
                                        </ul>

                                        <p>There is an option to add a "personalized message" as well.  Feel free to use that space just to say 'hi' or perhaps to suggest ideas for what the recipient should record.  Or you can just leave it blank (we do provide suggestions for what to say on the recording page).</p>

                                        <p>Only the blanks with asterisks are necessary; everything else is optional.</p>
                                    </div>
                                    <div class="inside_right" style="padding-top: 105px;">
                                        <asp:Image ID="Image1" runat="server" ImageAlign="right" ImageUrl="Http://www.bellybuds.com/wp-content/plugins/voiceshare/VoiceShare-Create-an-Invitation.jpg" /><!--<img align="right" src="http://www.bellybuds.com/wp-content/plugins/voiceshare/VoiceShare-Create-an-Invitation.jpg">-->
                                    </div>
                                </div>
                                <div class="clear_all"></div>
                                <div class="inside_full">
                                    <div class="fboxes2" id="vsFormWrapper">


                                        <div class="vs-row vs-row-sect fboxes">
                                            <div class="vs-row">                                               
                                                <p>
                                                    Invitation Title (For your Belly Bud&#39;s archives - for example, &quot;Invitation to Aunt Sue&quot;)*
                                                </p>
                                                <table class="auto-style1">
                                                    <tr>
                                                        <td class="auto-style2">
                                                            <asp:TextBox ID="TextBox2" runat="server" Width="310px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <p>
                                                    Your Personalized Message (You may edit to say whatever you&#39;d like!)
                                                </p>
                                                <table class="auto-style1">
                                                    <tr>
                                                        <td class="auto-style3">
                                                            <asp:TextBox ID="txtPersonalMsg" runat="server" Columns="100" Rows="8" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="vs-row vs-row-sect fboxes">
                                            <p>Enter email address(es) here:</p>
                                            <asp:TextBox ID="txtInviteEmail" runat="server" Width="660px"></asp:TextBox>
                                        </div>
                                        <div class="vs-row vs-row-sect" id="vsSubmitWrapper">
                                            <div style="text-align: center" class="vs-row">
                                                <div>
                                                    <span style="display: none">
                                                        <asp:Button ID="btnPreview" runat="server" Text="Preview Invitation"></asp:Button></span>
                                                    <span>
                                                        <asp:Button ID="btnContinue" runat="server" Text="Submit" OnClick="btnContinue_Click"></asp:Button></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear_all"></div>
                        </div>
                        <div class="outside">
                            <div class="inside inside_no_padding footer">
                                <div class="fix" id="fcolumns_container">
                                    <div class="fcol">
                                    </div>
                                    <div class="fcol">
                                        <div class="fcol_pad">
                                            <h3>Pages</h3>
                                            <ul id="footnav">
                                                <li class="page_item "><a title="Home" href="http://www.bellybuds.com/" class="home">Home</a></li>
                                                <li class="page_item page-item-36"><a title="Contact Us" href="http://www.bellybuds.com/contact-us/">Contact Us</a></li>
                                                <li class="page_item page-item-8"><a title="About" href="http://www.bellybuds.com/prenatal_music_history/">About</a></li>
                                                <li class="page_item page-item-42"><a title="Blog" href="/category/blog">Blog</a></li>
                                                <li class="page_item page-item-2768"><a title="Help" href="http://www.bellybuds.com/help/">Help</a></li>
                                                <li class="page_item page-item-772"><a title="Problem?  Suggestion?" href="http://www.bellybuds.com/report-a-bug/">Problem?  Suggestion?</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="fcol">
                                        <div class="fcol_pad">
                                            <h3>Stay In Touch</h3>
                                            <ul>
                                                <li><a class="rsslink" href="http://www.bellybuds.com/feed/">Site RSS Feed</a></li>
                                                <li><a class="twitterlink" href="http://www.twitter.com/bellybuds">Twitter</a></li>
                                                <li><a class="facebooklink" href="http://www.facebook.com/bellybuds">Facebook</a></li>
                                            </ul>                                            
                                            <script type="text/javascript">var switchTo5x = true;</script>
                                            <script src="http://w.sharethis.com/button/buttons.js" type="text/javascript"></script>
                                            <script type="text/javascript">								    stLight.options({ publisher: 'aa0a6497-fadd-4da7-8998-730c4484396d' });</script>
                                        </div>
                                    </div>
                                    <div class="fcol">
                                        <div class="fcol_pad">
                                            <span class="terms">&copy; 2011 BellyBuds, Patent Pending
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear_all"></div>
                            </div>
                        </div>
                    </div>
                    <!--END OF CONTENT -->
                    <!--BEGIN FOOTER -->
                </div>
                <!--END OF FOOTER -->
            </div>
        </div>
    </form>
</body>
</html>
