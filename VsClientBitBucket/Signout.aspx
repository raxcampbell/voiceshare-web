﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Signout.aspx.cs" Inherits="VsClientBitBucket.Signout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
      <title>Login</title>
      <link rel="STYLESHEET" type="text/css" href="style/fg_membersite.css" />
       <link href="Styles/Custom2.css" rel="stylesheet" />
     <link href="Styles/StyleSheet.css" type="text/css" rel="stylesheet" />
      <script type='text/javascript' src='scripts/gen_validatorv31.js'></script>
      <script type="text/javascript" src="Scripts/modernizr-2.6.2.js"></script>

</head>
    <body>
        <div id="fg_membersite">
            <form id="frmRegistrationResponse" runat="server">
                <div>
                    <fieldset>
                        <legend>Signout confirmation</legend>
                        <br />
                        <div>
                            <h2>You have logged out</h2>
                            <p>
                                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Login.aspx">Login again</asp:HyperLink>
                            </p>
                            <p>You will be redirected to the Belly Bud's homepage momentarily.</p>
                        </div>
                    </fieldset>
                </div>

                <script type="text/JavaScript">

                    setTimeout("location.href = 'http://voiceshare.wavhello.com';", 6000);

                </script>


            </form>
        </div>
</body>

</html>