﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubsribePage.aspx.cs" Inherits="VsClientBitBucket.SubsribePage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Invitation Information</title>
    <link href="Styles/Custom2.css" rel="stylesheet" />
    <link href="Styles/StyleSheet.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/modernizr-2.6.2.js"></script>
</head>
<%--<body style="background: url('/images/Background Picture BB.EXT_2806.jpg') no-repeat fixed center; -ms-background-size: 100%; background-size: 100%;">--%>
    <body style="background: url('/images/invite_landing_ext2806.jpg') no-repeat fixed bottom; -ms-background-size: 100%; background-size: 100%;">
    <div id="fg_membersite">
        <form id="frmRegistrationResponse" runat="server">
            
            <div>
                <fieldset>
                    <legend>Accept&nbsp; Invitation</legend>
                    <div id="contentDiv" runat="server"></div>
                    <%--<br />
                    <div>
                        You have been invited by:<br />
                        <asp:Label ID="lblSenderFullName" runat="server" Font-Bold="true"></asp:Label>
                    </div>
                    <div>
                        This invitation is for:<br />
                        <asp:Label ID="lblRecipientFullName" runat="server" Font-Bold="true"></asp:Label><asp:Label ID="lbl_AccepterFirstName" runat="server" Font-Bold="True" Visible="False"></asp:Label><asp:Label ID="lbl_AccepterLastName" runat="server" Font-Bold="True" Visible="False"></asp:Label>
                    </div>
                    <br />
                    <br />
                    <div>
                        <asp:HyperLink ID="hypRegister" runat="server" NavigateUrl="Register.aspx?parameter=Invite">Register to confirm your invitation.</asp:HyperLink>
                    </div>
                    <div>
                        <asp:HyperLink ID="hypLogin" runat="server" NavigateUrl="Login.aspx?parameter=invite">Login to record the message.</asp:HyperLink>
                    </div>
                    <div>
                        <asp:HyperLink ID="hypRegisterOtherTenant" runat="server" NavigateUrl="Register.aspx?parameter=InviteWithField">Our system indicates that you are already registered through another website. To access this website, you need to register.</asp:HyperLink>
                    </div>--%>
                </fieldset>
            </div>
        </form>
    </div>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-15214788-2']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

</script>
</body>
</html>
