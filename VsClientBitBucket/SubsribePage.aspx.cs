﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using VsClientBitBucket.Class;

namespace VsClientBitBucket
{
    public partial class SubsribePage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //try
            //{
            //Response.Redirect("RecipientRegister.aspx?buttom=1");
            var token = Session["AuthToken"] as string;
            string tenant = HttpContext.Current.Request.Url.Host.ToString();
            //string tenant = ConfigurationManager.AppSettings["DevDomain"];
            string inviteId = Request.QueryString["uriparameter"];
            Session["inviteId"] = inviteId;
            //string uriparameter = "6L7HjjkGP77bl55BrceAM6y1t3DMBa";

            //***************************************************************************************************************************
            VoiceWebService.RegisterServiceClient userexistence = new VoiceWebService.RegisterServiceClient();
            var existenceresult = userexistence.GetData(inviteId);
            //lets suppose they are not the member of that particular tenant

            //--rcampbell - 8/20/13 - updated labels to display a gap between first and last name. Previously, there was no gap.
            //lblSenderFullName.Text = existenceresult.FirstName + " " + existenceresult.LastName;
            var senderFullName = existenceresult.FirstName + " " + existenceresult.LastName;
            var recipientFullName = existenceresult.Recipient.RecipientFirstName + " " + existenceresult.Recipient.RecipientLastName;
            var accepterFirstName = existenceresult.Recipient.RecipientFirstName;
            var accepterLastName = existenceresult.Recipient.RecipientLastName;

            //lblRecipientFullName.Text = existenceresult.Recipient.RecipientFirstName + " " + existenceresult.Recipient.RecipientLastName;
            //lbl_AccepterFirstName.Text = existenceresult.Recipient.RecipientFirstName;
            //lbl_AccepterLastName.Text = existenceresult.Recipient.RecipientLastName;
            string recipientEmail = existenceresult.Recipient.RecipientEmail;
            Session["SenderFullName"] = senderFullName;
            Session["AccepterFirstName"] = accepterFirstName;
            Session["AccepterLastName"] = accepterLastName;
            //Session["AccepterFirstName"] = lbl_AccepterFirstName.Text;
            //Session["AccepterLastName"] = lbl_AccepterLastName.Text;
            Session["AccepterEmail"] = recipientEmail;

            ////***************************************************************************************************************************
            //// Rex - 1/17/2016 - New tenant is set in above code
            ////string tenant = HttpContext.Current.Request.Url.Host.ToString();
            ////string tenant = ConfigurationManager.AppSettings["domain"];
            ////***************************************************************************************************************************
            bool emailExistence = userexistence.CheckExistenceOfEmail(recipientEmail);
            if (emailExistence)
            {
                //get personID
                int personId = userexistence.returnPersonId(recipientEmail, tenant);
                bool returnPersonId = userexistence.CheckExistenceOfPersonWithTenant(tenant, personId);
                if (returnPersonId)
                {
                    //Add Connection
                    var generateConnectionExistingUser = new VoiceWebService.RegisterServiceClient();
                    string result = generateConnectionExistingUser.GenerateConnectionForOldUser(tenant, inviteId);
                    //if here, log in
                    contentDiv.InnerHtml = "<p>Congratulations - You and " + senderFullName + " are now connected!</p>" +
                                           "<p>NEW! VoiceShare is now available as a free mobile app with a streamlined experience and additional features! Download now:</p>" +
                                           "<a href='https://play.google.com/store/apps/details?id=com.wavhello.VoiceShare'><img src='/images/AppStoreBadges/en_app_rgb_wo_45.png' alt='Android store button'></a><br />" +
                                           "<a href='https://itunes.apple.com/us/app/voiceshare-by-wavhello/id1020772900?mt=8'><img src='/images/AppStoreBadges/Download_on_the_App_Store_Badge_US-UK_135x40.svg' alt='Apple store button'></a><br />" +
                                           "<a href='http://voiceshare.wavhello.com/Login.aspx'>Continue to the web version of VoiceShare</a>";

                    //*************************************************************************
                    // 1/17/2016 - Rex - Code commented to reflect changes per BellBuds request
                    //hypRegister.Visible = false;
                    //hypRegisterOtherTenant.Visible = false;
                    //hypAlready.Visible = false;
                    //Response.Redirect("Login.aspx?parameter=invite");
                    //*************************************************************************
                }
                else
                {
                    //If here, other tenant
                    //hypRegister.Visible = false;
                    //hypLogin.Visible = false;
                    //hypAlready.Visible = false;
                }

            }
            else
            {
                //If here, Register
                Response.Redirect("Register.aspx?parameter=InviteWithField");
                //hypRegisterOtherTenant.Visible = false;
                //hypLogin.Visible = false;
            }
            //************************************************************************************************************************
        }
    }
}