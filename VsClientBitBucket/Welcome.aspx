﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="VsClientBitBucket.HomeAlt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Belly Buds: Voiceshare Application</title>
    <link type="text/css" rel="stylesheet" href="Styles/Custom2.css" />
    <script type="text/javascript" src="Scripts/modernizr-2.6.2.js"></script>
</head>
<body class="home_start">
    <form id="form1" runat="server">
        <div id="wrapper">
            <div id="page">
                <!--BEGIN HEADER -->
                <div id="header">
                    <a style="float: left" href="http://voiceshare.wavhello.com/Dash.aspx">
                        <asp:Image ID="imgLogo" runat="server" ImageUrl="images/LoginAndRegister/voiceshare_by_wavhello_logo_transparent_320x114.png" />
                    </a>
                    <!-- iphone icons -->
                    <div class="icons">
                        <div class="nav-icon">
                            <asp:ImageButton ID="btnFacebook" PostBackUrl="http://www.facebook.com/wavhello" ImageUrl="images/facebook.png" runat="server"></asp:ImageButton>
                        </div>
                        <div class="nav-icon">
                            <asp:ImageButton ID="btnTwitter" PostBackUrl="http://www.twitter.com/wavhello" ImageUrl="images/twitter.png" runat="server"></asp:ImageButton>
                        </div>
                    </div>
                    <!-- /end iphone icons -->
                </div>
                <!--END OF HEADER -->
                <!--BEGIN CONTENT -->
                <div id="content">
                    <div class="outside">
                        <div class="inside">                            
                            <div class="inside_full">
                                <div style="text-align: right; font-size: 10px">Already a member?  <a href="Login.aspx">Click here</a> to sign in.</div>
                                <div class="inside_left">
                                    <asp:Image ID="vsLogo" runat="server" ImageUrl="images/voiceshare_logo.jpg" />
                                    <h3>Welcome to Bellybuds VoiceShare&trade;!</h3>
                                    <p>
                                        Now the baby-to-be in your life can become familiar
          with your voice, and those of other friends and family
          members.                 
                                    </p>
                                    <p>
                                        <i>This is
                                        <!--the beta version of -->
                                            our newly launched VoiceShare voice recording application.  If you encounter any problems or would like to suggest something to make the service better please drop us a line <a href="http://www.bellybuds.com/report-a-bug"><strong>HERE</strong></a> and just type VoiceShare in the subject heading. Thank you and enjoy!</i>
                                    </p>
                                    <p>Here's how it works:</p>
                                    <ol>
                                        <li>You record an audio message through VoiceShare and
            send it via email.</li>
                                        <li>The mom-to-be (or whomever you send it to) receives
            it at her email address.</li>
                                        <li>She plays it to her baby with <a target="_blank" href="http://bellybuds.amazonwebstore.com/">Bellybuds</a>.</li>
                                        <li>OR &mdash; you can choose to send invitations to
            others to record their own messages</li>
                                    </ol>
                                    <p>P.S. It's all free.</p>
                                    <p>
                                         <a href="Register.aspx">Click here</a> to register and start sharing with the new baby in your life!
                                    </p>
                                </div>
                                <div class="inside_right">
                                    <img style="float: right; padding: 5px" src="images/record.jpg">
                                </div>
                            </div>
                        </div>
                        <div class="clear_all"></div>
                    </div>
                    <div class="outside">
                        <div class="inside inside_no_padding footer">
                            <div class="fix" id="fcolumns_container">
                                <div class="fcol">
                                </div>
                                <div class="fcol">
                                    <div class="fcol_pad">
                                        <h3>Pages</h3>
                                        <ul id="footnav">
                                            <li class="page_item "><a title="Home" href="http://www.bellybuds.com/" class="home">Home</a></li>
                                            <li class="page_item page-item-36"><a title="Contact Us" href="http://www.bellybuds.com/contact-us/">Contact Us</a></li>
                                            <li class="page_item page-item-8"><a title="About" href="http://www.bellybuds.com/prenatal_music_history/">About</a></li>
                                            <li class="page_item page-item-42"><a title="Blog" href="/category/blog">Blog</a></li>
                                            <li class="page_item page-item-2768"><a title="Help" href="http://www.bellybuds.com/help/">Help</a></li>
                                            <li class="page_item page-item-772"><a title="Problem?  Suggestion?" href="http://www.bellybuds.com/report-a-bug/">Problem?  Suggestion?</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="fcol">
                                    <div class="fcol_pad">
                                        <h3>Stay In Touch</h3>
                                        <ul>
                                            <li><a class="rsslink" href="http://www.bellybuds.com/feed/">Site RSS Feed</a></li>
                                            <li><a class="twitterlink" href="http://www.twitter.com/bellybuds">Twitter</a></li>
                                            <li><a class="facebooklink" href="http://www.facebook.com/bellybuds">Facebook</a></li>
                                        </ul>
                                        <span class="st_facebook_large"><span style="text-decoration: none; color: #000000; display: inline-block; cursor: pointer;" class="stButton"><span class="stLarge" style="background-image: url(&quot;http://w.sharethis.com/images/facebook_32.png&quot;);"></span>
                                            <img src="http://w.sharethis.com/images/check-big.png" style="position: absolute; top: -7px; right: -7px; width: 19px; height: 19px; max-width: 19px; max-height: 19px; display: none;">
                                        </span></span>
                                    </div>
                                </div>
                                <div class="fcol">
                                    <div class="fcol_pad">
                                        <span class="terms">&copy; 2016 BellyBuds
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="clear_all"></div>
                        </div>
                    </div>
                </div>
                <!--END OF CONTENT -->
                <!--BEGIN FOOTER -->
            </div>
            <!--END OF FOOTER -->
        </div>
    </form>
</body>
</html>
