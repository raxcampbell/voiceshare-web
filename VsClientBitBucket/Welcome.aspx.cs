﻿using System;


namespace VsClientBitBucket
{
    public partial class HomeAlt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["bbCookie"] != null)
            {
                Response.Redirect("Login.aspx");
            }
        }        
    }
}