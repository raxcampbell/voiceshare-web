﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WhatToSay.aspx.cs" Inherits="VsClientBitBucket.WhatToSay" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Belly Buds: Voiceshare Application</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <link href="Styles/Custom2.css" rel="stylesheet" />
    <link href="Styles/Container.css" rel="stylesheet" />
    <script type="text/javascript">
        function goBack() {
            history.go(-1);
        }
    </script>
    <script type="text/javascript" src="swfobject.js"></script>
    <script type="text/javascript">
        //Adobe recommends that developers use SWFObject2 for Flash Player detection.
        //For more information see the SWFObject page at Google code (http://code.google.com/p/swfobject/).
        //Information is also available on the Adobe Developer Connection Under Detecting Flash Player versions and embedding SWF files with SWFObject 2"
        //Set to minimum required Flash Player version or 0 for no version detection
        //10.0.22 is the minimum Flash Player version taht properly supports the Speex audio codec used by default in FLVAR
        var swfVersionStr = "10.0.22";
        //xiSwfUrlStr can be used to define an express installer SWF.
        var xiSwfUrlStr = "";
        var flashvars = {
            userId: "123",
            recorderId: "456"
        };
        var params = {};
        params.quality = "high";
        params.bgcolor = "#ffffff";
        params.play = "true";
        params.loop = "true";
        params.wmode = "window";
        params.scale = "showall";
        params.menu = "true";
        params.devicefont = "false";
        params.salign = "";
        params.allowscriptaccess = "sameDomain";
        params.allowFullScreen = "false";
        var attributes = {};
        attributes.id = "audiorecorder";
        attributes.name = "audiorecorder";
        attributes.align = "middle";
        swfobject.createCSS("html", "height:100%; background-color: #ffffff;");
        //swfobject.createCSS("body", "margin:0; padding:0; overflow:hidden; height:100%;");
        swfobject.embedSWF(
            "audiorecorder.swf", "flashContent",
            "320", "140",
            swfVersionStr, xiSwfUrlStr,
            flashvars, params, attributes);
    </script>
</head>
<body>
    <form id="form1" runat="server">
            <div id="wrapper">
                <div id="page">
                    <div id="header">
                        <a style="float: left" href="http://voiceshare.wavhello.com/Dash.aspx">
                        <asp:Image ID="imgLogo" runat="server" ImageUrl="images/LoginAndRegister/voiceshare_by_wavhello_logo_transparent_320x114.png" />
                    </a>
                        <!-- iphone icons -->
                        <div class="icons">
                            <div class="nav-icon">
                                <asp:ImageButton ID="btnFacebook" runat="server" PostBackUrl="http://www.facebook.com/wavhello" ImageUrl="images/facebook.png"></asp:ImageButton>
                            </div>
                            <div class="nav-icon">
                                <asp:ImageButton ID="btnTwitter" runat="server" PostBackUrl="http://www.twitter.com/wavhello" ImageUrl="images/twitter.png"></asp:ImageButton>
                            </div>
                        </div>
                        <!-- /end iphone icons -->
                    </div>
                    <!--BEGIN CONTENT -->
                    <div id="content">
                        <div class="outside">
                            <div class="inside">
                                <div class="inside_full">
                                    <div style="text-align: right; font-size: 10px">
                                    (Not "<asp:Label ID="lbl_UserName" runat="server" />
                                    "?,<a href="Login.aspx">Sign In</a>)
                                </div>
                                <ul id="profile_bar">
                                    <li><a href="Record.aspx">Record a Message</a></li>
                                    <li><a href="Invite.aspx">Send an Invitation</a></li>
                                    <li><a href="Dash.aspx">My Dashboard</a></li>
                                    <li><a href="EditForm.aspx">Edit User Profile</a></li>
                                    <li><a href="FAQ.aspx">FAQ</a></li>
                                    <li><strong>You have <span runat="server" style="background-color: red; color: white; -ms-border-radius: 50%; border-radius: 50%; padding: 2px;" id="spanCounter"></span> connections.</strong></li>
                                </ul>
                                    <div class="inside_left">
                                        <img src="images/voiceshare_logo.jpg" alt="Record Click Share" />
                                        <h3>What To Say?</h3>
                                        <p>
                                            What you say isn’t as important as your tone of voice, and the real-life joy you express. Be
yourself, let your love come through. Your message will be absolutely perfect.
                                        </p>
                                        <p><strong>Here are some ideas to get you started:</strong></p>
                                        <p>
                                            <strong>Share your excitement about the new baby’s entry into the world.</strong>
                                            <br />
                                            Example: Hi, [Babyname] or [Little One]! I’m so glad you’re coming into the world. I can’t wait to
            meet you and hold you, and tell you in person how thrilled I am that you’ve come to join us.
            <br />
                                            <br />
                                            <strong>Read a story.</strong>
                                            Especially a story that has meaning for you, or that you’d like to give to the baby
            to enjoy into and through her childhood years.
                                        </p>
                                        <p>
                                            <strong>Describe what you’re doing, and relate it to the baby’s experience.</strong>
                                            <br />
                                            Example: We’re here in our house watching a movie about a whale. Whales are giant fish that
            swim in the ocean. Right now you probably can’t even imagine the size of the room you’re in, let
            alone and ocean, but that’s just one of many things I can’t wait to share with you.
                                        </p>
                                        <p>
                                            <strong>Sing a song. </strong>Even in the womb, babies respond positively to music. When you sing the same
            song to the baby after he’s born, he’ll already be familiar with it. Again, you needn’t be a rock
            star or opera singer. Just enjoy yourself, and share your joy.           
                                        </p>
                                        <p>
                                            <strong>Talk about a gift you’ll give the baby. Include other family members, if possible.</strong><br />
                                            For example: “Addy is holding up the teddy bear she wants to give you, and smiling a big smile.
            I think you’ll like his chocolate brown eyes and fuzzy whiskers. He can’t wait to share hugs with
            you.”           
                                        </p>
                                        <p>
                                            Again, it’s not so much what you say, but who you are and how you feel that makes the message
  special. Above all, have fun recording your message!
                                        </p>
                                        <asp:HyperLink ID="hypBack" runat="server" NavigateUrl="~/Record.aspx">Back to Record</asp:HyperLink>
                                    </div>
                                    <div class="inside_right">
                                        <img style="float: right; padding: 5px" src="images/record.jpg" alt="A mother recording a message" />
                                    </div>
                                </div>
                                <div class="clear_all"></div>
                            </div>
                            <div class="clear_all"></div>
                        </div>
                        <div class="outside">
                            <div class="inside footer">
                                <!--BEGIN FOOTER -->
                                <div class="container footbox">
                                    <div class="footInner" style="margin-left: 18%;">
                                    <div class="page"><strong>Pages:</strong></div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypHome" href="http://www.wavhello.com/" runat="server">Home</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypContact" href="http://wavhello.com/pages/contact-us" runat="server">Contact Us</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypTerms" NavigateUrl="http://www.wavhello.com/terms-of-use" runat="server">Terms of Service</asp:HyperLink>
                                    </div>
                                </div>
                                <div class="footInner">
                                    <div class="stay"><strong>Stay In Touch:</strong></div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypRSS" href="http://www.bellybuds.com/feed/" runat="server">Site RSS Feed</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypTwitter" href="http://www.twitter.com/wavhello" runat="server">Twitter</asp:HyperLink>
                                    </div>
                                    <div class="link">
                                        <asp:HyperLink ID="hypFacebook" href="http://www.facebook.com/wavhello" runat="server">Facebook</asp:HyperLink>
                                    </div>
                                </div>
                                <div style="margin-left: -30px;"><strong>&copy; 2016 BellyBuds</strong></div>
                                </div>
                                <!--END OF FOOTER -->
                                <div class="clear_all"></div>
                            </div>
                        </div>
                    </div>
                    <!--BEGIN FOOTER -->
                </div>
                <!--END OF FOOTER -->
            </div>
    </form>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-15214788-2']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</body>
</html>
