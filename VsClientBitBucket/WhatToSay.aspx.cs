﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VsClientBitBucket
{
    public partial class WhatToSay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            if (Session["UniqueKey"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                spanCounter.InnerHtml = Session["NetworkCounter"].ToString();
                lbl_UserName.Text = (String)Session["FullUserName"];
            }
        }
    }
}