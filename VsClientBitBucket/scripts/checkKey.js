﻿function checkKey(evt) {
    var evt = (evt) ? evt : ((event) ? event : null);
    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13) && (node.type == "text") && (node.name == "txtFromAddr")) {
        return false;
    }
}
document.onkeypress = checkKey;