using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Collections;
using System.Net.Mail;
using VsClientBitBucket.Class;

namespace bbvs
{
    public partial class UploadStream : System.Web.UI.Page
    {
        private const string UPLOAD_DIRECTORY = "/Media/Recordings/";
        private IList extensions = new string[] { "mp3" };

        //public static void CopyStream(Stream input, Stream output)
        //{
        //    byte[] buffer = new byte[8 * 1024];
        //    int len;
        //    while ( (len = input.Read(buffer, 0, buffer.Length)) > 0)
        //    {
        //        output.Write(buffer, 0, len);
        //    }    
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sOTKey = "";
                //Get the OneTimeKey
                sOTKey = (string)Session["UniqueKey"];
                VsClientBitBucket.VoiceWebService.RegisterServiceClient VSUpload = new VsClientBitBucket.VoiceWebService.RegisterServiceClient();
                VsClientBitBucket.VoiceWebService.VS_Tenant TenantNPersonId = VSUpload.TenantNPersonId(sOTKey);

                Session["UniqueKey"] = TenantNPersonId.Otkey;
                sOTKey = TenantNPersonId.Otkey;

                int Tenant = TenantNPersonId.Id;
                int PersonID = TenantNPersonId.Person.PersonId;
                // description and title from page
                string title = (string)Session["Title"];
                string description = (string)Session["Description"];
                // recipient from list selected on the page
                string recipientEmail = (string)Session["SenderEmail"];     
                //get recipientID
                VsClientBitBucket.VoiceWebService.VS_Person recipientIDWOtkey = VSUpload.GetSendersId(recipientEmail, sOTKey);

                //Session["UniqueKey"] = recipientIDWOtkey.OTKey;
                sOTKey = recipientIDWOtkey.OTKey;
                int recepientID = recipientIDWOtkey.PersonId;
               
                VsClientBitBucket.VoiceWebService.DataMsg senderEmail = VSUpload.GetSendersEmail(recepientID, Tenant, sOTKey);

                //Session["UniqueKey"] = senderEmail.OtKey;
                sOTKey = senderEmail.OtKey;

                //the duration of the recorded audio file in seconds but accurate to the millisecond (like this: 4.322)
                long sizebyte = 0;
                string duration = this.Request.Params["duration"];
                Decimal dur1 = Convert.ToDecimal(duration);
                Decimal decDurationSeconds = Math.Round(dur1, 3);
                // create record in vs_message and return msg_id
                // No longer necessary
                //VsClientBitBucket.VoiceWebService.RegisterServiceClient VSUpload = new VsClientBitBucket.VoiceWebService.RegisterServiceClient();
                VsClientBitBucket.VoiceWebService.DataMsg msgID = VSUpload.msgId(Tenant, PersonID, recepientID, title, description, DateTime.Now, "temp", sizebyte, decDurationSeconds, UPLOAD_DIRECTORY, sOTKey);

                Session["UniqueKey"] = msgID.OtKey;
                sOTKey = msgID.OtKey;
                string msgId = msgID.Msg;

                //string msgID = VSUpload.msgId(Tenant, PersonID, senderID, description, DateTime.Now, "Default.mp3", sizebyte, dur2, "C:/");
                string TenantName = Convert.ToString(Tenant);

                //the recorderId value sent via flash vars from index.html
                string recorderId = this.Request.Params["recorderId"];


                //the userId sent via flash vars from index.html
                string userId = this.Request.Params["userId"];
                //the swf sends the name of the recording via the GET variable named "recordName"
                //string recordName = this.Request.Params["recordName"];

                string recordName = TenantName + "-" + msgId + ".mp3";

                //string uploadFile = Path.Combine(uploadDirectory, recordName);
                MessageContract msg = new MessageContract();

                //Get the stream  
                msg.bytes = (Stream)Request.InputStream;
                msg.filePath = recordName;

                long filelength = VSUpload.UploadStreamVoiceMessage(msg.filePath, msg.bytes);
                if (filelength != 0)
                {                 
                    //UpdateFileName
                    VsClientBitBucket.VoiceWebService.DataMsg updateFileResult = VSUpload.UpdateMsgFileName(msgId, TenantName, filelength, sOTKey);
  
                    Session["UniqueKey"] = updateFileResult.OtKey;
                    sOTKey = updateFileResult.OtKey;

                    DateTime Date = DateTime.Now;
                    //Session["Datetime"] = Date;
                    //SavetoMsg_Acct
                    VsClientBitBucket.VoiceWebService.DataMsg msgAcctResult = VSUpload.InsertMsg_Act(PersonID, recepientID, Date, msgId, sOTKey);

                    Session["UniqueKey"] = msgAcctResult.OtKey;
                    sOTKey = msgAcctResult.OtKey;

                    string response = string.Empty;
                    //response = "save=ok&fileurl=recordings/" + recordName;
                    response = "save=ok&fileurl=/Media/Recordings/" + recordName;
                    Response.Write(response);

                    string TenantUri = HttpContext.Current.Request.Url.Host.ToString();

                    //Email Portion
                    string recepientName = (string)Session["SenderName"];
                    string firstName = (string)Session["UserFirstName"];
                    string lastName = (string)Session["UserLastName"];

                    //get Loginpage
                    VsClientBitBucket.VoiceWebService.DataMsg loginUri = VSUpload.getAudioSrc(TenantUri, "LOGIN", sOTKey);

                    
                    //MailSubject
                    //string MailSubject = recepientName + ", " + "you've received a new baby message!";
                    //MailBody
                    //string MailBody = "<p>" + recepientName + "," + "</p><p>" + firstName + " " + lastName + " recorded a voice message and wrote you the note below:" + "</p><p>" + description +
                                        //"</p></br><p>" + "<a href='" + LoginUri.Msg + "'>" + "Login" + "</a>" + " to Bellybuds VoiceShare to hear your voice message!" + "</p><p>" + "-Team Bellybuds" + "</p>";
                    //send an email
                    //VsClientBitBucket.VoiceWebService.DataMsg emailresult = VSUpload.SendEmailToConfirmMsg(recepientID, TenantUri, MailSubject, MailBody, sOTKey);

                    if (loginUri == null)
                    {
                        //delete all data
                    }
                    else
                    {
                        Session["UniqueKey"] = loginUri.OtKey;
                        sOTKey = loginUri.OtKey;
                        //Session["UniqueKey"] = emailresult.OtKey;
                        //sOTKey = emailresult.OtKey;
                    }
                }
                else
                {
                    //To be done handle exception for file length 0
                }            
                }
                catch (Exception ex)
                {
                    string UPLOAD_DIRECTORY = "Exception/";
                    string uploadDirectory = HttpContext.Current.Server.MapPath(UPLOAD_DIRECTORY);
             
                    string exceptionFile = "exceptionlog.txt";
                    string uploadFile = Path.Combine(uploadDirectory, exceptionFile);
                    string New = "New";
                    DateTime DateTime = System.DateTime.Now;
                    string MethodName = "WebService's database connect exception occurs over string SaveInvitedatas(int SenderId, string RecipientEmail, string RecLastName, string RecFirstName, DateTime SentDate, int TenantId, string InviteStatus, string MsgType, string MsgText)";
                    string exception = ex.Message.ToString();
                    string[] ExceptionOccur = { " ", New, "Date:", DateTime.ToString(), "MethodName:", MethodName, "Exception:", exception };
                    File.AppendAllLines(uploadFile, ExceptionOccur);
                }
        }        
    }
}